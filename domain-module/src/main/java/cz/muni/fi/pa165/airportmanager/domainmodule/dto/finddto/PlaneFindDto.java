package cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.util.UUID;

public class PlaneFindDto extends DomainFindDto {
    private final String name;
    private final String type;
    private final Integer capacity;
    private final UUID airlineId;
    private final UUID currentLocationId;

    public PlaneFindDto() {
        super(null);
        this.name = "";
        this.type = "";
        this.capacity = null;
        this.airlineId = null;
        this.currentLocationId = null;
    }

    public PlaneFindDto(UUID guid, String name, String type, Integer capacity,
                        UUID airlineId, UUID currentLocationId) {
        super(guid);
        this.name = name;
        this.type = type;
        this.capacity = capacity;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }
}
