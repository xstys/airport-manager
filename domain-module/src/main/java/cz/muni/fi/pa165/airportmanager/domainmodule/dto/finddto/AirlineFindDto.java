package cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.util.List;
import java.util.UUID;

public class AirlineFindDto extends DomainFindDto {
    private final String name;
    private final List<UUID> flights;

    public AirlineFindDto() {
        super(null);
        this.name = "";
        this.flights = null;
    }
    public AirlineFindDto(UUID guid, String name, List<UUID> flights) {
        super(guid);
        this.name = name;
        this.flights = flights;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getFlights() {
        return flights;
    }
}
