package cz.muni.fi.pa165.airportmanager.domainmodule.data.model;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.Objects;
import java.util.UUID;

/**
 * An abstract class representing any database object.
 *
 * @author Krystof-Mikulas Stys
 */

@MappedSuperclass
public abstract class DomainEntity {
    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private final UUID id;
    @Column(name = "CREATED", nullable = false)
    private final ZonedDateTime created;
    @Column(name = "UPDATED")
    private ZonedDateTime updated = null;
    @Column(name = "DELETED")
    private ZonedDateTime deleted = null;

    public DomainEntity(UUID id) {
        this.id = Objects.requireNonNullElseGet(id, UUID::randomUUID);
        created = ZonedDateTime.now(ZoneId.of("UTC"));
    }

    public ZonedDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(ZonedDateTime changedTime) {
        updated = changedTime;
    }

    public ZonedDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(ZonedDateTime deletedTime) {
        deleted = deletedTime;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public UUID getId() {
        return id;
    }
}
