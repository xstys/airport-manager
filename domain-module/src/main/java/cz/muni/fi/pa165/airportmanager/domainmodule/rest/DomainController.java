package cz.muni.fi.pa165.airportmanager.domainmodule.rest;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ServerDownException;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

/**
 * An abstract class representing any controller which handles HTTP requests.
 *
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainController<ENTITY extends DomainEntity,
                                       INSERTDTO extends DomainInsertDto,
                                       UPDATEDTO extends DomainUpdateDto,
                                       FINDDTO extends DomainFindDto> {
    protected final DomainFacade<ENTITY, INSERTDTO, UPDATEDTO, FINDDTO> facade;

    public DomainController(DomainFacade<ENTITY, INSERTDTO,
                                         UPDATEDTO, FINDDTO> facade) {
        this.facade = facade;
    }

    public List<FINDDTO> getEntities() {
        return facade.getEntities();
    }

    public UUID addEntity(INSERTDTO entity) {
        return facade.addEntity(entity);
    }

    /**
     * @param updatedto a DTO representing a changed object.
     * @throws ResponseStatusException generates the information
     */
    public void updateEntity(UPDATEDTO updatedto) {
        facade.updateEntity(updatedto);
    }

    /**
     * @param id of an entity to be deleted
     * @throws ResponseStatusException generates the information
     */
    public void delete(UUID id) {
        facade.delete(id);
    }

    /**
     * @param id is an id of an entity, which should be
     * @return DTO object representing the entity,
     *         null if the entity was not found.
     * @throws ResponseStatusException generates the information
     */
    public FINDDTO findById(UUID id) {
        return facade.findById(id);
    }

    @ExceptionHandler(InvalidUserInputException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleNoSuchElementFoundException(
            InvalidUserInputException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body('"' + exception.getMessage() + '"');
    }

    @ExceptionHandler(ServerDownException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> handleServerDownException(
            ServerDownException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body('"' + exception.getMessage() + '"');
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleObjectNotFoundException(
            ObjectNotFoundException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body('"' + exception.getMessage() + '"');
    }

    @ExceptionHandler({MethodArgumentNotValidException.class,
                       TypeMismatchException.class})
    public ResponseEntity<String> handleMethodArgumentNotValid() {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("\"The provided input parameters " +
                        "do not match the required types.\"");
    }
}
