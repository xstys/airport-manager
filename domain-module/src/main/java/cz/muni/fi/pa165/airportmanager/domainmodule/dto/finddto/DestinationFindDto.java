package cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.util.UUID;

public class DestinationFindDto extends DomainFindDto {
    private final String city;
    private final String country;

    public DestinationFindDto() {
        super(UUID.randomUUID());
        city = "";
        country = "";
    }

    public DestinationFindDto(UUID guid, String city, String country) {
        super(guid);
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}