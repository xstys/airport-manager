package cz.muni.fi.pa165.airportmanager.domainmodule.data.repository;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

/**
 * An interface representing any database access object.
 * @author Krystof-Mikulas Stys
 */
public interface DomainRepository<ENTITY extends DomainEntity>
        extends JpaRepository<ENTITY, UUID> {
    /**
     * This method returns all non-deleted entities.
     * @return list of non-deleted entities.
     */
    List<ENTITY> findByDeletedIsNull();

    /**
     * This method is used to filter entities by their ids and ensures,
     * that is shows only non deleted items.
     * @param ids list of ids in GUID/ UUID format
     * @return list of entities that fulfill such criteria.
     */
    List<ENTITY> findByIdInAndDeletedIsNull(List<UUID> ids);
}
