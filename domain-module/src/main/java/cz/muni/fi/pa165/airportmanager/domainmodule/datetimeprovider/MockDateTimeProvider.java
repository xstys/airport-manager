package cz.muni.fi.pa165.airportmanager.domainmodule.datetimeprovider;

import java.time.LocalDateTime;

public class MockDateTimeProvider implements DateTimeProvider {
    private final LocalDateTime localDateTime;

    public MockDateTimeProvider(LocalDateTime localDateTime) {

        this.localDateTime = localDateTime;
    }

    @Override
    public LocalDateTime getCurrentTime() {
        return localDateTime;
    }
}
