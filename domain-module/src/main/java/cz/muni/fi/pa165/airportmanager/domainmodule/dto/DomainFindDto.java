package cz.muni.fi.pa165.airportmanager.domainmodule.dto;

import java.util.UUID;

/**
 * An abstract class representing any data transfer object returned
 * from find operations of facades.
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainFindDto {

    private final UUID guid;

    public DomainFindDto(UUID guid) {
        this.guid = guid;
    }

    public UUID getGuid() {
        return guid;
    }
}
