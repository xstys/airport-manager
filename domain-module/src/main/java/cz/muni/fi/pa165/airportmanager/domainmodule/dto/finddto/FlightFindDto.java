package cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

public class FlightFindDto extends DomainFindDto {
    private final UUID guid;
    private final UUID airlineId;
    private final UUID planeId;
    private final UUID originId;
    private final UUID destinationId;
    private final ZonedDateTime departTime;
    private final ZonedDateTime arriveTime;
    private final List<UUID> stewards;

    public FlightFindDto() {
        super(null);
        this.guid = null;
        this.planeId = null;
        this.airlineId = null;
        this.originId = null;
        this.destinationId = null;
        this.departTime = null;
        this.arriveTime = null;
        this.stewards = null;
    }

    public FlightFindDto(UUID guid, UUID planeId, UUID originId,
                         UUID destinationId, ZonedDateTime departTime,
                         ZonedDateTime arriveTime, List<UUID> stewards,
                         UUID airlineId) {
        super(guid);
        this.guid = guid;
        this.planeId = planeId;
        this.airlineId = airlineId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = stewards;
    }

    public UUID getGuid() {
        return guid;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getPlaneId() {
        return planeId;
    }

    public UUID getOriginId() {
        return originId;
    }

    public UUID getDestinationId() {
        return destinationId;
    }

    public ZonedDateTime getDepartTime() {
        return departTime;
    }

    public ZonedDateTime getArriveTime() {
        return arriveTime;
    }

    public List<UUID> getStewards() {
        return stewards;
    }
}
