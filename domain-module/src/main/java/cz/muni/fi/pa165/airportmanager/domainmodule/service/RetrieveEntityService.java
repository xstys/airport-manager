package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.util.UUID;

public interface RetrieveEntityService<FINDDTO extends DomainFindDto> {
    FINDDTO retrieve(UUID id, String authenticationHeader);
}
