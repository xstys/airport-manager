package cz.muni.fi.pa165.airportmanager.domainmodule.dto;

import java.util.UUID;

public abstract class DomainDetailDto {
    private final UUID guid;

    public DomainDetailDto(UUID guid) {
        this.guid = guid;
    }

    public UUID getGuid() {
        return guid;
    }
}
