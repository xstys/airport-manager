package cz.muni.fi.pa165.airportmanager.domainmodule.exception;

/**
 * A runtime exception for when there is an issue with connecting
 * to a different server.
 * @author Krystof-Mikulas Stys
 */
public class ServerDownException extends RuntimeException {
    public ServerDownException(String message) { super(message); }
}