package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.UUID;

public abstract class RetrieveMultipleEntitiesByIdServiceImpl<FINDDTO extends DomainFindDto>
        extends RetrieveEntityServiceImpl<FINDDTO>
        implements RetrieveMultipleEntitiesService<FINDDTO> {
    private final String multipleEntityEndpoint;

    public RetrieveMultipleEntitiesByIdServiceImpl(String entityUrl,
                                                   String entityEndpoint,
                                                   String multipleEntityEndpoint) {
        super(entityUrl, entityEndpoint);
        this.multipleEntityEndpoint = multipleEntityEndpoint;
    }

    protected WebClient.ResponseSpec retrieveResponseSpec(List<UUID> ids,
                                                          String authenticationHeader) {
        var webClient = WebClient.create();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < ids.size(); i++) {
            var currentId = ids.get(i);

            if (i == 0) {
                sb.append(currentId);
            } else {
                sb.append("&ids=");
                sb.append(currentId);
            }
        }

        return webClient.get()
                .uri(destinationUrl + multipleEntityEndpoint + "?ids=" + sb)
                .header(HttpHeaders.AUTHORIZATION, authenticationHeader)
                .retrieve();
    }
}
