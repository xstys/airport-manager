package cz.muni.fi.pa165.airportmanager.domainmodule.exception;

/**
 * A runtime exception for when the user input query returns null
 * @author Tereza Vrabcova
 */
public class ObjectNotFoundException extends RuntimeException {
    public ObjectNotFoundException(String message) { super(message); }
}