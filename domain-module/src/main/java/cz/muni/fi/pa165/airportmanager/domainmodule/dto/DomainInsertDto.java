package cz.muni.fi.pa165.airportmanager.domainmodule.dto;

/**
 * An abstract class representing any data transfer object retrieved
 * in insert endpoints.
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainInsertDto {
}
