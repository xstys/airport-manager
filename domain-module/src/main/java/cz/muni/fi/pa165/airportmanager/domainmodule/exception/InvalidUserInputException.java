package cz.muni.fi.pa165.airportmanager.domainmodule.exception;

/**
 * A runtime exception for when there is an issue with the user input.
 * @author Krystof-Mikulas Stys
 */
public class InvalidUserInputException extends RuntimeException {
    public InvalidUserInputException(String message) {
        super(message);
    }
}
