package cz.muni.fi.pa165.airportmanager.domainmodule.datetimeprovider;

import java.time.LocalDateTime;

/**
 * Adapter for replacing the datetime provider.
 * @author Krystof-Mikulas Stys
 */
public interface DateTimeProvider {
    /**
     * @return LocalDateTime returns current date time.
     */
    LocalDateTime getCurrentTime();
}
