package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;

import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * An abstract class representing any service
 * which handles business operations regarding a specific entity.
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainServiceImpl<ENTITY extends DomainEntity,
                                        INSERTDTO extends DomainInsertDto,
                                        UPDATEDTO extends DomainUpdateDto,
                                        FINDDTO extends DomainFindDto>
        implements DomainService<ENTITY, INSERTDTO, UPDATEDTO, FINDDTO> {
    protected final DomainRepository<ENTITY> domainRepository;
    protected final DomainMapper<ENTITY, INSERTDTO, UPDATEDTO, FINDDTO> domainMapper;

    public DomainServiceImpl(DomainRepository<ENTITY> domainRepository,
                             DomainMapper<ENTITY, INSERTDTO, UPDATEDTO,
                                          FINDDTO> domainMapper) {
        this.domainRepository = domainRepository;
        this.domainMapper = domainMapper;
    }

    @Transactional(readOnly = true)
    public List<FINDDTO> getEntities() {
        List<ENTITY> entities = domainRepository.findByDeletedIsNull();

        return domainMapper.map(entities);
    }

    @Transactional
    public UUID addEntity(INSERTDTO insertdto) {
        ENTITY entity = domainMapper.map(insertdto);
        ENTITY savedEntity = domainRepository.save(entity);

        if (savedEntity.equals(null)) {
            throw new InvalidUserInputException("Invalid data was provided.");
        }

        return savedEntity.getId();
    }

    @Transactional
    public void updateEntity(UPDATEDTO updatedto) {
        if (updatedto == null) {
            throw new InvalidUserInputException(
                    "Invalid update value was provided!");
        }

        var found = domainRepository.findById(updatedto.getGuid());

        if (found.isEmpty()) {
            throw new ObjectNotFoundException("This item does not exist.");
        }

        if (found.get().getDeleted() != null) {
            throw new InvalidUserInputException(
                    "This item has been previously deleted.");
        }

        ENTITY entity = domainMapper.map(updatedto);

        entity.setUpdated(ZonedDateTime.now(ZoneId.of("UTC")));

        domainRepository.save(entity);
    }

    @Transactional
    public void delete(UUID id) {
        Optional<ENTITY> entity = domainRepository.findById(id);

        if (entity.isEmpty()) {
            throw new ObjectNotFoundException("This item does not exist.");
        }

        if (entity.get().getDeleted() != null) {
            throw new InvalidUserInputException(
                    "This item has been previously deleted.");
        }

        ENTITY changedEntity = entity.get();

        changedEntity.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));

        domainRepository.save(changedEntity);
    }

    @Transactional(readOnly = true)
    public FINDDTO findById(UUID id) {
        Optional<ENTITY> entity = domainRepository.findById(id);

        if (entity.isEmpty()) {
            throw new ObjectNotFoundException(
                    "No item with this id has been found.");
        }

        if (entity.get().getDeleted() != null) {
            throw new InvalidUserInputException(
                    "This item has been previously deleted.");
        }

        return domainMapper.map(entity.get());
    }
}
