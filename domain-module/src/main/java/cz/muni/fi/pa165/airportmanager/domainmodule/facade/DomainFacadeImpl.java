package cz.muni.fi.pa165.airportmanager.domainmodule.facade;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainService;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * An abstract class representing any facade which divides work
 * for request handling to services.
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainFacadeImpl<ENTITY extends DomainEntity,
                                       INSERTDTO extends DomainInsertDto,
                                       UPDATEDTO extends DomainUpdateDto,
                                       FINDDTO extends DomainFindDto>
        implements DomainFacade<ENTITY, INSERTDTO, UPDATEDTO, FINDDTO> {
    protected final DomainService<ENTITY, INSERTDTO, UPDATEDTO, FINDDTO> domainService;

    public DomainFacadeImpl(DomainService<ENTITY, INSERTDTO,
                                          UPDATEDTO, FINDDTO> domainService) {
        this.domainService = domainService;
    }

    @Transactional(readOnly = true)
    public List<FINDDTO> getEntities() {
        return domainService.getEntities();
    }

    @Transactional
    public UUID addEntity(INSERTDTO insertdto) {
        return domainService.addEntity(insertdto);
    }

    @Transactional
    public void updateEntity(UPDATEDTO updatedto) {
        domainService.updateEntity(updatedto);
    }

    @Transactional
    public void delete(UUID id) {
        domainService.delete(id);
    }

    @Transactional(readOnly = true)
    public FINDDTO findById(UUID id) {
        return domainService.findById(id);
    }
}
