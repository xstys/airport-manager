package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.List;
import java.util.UUID;

/**
 * An interface representing any facade which divides work
 * for request handling to services.
 * @author Krystof-Mikulas Stys
 */
public interface DomainService<ENTITY extends DomainEntity,
                               INSERTDTO extends DomainInsertDto,
                               UPDATEDTO extends DomainUpdateDto,
                               FINDDTO extends DomainFindDto> {
    /**
     * @return all database entries.
     */
    List<FINDDTO> getEntities();

    /**
     * Adds a new entry.
     * @param entity an object which should be added
     * @return UUID of added entity
     */
    UUID addEntity(INSERTDTO entity);
    /**
     * Updates an existing entry.
     * @param entity with the id of the edited entry and new values.
     */
    void updateEntity(UPDATEDTO entity);
    /**
     * Sets the ID specific object as deleted and
     * does not allow any further changes.
     * @param id is the UUID of the entity to be deleted.
     */
    void delete(UUID id);
    /**
     * Finds the entity with the provided ID
     * @param id is the UUID of the entity to be found.
     */
    FINDDTO findById(UUID id);
}
