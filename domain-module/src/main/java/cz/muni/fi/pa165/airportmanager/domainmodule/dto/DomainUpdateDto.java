package cz.muni.fi.pa165.airportmanager.domainmodule.dto;

import java.util.UUID;

/**
 * An abstract class representing any data transfer object retrieved
 * from update endpoints.
 * @author Krystof-Mikulas Stys
 */
public abstract class DomainUpdateDto {
    private final UUID guid;

    public DomainUpdateDto(UUID guid) {
        this.guid = guid;
    }

    public UUID getGuid() {
        return guid;
    }
}
