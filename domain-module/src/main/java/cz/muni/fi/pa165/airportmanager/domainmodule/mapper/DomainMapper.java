package cz.muni.fi.pa165.airportmanager.domainmodule.mapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.List;

/**
 * An interface representing any mapper which converts database entities
 * to data transfer objects.
 * @author Krystof-Mikulas Stys
 */
public interface DomainMapper<ENTITY extends DomainEntity,
                              INSERTDTO extends DomainInsertDto,
                              UPDATEDTO extends DomainUpdateDto,
                              FINDDTO extends DomainFindDto> {
    ENTITY map(INSERTDTO insertdto);
    ENTITY map(UPDATEDTO updatedto);
    FINDDTO map(ENTITY entity);
    List<FINDDTO> map(List<ENTITY> entity);
}
