package cz.muni.fi.pa165.airportmanager.domainmodule.datetimeprovider;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LocalDateTimeProvider implements DateTimeProvider {
    @Override
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }
}
