package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;

public abstract class RetrieveEntityServiceImpl<FindDTO extends DomainFindDto>
        implements RetrieveEntityService<FindDTO> {
    protected final String destinationUrl;
    protected final String destinationEndpoint;

    public RetrieveEntityServiceImpl(String destinationUrl,
                                     String destinationEndpoint) {
        this.destinationUrl = destinationUrl;
        this.destinationEndpoint = destinationEndpoint;
    }

    protected WebClient.ResponseSpec retrieveResponseSpec(UUID id,
                                                          String authenticationHeader) {
        var webClient = WebClient.create();

        return webClient.get()
                .uri(destinationUrl + destinationEndpoint + id)
                .header(HttpHeaders.AUTHORIZATION, authenticationHeader)
                .retrieve();
    }
}
