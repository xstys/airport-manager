package cz.muni.fi.pa165.airportmanager.domainmodule.facade;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.List;
import java.util.UUID;

/**
 * An interface representing any facade which divides work
 * for request handling to services.
 * @author Krystof-Mikulas Stys
 */
public interface DomainFacade<ENTITY extends DomainEntity,
                              INSERTDTO extends DomainInsertDto,
                              UPDATEDTO extends DomainUpdateDto,
                              FINDDTO extends DomainFindDto> {
    List<FINDDTO> getEntities();
    UUID addEntity(INSERTDTO entity);
    void updateEntity(UPDATEDTO entity);
    void delete(UUID id);
    FINDDTO findById(UUID id);
}
