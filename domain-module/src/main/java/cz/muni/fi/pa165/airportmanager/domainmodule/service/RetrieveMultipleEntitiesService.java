package cz.muni.fi.pa165.airportmanager.domainmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainFindDto;

import java.util.List;
import java.util.UUID;

public interface RetrieveMultipleEntitiesService<FINDDTO extends DomainFindDto>
        extends RetrieveEntityService<FINDDTO> {
    List<FINDDTO> retrieve(List<UUID> id, String authenticationHeader);
}
