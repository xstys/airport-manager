package cz.muni.fi.pa165.airportmanager.flightmodule.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZonedDateTime;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
class FlightIntegrationTest {
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    void cleanup() {
        flightRepository.deleteAll();
    }

    @Test
    void correctPageNumberTest() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(1),
                           ZonedDateTime.now().plusDays(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=1&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var totalPages = (int) pages.get("totalPages");
        assertEquals(2, totalPages);
    }

    @Test
    void correctFlightNumberTest() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(1),
                           ZonedDateTime.now().plusDays(2),
                           List.of(UUID.randomUUID(),
                                   UUID.randomUUID()));

        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var totalFlights = (int) pages.get("totalFlights");
        assertEquals(2, totalFlights);
    }

    @Test
    void noFlightsTest() throws Exception {
        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var totalFlights = (int) pages.get("totalFlights");
        assertEquals(0, totalFlights);
    }

    @Test
    void correctCurrentPage() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(1),
                           ZonedDateTime.now().plusDays(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));

        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var currentPage = (int) pages.get("currentPage");
        assertEquals(0, currentPage);
    }

    @Test
    void noFlightsFromThePast() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().minusHours(2),
                           ZonedDateTime.now(),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(1),
                           ZonedDateTime.now().plusDays(2),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));

        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var totalFlights = (int) pages.get("totalFlights");
        assertEquals(1, totalFlights);
    }

    @Test
    void flightsOrderedByNewestTest() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(2),
                           ZonedDateTime.now().plusDays(3),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(5),
                           ZonedDateTime.now().plusDays(6),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var c = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(10),
                           ZonedDateTime.now().plusDays(11),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));

        flightRepository.save(c);
        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=newest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var flightList = (ArrayList) pages.get("flights");

        for (int i = 0; i < flightList.size() - 1; i++) {
            var flight = flightList.get(i);
            var converted = (LinkedHashMap<String, String>) flight;
            var dateStr = converted.get("departTime");
            var date = ZonedDateTime.parse(dateStr);

            var next = flightList.get(i + 1);
            var nextDateStr = ((LinkedHashMap<String, String>)next)
                    .get("departTime");
            var nextDate = ZonedDateTime.parse(nextDateStr);

            assertTrue(date.isAfter(nextDate));
        }
    }

    @Test
    void flightsOrderedByOldestTest() throws Exception {
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(2),
                           ZonedDateTime.now().plusDays(3),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(5),
                           ZonedDateTime.now().plusDays(6),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));
        var c = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusDays(10),
                           ZonedDateTime.now().plusDays(11),
                           List.of(UUID.randomUUID(), UUID.randomUUID()));

        flightRepository.save(c);
        flightRepository.save(a);
        flightRepository.save(b);
        flightRepository.flush();

        var result = mockMvc.perform(
                get("/api/v1/flights?page=0&size=5&order=oldest"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        Map<String, Object> pages = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});
        var flightList = (ArrayList) pages.get("flights");

        for (int i = 0; i < flightList.size() - 1; i++) {
            var flight = flightList.get(i);
            var converted = (LinkedHashMap<String, String>) flight;
            var dateStr = converted.get("departTime");
            var date = ZonedDateTime.parse(dateStr);

            var next = flightList.get(i + 1);
            var nextDateStr = ((LinkedHashMap<String, String>)next)
                    .get("departTime");
            var nextDate = ZonedDateTime.parse(nextDateStr);

            assertTrue(date.isBefore(nextDate));
        }
    }
}
