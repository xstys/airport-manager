package cz.muni.fi.pa165.airportmanager.flightmodule.availablestewards;

import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.mapper.FlightMapper;
import cz.muni.fi.pa165.airportmanager.flightmodule.service.FlightService;
import cz.muni.fi.pa165.airportmanager.flightmodule.service.FlightServiceImpl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class AvailableStewardsTest {
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    FlightMapper mapper;
    FlightService service;

    @BeforeEach
    void setUp() {
        mapper = mock(FlightMapper.class);
        service = new FlightServiceImpl(flightRepository, mapper);
    }

    @AfterEach
    void cleanup() {
        flightRepository.deleteAll();
    }

    @Test
    void stewardNotAvailableThrowsExceptionInService() {
        var stewardId = UUID.randomUUID();
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(stewardId, UUID.randomUUID()));

        var insertDto = new FlightInsertDto(UUID.randomUUID(),
                                            UUID.randomUUID(),
                                            UUID.randomUUID(),
                                            UUID.randomUUID(),
                                            ZonedDateTime.now().plusHours(1),
                                            ZonedDateTime.now().plusHours(2),
                                            List.of(stewardId,
                                                    UUID.randomUUID()));

        flightRepository.save(a);
        flightRepository.flush();
        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(insertDto));
    }

    @Test
    void stewardNotAvailable() {
        var stewardId = UUID.randomUUID();
        var a = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(stewardId, UUID.randomUUID()));
        var b = new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(),
                           ZonedDateTime.now().plusHours(1),
                           ZonedDateTime.now().plusHours(2),
                           List.of(stewardId,
                                   UUID.randomUUID()));

        flightRepository.save(a);
        flightRepository.flush();
        assertFalse(flightRepository
                .isStewardAvailable(b.getDepartTime(), b.getArriveTime(),
                                    stewardId));
    }
}
