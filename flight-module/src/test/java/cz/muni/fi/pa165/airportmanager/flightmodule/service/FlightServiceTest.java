package cz.muni.fi.pa165.airportmanager.flightmodule.service;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.mapper.FlightMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.ZonedDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
@SpringBootTest
@ActiveProfiles("test")
class FlightServiceTest {
    FlightRepository repository;
    FlightMapper mapper;
    FlightService service;
    FlightInsertDto validInsertDto;
    List<UUID> validStewards;

    @BeforeEach
    void setUp() {
        repository = mock(FlightRepository.class);
        mapper = mock(FlightMapper.class);
        service = new FlightServiceImpl(repository, mapper);
        validStewards = List.of(UUID.randomUUID(), UUID.randomUUID());
        validInsertDto = new FlightInsertDto(UUID.randomUUID(),
                                             UUID.randomUUID(),
                                             UUID.randomUUID(),
                                             UUID.randomUUID(),
                                             ZonedDateTime.now(),
                                             ZonedDateTime.now().plusHours(2),
                                             validStewards);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTest() {
        when(repository.isStewardAvailable(any(), any(), any()))
                .thenReturn(true);
        when(repository.save(any())).thenReturn(new Flight());
        service.addEntity(validInsertDto);

        verify(repository).save(any());
    }

    @Test
    void addNoPlaneTest() {
        validInsertDto.setPlaneId(null);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addNoOriginTest() {
        validInsertDto.setOriginId(null);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addNoDestinationTest() {
        validInsertDto.setDestinationId(null);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addNullStewardsTest() {
        validInsertDto.setStewards(null);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addNoStewardsTest() {
        var originId = UUID.randomUUID();
        var destinationId = UUID.randomUUID();
        var irrelevantId = UUID.randomUUID();
        var flight = new FlightInsertDto(irrelevantId, irrelevantId,
                                         originId, destinationId,
                                         ZonedDateTime.now(),
                                         ZonedDateTime.now().plusHours(5),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(flight));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addTooManyStewardsTest() {
        var stewards = List.of(UUID.randomUUID(), UUID.randomUUID(),
                               UUID.randomUUID(), UUID.randomUUID(),
                               UUID.randomUUID(), UUID.randomUUID());
        validInsertDto.setStewards(stewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addDuplicatedStewardsTest() {
        var id = UUID.randomUUID();
        var stewards = List.of(id, id);
        validInsertDto.setStewards(stewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(validInsertDto));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addSameDepartureAndArrivalTest() {
        var originId = UUID.randomUUID();
        var destinationId = UUID.randomUUID();
        var irrelevantId = UUID.randomUUID();
        var flight = new FlightInsertDto(irrelevantId, irrelevantId, originId,
                                         destinationId, ZonedDateTime.now(),
                                         ZonedDateTime.now(),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(flight));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addSameOriginAndDestinationTest() {
        var irrelevantId = UUID.randomUUID();
        var flight = new FlightInsertDto(irrelevantId, irrelevantId,
                                         irrelevantId, irrelevantId,
                                         ZonedDateTime.now(),
                                         ZonedDateTime.now(),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(flight));

        verify(repository, times(0)).save(any());
    }

    @Test
    void addNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> service.addEntity(null));
        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(null));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNonExistentTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards);
        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNoPlaneTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         null,
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNoOriginTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         validInsertDto.getPlaneId(),
                                  null,
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNoDestinationTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         null,
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNullStewardsTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         null);

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateNoStewardsTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateTooManyStewardsTest() {
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         null,
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         List.of(UUID.randomUUID(),
                                                 UUID.randomUUID(),
                                                 UUID.randomUUID(),
                                                 UUID.randomUUID(),
                                                 UUID.randomUUID(),
                                                 UUID.randomUUID()));

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateDuplicatedStewardsTest() {
        var id = UUID.randomUUID();
        var stewards = List.of(id, id);
        var entity = new FlightUpdateDto(UUID.randomUUID(),
                                         validInsertDto.getAirlineId(),
                                         null,
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         stewards);

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(entity));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateSameDepartureAndArrivalTest() {
        var originId = UUID.randomUUID();
        var destinationId = UUID.randomUUID();
        var irrelevantId = UUID.randomUUID();
        var flight = new FlightUpdateDto(irrelevantId, irrelevantId,
                                         irrelevantId, originId,
                                         destinationId, ZonedDateTime.now(),
                                         ZonedDateTime.now(),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(flight));

        verify(repository, times(0)).save(any());
    }

    @Test
    void updateSameOriginAndDestinationTest() {
        var irrelevantId = UUID.randomUUID();
        var flight = new FlightUpdateDto(irrelevantId, irrelevantId,
                                         irrelevantId, irrelevantId,
                                         irrelevantId, ZonedDateTime.now(),
                                         ZonedDateTime.now(),
                                         new ArrayList<>());

        assertThrows(InvalidUserInputException.class,
                     () -> service.updateEntity(flight));

        verify(repository, times(0)).save(any());
    }

    @Test
    void findByNullTest() {
        assertThrows(ObjectNotFoundException.class,
                     () -> service.findById(null));

        verify(repository, times(0)).save(any());
    }

    @Test
    void findNonexistentTest() {
        assertThrows(ObjectNotFoundException.class,
                     () -> service.findById(UUID.randomUUID()));

        verify(repository, times(0)).save(any());
    }

    @Test
    void findTest() {
        var id = UUID.randomUUID();
        var found = new Flight(id, validInsertDto.getAirlineId(),
                               validInsertDto.getPlaneId(),
                               validInsertDto.getOriginId(),
                               validInsertDto.getDestinationId(),
                               validInsertDto.getDepartTime(),
                               validInsertDto.getArriveTime(),
                               validStewards);
        var foundDto = new FlightFindDto(id, validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards,
                                         validInsertDto.getAirlineId());

        when(repository.findById(id)).thenReturn(Optional.of(found));
        when(mapper.map(found)).thenReturn(foundDto);

        var result = service.findById(id);

        verify(repository, times(1)).findById(any());

        assertEquals(foundDto, result);
    }

    @Test
    void deleteNullTest() {
        assertThrows(ObjectNotFoundException.class,
                     () -> service.delete(null));

        verify(repository, times(0)).delete(any());
    }

    @Test
    void deleteNonexistentTest() {
        assertThrows(ObjectNotFoundException.class,
                     () -> service.delete(UUID.randomUUID()));

        verify(repository, times(0)).delete(any());
    }

    @Test
    void deleteTest() {
        var id = UUID.randomUUID();
        var found = new Flight(id, validInsertDto.getAirlineId(),
                               validInsertDto.getPlaneId(),
                               validInsertDto.getOriginId(),
                               validInsertDto.getDestinationId(),
                               validInsertDto.getDepartTime(),
                               validInsertDto.getArriveTime(),
                               validStewards);

        when(repository.findById(id)).thenReturn(Optional.of(found));

        service.delete(id);

        verify(repository, times(1)).save(found);
    }

    @Test
    void getAllEmptyTest() {
        when(repository.findAll()).thenReturn(new ArrayList<>());
        when(mapper.map(anyList())).thenReturn(new ArrayList<>());

        var result = service.getEntities();

        verify(repository, times(1)).findByDeletedIsNull();

        assertEquals(0, result.size());
    }

    @Test
    void getAllTest() {
        var id = UUID.randomUUID();
        var found = new Flight(id, validInsertDto.getAirlineId(),
                               validInsertDto.getPlaneId(),
                               validInsertDto.getOriginId(),
                               validInsertDto.getDestinationId(),
                               validInsertDto.getDepartTime(),
                               validInsertDto.getArriveTime(),
                               validStewards);
        var foundDto = new FlightFindDto(id, validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards,
                                         validInsertDto.getAirlineId());

        when(repository.findAll()).thenReturn(List.of(found));
        when(mapper.map(anyList())).thenReturn(List.of(foundDto));

        var result = service.getEntities();

        verify(repository, times(1)).findByDeletedIsNull();

        assertEquals(1, result.size());
        assertEquals(foundDto, result.get(0));
    }

    @Test
    void getSubsequentTest() {
        var id = UUID.randomUUID();
        var found = new Flight(id, validInsertDto.getAirlineId(),
                               validInsertDto.getPlaneId(),
                               validInsertDto.getOriginId(),
                               validInsertDto.getDestinationId(),
                               validInsertDto.getDepartTime(),
                               validInsertDto.getArriveTime(),
                               validStewards);
        var foundDto = new FlightFindDto(id, validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards,
                                         validInsertDto.getAirlineId());

        when(repository.findById(any())).thenReturn(Optional.of(found));
        when(repository.findSubsequentFlights(any(), any(), any()))
                .thenReturn(List.of(found));
        when(mapper.map(anyList())).thenReturn(List.of(foundDto));

        var result = service.getSubsequentFlights(id);

        verify(repository, times(1))
                .findSubsequentFlights(any(), any(), any());

        assertEquals(1, result.size());
        assertEquals(foundDto, result.get(0));
    }

    @Test
    void getSubsequentEmptyTest() {
        var id = UUID.randomUUID();
        var found = new Flight(id, validInsertDto.getAirlineId(),
                               validInsertDto.getPlaneId(),
                               validInsertDto.getOriginId(),
                               validInsertDto.getDestinationId(),
                               validInsertDto.getDepartTime(),
                               validInsertDto.getArriveTime(),
                               validStewards);
        var foundDto = new FlightFindDto(id, validInsertDto.getPlaneId(),
                                         validInsertDto.getOriginId(),
                                         validInsertDto.getDestinationId(),
                                         validInsertDto.getDepartTime(),
                                         validInsertDto.getArriveTime(),
                                         validStewards,
                                         validInsertDto.getAirlineId());

        when(repository.findById(any())).thenReturn(Optional.empty());

        assertThrows(InvalidUserInputException.class,
                     () -> service.getSubsequentFlights(id));
    }
}
