package cz.muni.fi.pa165.airportmanager.flightmodule.findsubsequentflights;

import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZonedDateTime;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class FindSubsequentFlightsTest {
    @Autowired
    private FlightRepository repository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @AfterEach
    public void cleanup() {
        repository.deleteAll();
    }

    @Test
    public void findSubsequentFlightsInvalidId() throws Exception {
        ResultActions result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void findNoSubsequentFlights() throws Exception {
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), UUID.randomUUID(),
                           ZonedDateTime.now(),
                           ZonedDateTime.now().plusHours(2)));

        repository.flush();

        ResultActions result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        result.andExpect(content().json("[]"));
    }

    /**
     * The test checks whether the lowest possible value of departure time
     * of a subsequent flight (one hour in this case) is still included
     * in the list of subsequent flights.
     */
    @Test
    public void findSubsequentFlightsLowestIncludedDepartureTimeFlightReturned()
            throws Exception {
        var destId = UUID.randomUUID();
        var time = ZonedDateTime.now().plusHours(2);
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), destId, ZonedDateTime.now(),
                           time));
        Flight subsequentFlight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(), time.plusHours(1),
                           time.plusHours(2)));

        repository.flush();

        MvcResult result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        byte[] content = result.getResponse().getContentAsByteArray();
        var actualObject = objectMapper.readValue(content,
                                                  FlightFindDto[].class);

        assertEquals(1, actualObject.length);
        assertEquals(subsequentFlight.getId(), actualObject[0].getGuid());
    }

    @Test
    public void findSubsequentFlightMultipleSubsequent() throws Exception {
        var destId = UUID.randomUUID();
        var time = ZonedDateTime.now().plusHours(2);
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), destId, ZonedDateTime.now(),
                           time));
        Flight subsequentFlight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(),
                           time.plusHours(5).minusSeconds(1),
                           time.plusHours(7)));
        Flight subsequentFlight2 = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(),
                           time.plusHours(1), time.plusHours(2)));

        repository.flush();

        MvcResult result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        byte[] content = result.getResponse().getContentAsByteArray();
        var actualObject = objectMapper.readValue(content,
                                                  FlightFindDto[].class);

        assertEquals(2, actualObject.length);
        assertEquals(subsequentFlight.getId(), actualObject[0].getGuid());
        assertEquals(subsequentFlight2.getId(), actualObject[1].getGuid());
    }

    /**
     * The test checks whether the highest possible value of departure time
     * of a subsequent flight (5 hours minus one second in this case)
     * is still included in the list of subsequent flights.
     */
    @Test
    public void findSubsequentFlightsHighestIncludedDepartureTimeFlightReturned()
            throws Exception {
        var destId = UUID.randomUUID();
        var time = ZonedDateTime.now().plusHours(2);
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), destId,
                           ZonedDateTime.now(), time));
        Flight subsequentFlight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(),
                           time.plusHours(5).minusSeconds(1),
                           time.plusHours(7)));

        repository.flush();

        MvcResult result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        byte[] content = result.getResponse().getContentAsByteArray();
        var actualObject = objectMapper.readValue(content,
                                                  FlightFindDto[].class);

        assertEquals(1, actualObject.length);
        assertEquals(subsequentFlight.getId(), actualObject[0].getGuid());
    }

    /**
     * The test checks whether the highest value under the lower threshold
     * (one hour minus one second in this case) is
     * not included in the list a subsequent flight.
     */
    @Test
    public void findSubsequentFlightsHighestNotIncludedTimeFlightNotIncluded()
            throws Exception {
        var destId = UUID.randomUUID();
        var time = ZonedDateTime.now().plusHours(2);
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), destId, ZonedDateTime.now(),
                           time));
        Flight subsequentFlight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(),
                           time.plusHours(1).minusSeconds(1),
                           time.plusHours(2)));

        repository.flush();

        MvcResult result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        byte[] content = result.getResponse().getContentAsByteArray();
        var actualObject = objectMapper.readValue(content,
                                                  FlightFindDto[].class);

        assertEquals(0, actualObject.length);
    }

    /**
     * The test checks whether the lowest possible value of departure time
     * over the top threshold of a subsequent flight (5 hours in this case)
     * is not included in the list of subsequent flights.
     */
    @Test
    public void findSubsequentFlightsLowestOverNotIncludedTimeFlightNotIncluded()
            throws Exception {
        var destId = UUID.randomUUID();
        var time = ZonedDateTime.now().plusHours(2);
        Flight flight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(),
                           UUID.randomUUID(), destId,
                           ZonedDateTime.now(),
                           time));
        Flight subsequentFlight = repository.save(
                new Flight(UUID.randomUUID(), UUID.randomUUID(), destId,
                           UUID.randomUUID(), time.plusHours(5),
                           time.plusHours(7)));

        repository.flush();

        MvcResult result = mockMvc.perform(
                get("/api/v1/flights/subsequent/" + flight.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        byte[] content = result.getResponse().getContentAsByteArray();
        var actualObject = objectMapper.readValue(content,
                                                  FlightFindDto[].class);

        assertEquals(0, actualObject.length);
    }
}
