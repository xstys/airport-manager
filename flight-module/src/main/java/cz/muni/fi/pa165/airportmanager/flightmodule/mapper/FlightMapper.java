package cz.muni.fi.pa165.airportmanager.flightmodule.mapper;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightMapper implements DomainMapper<Flight, FlightInsertDto,
                                                  FlightUpdateDto,
                                                  FlightFindDto> {
    @Override
    public Flight map(FlightInsertDto flightInsertDto) {
        if (flightInsertDto == null) {
            return null;
        }

        return new Flight(flightInsertDto.getAirlineId(),
                          flightInsertDto.getPlaneId(),
                          flightInsertDto.getOriginId(),
                          flightInsertDto.getDestinationId(),
                          flightInsertDto.getDepartTime(),
                          flightInsertDto.getArriveTime(),
                          flightInsertDto.getStewards());
    }

    @Override
    public Flight map(FlightUpdateDto flightUpdateDto) {
        if (flightUpdateDto == null) {
            return null;
        }

        return new Flight(flightUpdateDto.getGuid(),
                          flightUpdateDto.getAirlineId(),
                          flightUpdateDto.getPlaneId(),
                          flightUpdateDto.getOriginId(),
                          flightUpdateDto.getDestinationId(),
                          flightUpdateDto.getDepartTime(),
                          flightUpdateDto.getArriveTime(),
                          flightUpdateDto.getStewards());
    }

    @Override
    public FlightFindDto map(Flight entity) {
        if (entity == null) {
            return null;
        }

        return new FlightFindDto(entity.getId(), entity.getPlaneId(),
                                 entity.getOriginId(),
                                 entity.getDestinationId(),
                                 entity.getDepartTime(),
                                 entity.getArriveTime(),
                                 entity.getStewards(),
                                 entity.getAirlineId());
    }

    @Override
    public List<FlightFindDto> map(List<Flight> entity) {
        return entity.stream()
                .map(e -> new FlightFindDto(e.getId(), e.getPlaneId(),
                                            e.getOriginId(),
                                            e.getDestinationId(),
                                            e.getDepartTime(),
                                            e.getArriveTime(),
                                            e.getStewards(),
                                            e.getAirlineId()))
                .collect(Collectors.toList());
    }
}
