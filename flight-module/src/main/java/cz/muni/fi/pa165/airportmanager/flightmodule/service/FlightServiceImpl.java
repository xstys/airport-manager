package cz.muni.fi.pa165.airportmanager.flightmodule.service;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.StewardChangeDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import java.util.*;

@Service
public class FlightServiceImpl
        extends DomainServiceImpl<Flight, FlightInsertDto,
                                  FlightUpdateDto, FlightFindDto>
        implements FlightService {
    public static final String STEWARD_IS_DUPLICATED_IN_THE_LIST = "A steward is duplicated in the list.";
    private final FlightRepository flightRepository;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository,
                             DomainMapper<Flight, FlightInsertDto,
                                          FlightUpdateDto,
                                          FlightFindDto> mapper) {
        super(flightRepository, mapper);
        this.flightRepository = flightRepository;
    }

    @Override
    public UUID addEntity(FlightInsertDto entity) {
        if (entityCheck(entity)) {
            return super.addEntity(entity);
        }

        throw new InvalidUserInputException("Invalid flight was provided!");
    }

    @Override
    public void updateEntity(FlightUpdateDto entity) {
        if (entityCheck(entity)) {
            super.updateEntity(entity);
        }
    }

    @Override
    public void addSteward(StewardChangeDto steward) {
        FlightFindDto foundFlight = this.findById(steward.getFlight());
        List<UUID> newStewards = foundFlight.getStewards();

        if (newStewards.contains(steward.getSteward())) {
            throw new InvalidUserInputException(
                    "Cannot assign steward to the same flight twice.");
        }

        if (newStewards.size() == 5) {
            throw new InvalidUserInputException(
                    "There cannot be more than 5 stewards.");
        }

        checkStewardAvailability(foundFlight, steward.getSteward());

        newStewards.add(steward.getSteward());

        checkStewardUniqueness(newStewards);

        FlightUpdateDto updateFlight = new FlightUpdateDto(
                foundFlight.getGuid(),
                foundFlight.getAirlineId(),
                foundFlight.getPlaneId(),
                foundFlight.getOriginId(),
                foundFlight.getDestinationId(),
                foundFlight.getDepartTime(),
                foundFlight.getArriveTime(),
                newStewards
        );

        this.updateEntity(updateFlight);
    }

    @Override
    public void removeSteward(StewardChangeDto steward) {
        FlightFindDto foundFlight = this.findById(steward.getFlight());
        List<UUID> newStewards = foundFlight.getStewards();

        if (newStewards.size() == 2) {
            throw new InvalidUserInputException(
                    "There cannot be less than 2 stewards.");
        }

        newStewards.remove(steward.getSteward());

        checkStewardUniqueness(newStewards);

        FlightUpdateDto updateFlight = new FlightUpdateDto(
                foundFlight.getGuid(),
                foundFlight.getAirlineId(),
                foundFlight.getPlaneId(),
                foundFlight.getOriginId(),
                foundFlight.getDestinationId(),
                foundFlight.getDepartTime(),
                foundFlight.getArriveTime(),
                newStewards
        );

        this.updateEntity(updateFlight);
    }

    private void checkStewardUniqueness(List<UUID> newStewards) {
        if (new HashSet<>(newStewards).size() != newStewards.size()) {
            throw new InvalidUserInputException(
                    STEWARD_IS_DUPLICATED_IN_THE_LIST);
        }
    }

    @Override
    public List<FlightFindDto> getEntities() {
        return super.getEntities().stream()
                .sorted(Comparator.comparing(FlightFindDto::getDepartTime)
                        .reversed())
                .toList();
    }

    public Map<String, Object> selectFlights(int page, int size,
                                             String order) {
        ZonedDateTime currentTime = ZonedDateTime.now()
                .withZoneSameInstant(ZoneOffset.UTC);

        return selectFlights(page, size, order, currentTime);
    }

    public Map<String, Object> selectFlights(int page, int size, String order,
                                             ZonedDateTime departTime) {
        if (size <= 0) {
            throw new InvalidUserInputException("Invalid page size");
        }

        ZonedDateTime currentTime = departTime
                .withZoneSameInstant(ZoneOffset.UTC);

        Pageable pageable;
        if (order.equals("newest")) {
            pageable = PageRequest.of(page, size, Sort.by("departTime")
                    .descending());
        } else {
            pageable = PageRequest.of(page, size, Sort.by("departTime")
                    .ascending());
        }

        Page<Flight> flights = flightRepository
                .findFlightsFromDate(currentTime, pageable);

        Map<String, Object> response = new HashMap<>();

        response.put("flights", domainMapper.map(flights.getContent()));
        response.put("currentPage", page);
        response.put("totalFlights", flights.getTotalElements());
        response.put("totalPages", flights.getTotalPages());

        return response;
    }

    public List<FlightFindDto> getSubsequentFlights(UUID id) {
        var flight = flightRepository.findById(id);

        if (flight.isEmpty()) {
            throw new InvalidUserInputException("This flight does not exist.");
        }

        var value = flight.get();
        var start = value.getArriveTime().plus(Duration.ofHours(1));
        var end = value.getArriveTime().plus(Duration.ofHours(5));
        var flights = flightRepository
                .findSubsequentFlights(start, end, value.getDestinationId());

        return super.domainMapper.map(flights.stream().toList());
    }

    @Override
    public List<FlightFindDto> selectInterferingFlightsFromAirline(UUID airlineID,
                                                                   ZonedDateTime startTime,
                                                                   ZonedDateTime endTime) {
        //Sanitize the datetime
        startTime = startTime.withZoneSameInstant(ZoneOffset.UTC);
        endTime = endTime.withZoneSameInstant(ZoneOffset.UTC);

        var flights = flightRepository
                .findInterferingFlightsFromAirline(airlineID, startTime,
                                                   endTime);

        return domainMapper.map(flights.stream().toList());
    }

    private boolean entityCheck(FlightInsertDto flight) {
        if (flight == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (flight.getClass() != FlightInsertDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct Flight class!");
        }

        if (flight.getPlaneId() == null) {
            throw new InvalidUserInputException(
                    "Plane GUID needs to be provided.");
        }

        if (flight.getOriginId() == null) {
            throw new InvalidUserInputException(
                    "Origin GUID needs to be provided.");
        }

        if (flight.getDestinationId() == null) {
            throw new InvalidUserInputException(
                    "Destination GUID needs to be provided.");
        }

        if (flight.getDepartTime() == null) {
            throw new InvalidUserInputException(
                    "Depart time needs to be provided.");
        }

        if (flight.getArriveTime() == null) {
            throw new InvalidUserInputException(
                    "Arrive time needs to be provided.");
        }

        if (flight.getStewards() == null) {
            throw new InvalidUserInputException(
                    "Array of stewards needs to be initialized.");
        }

        if (flight.getOriginId().equals(flight.getDestinationId())) {
            throw new InvalidUserInputException(
                    "The origin and destination of the flight are the same.");
        }

        if (flight.getDepartTime().isAfter(flight.getArriveTime())
                || flight.getDepartTime().isEqual(flight.getArriveTime())) {
            throw new InvalidUserInputException(
                    "The depart time needs to happen sooner than the arrive time.");
        }

        if (flight.getStewards().size() > 5) {
            throw new InvalidUserInputException("Too many stewards " +
                    "specified, maximal allowed number of stewards is 5.");
        }

        if (flight.getStewards().size() < 2) {
            throw new InvalidUserInputException(
                    "There need to be at least two stewards on board.");
        }

        if (checkFlightCollides(flight)) {
            throw new InvalidUserInputException(
                    "The plane has another flight scheduled between the " +
                            "specified times.");
        }

        if (new HashSet<>(flight.getStewards()).size()
                != flight.getStewards().size()) {
            throw new InvalidUserInputException(
                    STEWARD_IS_DUPLICATED_IN_THE_LIST);
        }

        checkStewardAvailability(flight);

        return true;
    }

    private boolean entityCheck(FlightUpdateDto flight) {
        if (flight == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (flight.getClass() != FlightUpdateDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct Flight class!");
        }

        if (flight.getPlaneId() == null) {
            throw new InvalidUserInputException(
                    "Plane GUID needs to be provided.");
        }

        if (flight.getOriginId() == null) {
            throw new InvalidUserInputException(
                    "Origin GUID needs to be provided.");
        }

        if (flight.getDestinationId() == null) {
            throw new InvalidUserInputException(
                    "Destination GUID needs to be provided.");
        }

        if (flight.getDepartTime() == null) {
            throw new InvalidUserInputException(
                    "Depart time needs to be provided.");
        }

        if (flight.getArriveTime() == null) {
            throw new InvalidUserInputException(
                    "Arrive time needs to be provided.");
        }

        if (flight.getStewards() == null) {
            throw new InvalidUserInputException(
                    "Array of stewards needs to be initialized.");
        }

        if (flight.getOriginId().equals(flight.getDestinationId())) {
            throw new InvalidUserInputException(
                    "The origin and destination of the flight are the same.");
        }

        if (flight.getDepartTime().isAfter(flight.getArriveTime())
                || flight.getDepartTime().isEqual(flight.getArriveTime())) {
            throw new InvalidUserInputException(
                    "The depart time needs to happen sooner than the " +
                            "arrive time.");
        }

        if (flight.getStewards().size() > 5) {
            throw new InvalidUserInputException("Too many stewards " +
                    "specified, maximal allowed number of stewards is 5.");
        }

        if (flight.getStewards().size() < 2) {
            throw new InvalidUserInputException(
                    "There need to be at least two stewards on board.");
        }

        if (checkFlightCollides(flight)) {
            throw new InvalidUserInputException(
                    "The plane has another flight scheduled between the specified times.");
        }

        if (new HashSet<>(flight.getStewards()).size()
                != flight.getStewards().size()) {
            throw new InvalidUserInputException(
                    STEWARD_IS_DUPLICATED_IN_THE_LIST);
        }

        checkStewardAvailability(flight);

        return true;
    }

    private void checkStewardAvailability(FlightInsertDto flight) {
        for (var id : flight.getStewards()) {
            if (!flightRepository.isStewardAvailable(flight.getDepartTime(),
                                                     flight.getArriveTime(),
                                                     id)) {
                throw new InvalidUserInputException(
                        constructStewardUnavailableMessage(id));
            }
        }
    }

    private void checkStewardAvailability(FlightFindDto flight, UUID steward) {
        if (!flightRepository.isStewardAvailable(flight.getDepartTime(),
                                                 flight.getArriveTime(),
                                                 steward)) {
            throw new InvalidUserInputException(
                    constructStewardUnavailableMessage(steward));
        }
    }

    private void checkStewardAvailability(FlightUpdateDto flight) {
        for (var id : flight.getStewards()) {
            if (!flightRepository.isStewardAvailable(flight.getDepartTime(),
                                                     flight.getArriveTime(),
                                                     id, flight.getGuid())) {
                throw new InvalidUserInputException(
                        constructStewardUnavailableMessage(id));
            }
        }
    }

    private String constructStewardUnavailableMessage(UUID id) {
        return "The steward with id: " + id +
                " is not available for this flight.";
    }

    private boolean checkFlightCollides(FlightInsertDto flight) {
        List<FlightFindDto> samePlane = this.getEntities().stream()
                .filter(e -> e.getPlaneId().equals(flight.getPlaneId()))
                .toList();
        for (FlightFindDto f : samePlane) {
            if ((f.getDepartTime().isEqual(flight.getDepartTime()))
                    || (f.getDepartTime().isEqual(flight.getArriveTime()))
                    || (f.getArriveTime().isEqual(flight.getDepartTime()))
                    || (f.getArriveTime().isEqual(flight.getArriveTime()))
                    || (!(f.getArriveTime().isBefore(flight.getDepartTime())
                    || f.getDepartTime().isAfter(flight.getArriveTime())))) {
                return true;
            }
        }

        return false;
    }

    private boolean checkFlightCollides(FlightUpdateDto flight) {
        List<FlightFindDto> samePlane = this.getEntities().stream()
                .filter(e -> e.getPlaneId().equals(flight.getPlaneId()) &&
                        !(e.getGuid().equals(flight.getGuid()))).toList();
        for (FlightFindDto f : samePlane) {
            if ((f.getDepartTime().isEqual(flight.getDepartTime()))
                    || (f.getDepartTime().isEqual(flight.getArriveTime()))
                    || (f.getArriveTime().isEqual(flight.getDepartTime()))
                    || (f.getArriveTime().isEqual(flight.getArriveTime()))
                    || (!(f.getArriveTime().isBefore(flight.getDepartTime())
                    || f.getDepartTime().isAfter(flight.getArriveTime())))) {
                return true;
            }
        }

        return false;
    }

    public FlightFindDto findLastCompletedFlightOfPlaneBeforeDate(UUID planeId,
                                                                  ZonedDateTime date) {
        Flight flight = flightRepository
                .findLastCompletedFlightOfPlaneBeforeDate(date, planeId);
        return super.domainMapper.map(flight);
    }
}
