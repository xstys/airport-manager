package cz.muni.fi.pa165.airportmanager.flightmodule.data.repository;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface FlightRepository extends DomainRepository<Flight> {
    @Query("SELECT f from Flight f WHERE f.departTime>=?1 AND " +
            "f.deleted IS NULL")
    Page<Flight> findFlightsFromDate(ZonedDateTime date, Pageable pageable);

    @Query("SELECT f from Flight f WHERE f.originId =?3 AND " +
            "f.deleted IS NULL AND f.departTime >=?1 AND f.departTime < ?2")
    Collection<Flight> findSubsequentFlights(ZonedDateTime dateBegin,
                                             ZonedDateTime dateEnd,
                                             UUID destinationId);

    /**
     * Gets flights from provided airline that time-wise interfere (they are happening in this time window) with provided time window
     *
     * @param airlineId ID of the airline to filter flights on
     * @param dateBegin start datetime of the time window
     * @param dateEnd   end datetime of the time window
     * @return List of flights from a provided airline that interfere with provided time window
     */
    @Query("SELECT f from Flight f WHERE f.airlineId =?1 AND " +
            "f.deleted IS NULL AND (" +
            "(f.arriveTime >= ?2 AND f.arriveTime <= ?3) OR " +
            "(f.departTime >= ?2 AND f.departTime <= ?3) OR " +
            "(f.departTime <= ?2 AND f.arriveTime >= ?3))"
    )
    Collection<Flight> findInterferingFlightsFromAirline(UUID airlineId,
                                                         ZonedDateTime dateBegin,
                                                         ZonedDateTime dateEnd);

    @Query("SELECT count(f) = 0 from Flight f WHERE " +
            "?3 MEMBER OF f.stewards AND f.deleted IS NULL AND " +
            "((f.departTime BETWEEN ?1 AND ?2) OR " +
            "(f.arriveTime BETWEEN ?1 AND ?2))")
    boolean isStewardAvailable(ZonedDateTime departTime,
                               ZonedDateTime arrivalTime,
                               UUID steward);

    @Query("SELECT count(f) = 0 from Flight f WHERE " +
            "?3 MEMBER OF f.stewards AND f.deleted IS NULL AND " +
            "((f.departTime BETWEEN ?1 AND ?2) OR " +
            "(f.arriveTime BETWEEN ?1 AND ?2)) AND ?4 != f.id")
    boolean isStewardAvailable(ZonedDateTime departTime,
                               ZonedDateTime arrivalTime,
                               UUID steward, UUID flight);

    @Query(value = "SELECT f FROM Flight f WHERE f.deleted IS NULL AND " +
            "f.arriveTime <= ?1 AND f.planeId = ?2 " +
            "ORDER BY f.arriveTime DESC LIMIT 1")
    Flight findLastCompletedFlightOfPlaneBeforeDate(ZonedDateTime date,
                                                    UUID plane);
}
