package cz.muni.fi.pa165.airportmanager.flightmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ServerDownException;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;

import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.UUID;

@Service
public class AirlineServiceImpl
        extends RetrieveEntityServiceImpl<AirlineFindDto>
        implements RetrieveEntityService<AirlineFindDto> {
    public AirlineServiceImpl(@Value("${airline.url}") String destinationUrl,
                              @Value("${airline.endpoint}") String destinationEndpoint) {
        super(destinationUrl, destinationEndpoint);
    }

    @Override
    public AirlineFindDto retrieve(UUID id, String authenticationHeader) {
        try {
            return retrieveResponseSpec(id, authenticationHeader)
                    .bodyToMono(AirlineFindDto.class)
                    .block();
        }  catch (WebClientResponseException e) {
            if (e.getStatusCode() == HttpStatusCode.valueOf(404)) {
                throw new ObjectNotFoundException("Airline ID not found.");
            }

            if (e.getStatusCode() == HttpStatusCode.valueOf(400)) {
                throw new InvalidUserInputException(
                        "Invalid airline ID was provided.");
            }

            throw new ServerDownException(
                    "Unexpected error while communicating " +
                            "with the airline server.");
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Airline server is down.");
        }
    }
}
