package cz.muni.fi.pa165.airportmanager.flightmodule.facade;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightDetailDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.StewardChangeDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.service.*;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.*;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacadeImpl;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveMultipleEntitiesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class FlightFacadeImpl
        extends DomainFacadeImpl<Flight, FlightInsertDto,
                                 FlightUpdateDto, FlightFindDto>
        implements FlightFacade {
    private final RetrieveEntityService<DestinationFindDto> destinationService;
    private final RetrieveEntityService<AirlineFindDto> airlineService;
    private final RetrieveEntityService<PlaneFindDto> planeService;
    private final RetrieveMultipleEntitiesService<StewardFindDto> stewardService;

    @Autowired
    public FlightFacadeImpl(FlightService flightService,
                            RetrieveEntityService<DestinationFindDto> destinationService,
                            RetrieveEntityService<AirlineFindDto> airlineService,
                            RetrieveEntityService<PlaneFindDto> planeService,
                            RetrieveMultipleEntitiesService<StewardFindDto> stewardService) {
        super(flightService);
        this.destinationService = destinationService;
        this.airlineService = airlineService;
        this.planeService = planeService;
        this.stewardService = stewardService;
    }

    @Override
    public FlightDetailDto getDetailDto(UUID id, String token) {
        FlightFindDto flightFindDto = domainService.findById(id);

        if (flightFindDto == null) {
            throw new InvalidUserInputException("Invalid id.");
        }

        DestinationFindDto originDto = destinationService
                .retrieve(flightFindDto.getOriginId(), token);
        DestinationFindDto destinationDto = destinationService
                .retrieve(flightFindDto.getDestinationId(), token);
        List<StewardFindDto> stewardDtos = stewardService
                .retrieve(flightFindDto.getStewards(), token);
        PlaneFindDto planeDto = planeService
                .retrieve(flightFindDto.getPlaneId(), token);

        if (planeDto == null) {
            throw new InvalidUserInputException(
                    "Plane not connected to an airline.");
        }

        AirlineFindDto airlineDto = airlineService
                .retrieve(flightFindDto.getAirlineId(), token);

        if (originDto == null || destinationDto == null
                || stewardDtos == null || airlineDto == null) {
            throw new InvalidUserInputException("Invalid flight.");
        }

        return new FlightDetailDto(flightFindDto.getGuid(), airlineDto,
                                   planeDto, originDto, destinationDto,
                                   flightFindDto.getDepartTime(),
                                   flightFindDto.getArriveTime(), stewardDtos);
    }

    @Override
    public void addSteward(StewardChangeDto steward, String token) {
        if (stewardService.retrieve(steward.getSteward(), token) == null) {
            throw new InvalidUserInputException("Unknown steward provided.");
        }

        ((FlightService) super.domainService).addSteward(steward);
    }

    @Override
    public void removeSteward(StewardChangeDto steward, String token) {
        if (stewardService.retrieve(steward.getSteward(), token) == null) {
            throw new InvalidUserInputException("Unknown steward provided.");
        }

        ((FlightService) super.domainService).removeSteward(steward);
    }

    @Override
    public List<FlightFindDto> getSubsequentFlights(UUID id) {
        return ((FlightService) super.domainService).getSubsequentFlights(id);
    }

    public UUID addEntity(FlightInsertDto insertDto, String token) {
        checkEntity(insertDto, token);
        return super.addEntity(insertDto);
    }

    private void checkEntity(FlightInsertDto insertDto, String token) {
        if (insertDto == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (planeService.retrieve(insertDto.getPlaneId(), token) == null) {
            throw new ObjectNotFoundException("Unknown plane was provided");
        }

        if (destinationService.retrieve(insertDto.getOriginId(), token)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown origin destination was provided");
        }

        if (destinationService.retrieve(insertDto.getDestinationId(), token)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown target destination was provided");
        }

        List<StewardFindDto> stewards = stewardService
                .retrieve(insertDto.getStewards(), token);
        checkCorrectArguments(token, stewards, insertDto.getStewards(),
                              insertDto.getAirlineId(),
                              insertDto.getPlaneId());
    }

    public void updateEntity(FlightUpdateDto updateDto, String token) {
        checkEntity(updateDto, token);
        super.updateEntity(updateDto);
    }

    private void checkEntity(FlightUpdateDto updateDto, String token) {
        if (updateDto == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (planeService.retrieve(updateDto.getPlaneId(), token) == null) {
            throw new ObjectNotFoundException("Unknown plane was provided");
        }

        if (destinationService.retrieve(updateDto.getOriginId(), token)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown origin destination was provided");
        }

        if (destinationService.retrieve(updateDto.getDestinationId(), token)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown target destination was provided");
        }

        List<StewardFindDto> stewards = (stewardService
                .retrieve(updateDto.getStewards(), token));
        checkCorrectArguments(token, stewards, updateDto.getStewards(),
                              updateDto.getAirlineId(),
                              updateDto.getPlaneId());
    }

    private void checkCorrectArguments(String token,
                                       List<StewardFindDto> stewards,
                                       List<UUID> stewards2,
                                       UUID airlineId, UUID planeId) {
        if (stewards2.size() != (stewards.size())) {
            throw new ObjectNotFoundException("Unknown or duplicate steward was provided");
        }

        if  (airlineService.retrieve(airlineId, token) == null) {
            throw new ObjectNotFoundException("Unknown airline was provided");
        }

        if (!(airlineService.retrieve(airlineId, token).getGuid()
                .equals(planeService.retrieve(planeId, token)
                        .getAirlineId()
                ))) {
            throw new InvalidUserInputException(
                    "Plane does not belong to the flight airline.");
        }
    }
}
