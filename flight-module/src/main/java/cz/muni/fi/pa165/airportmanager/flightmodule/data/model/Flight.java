package cz.muni.fi.pa165.airportmanager.flightmodule.data.model;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="FLIGHT")
public class Flight extends DomainEntity {

    @Column(name="AIRLINE_ID", nullable=false)
    private UUID airlineId;
    @Column(name="PLANE_ID", nullable=false)
    private UUID planeId;
    @Column(name="ORIGIN_ID", nullable=false)
    private UUID originId;
    @Column(name="DESTINATION_ID", nullable=false)
    private UUID destinationId;
    @Column(name="DEPART_TIME", nullable=false)
    private ZonedDateTime departTime;
    @Column(name="ARRIVE_TIME", nullable=false)
    private ZonedDateTime arriveTime;
    @ElementCollection
    @Column(name="STEWARDS", nullable=false)
    private List<UUID> stewards;

    public Flight() {
        super(null);
    }

    public Flight(UUID guid, UUID airlineId, UUID planeId, UUID originId,
                  UUID destinationId, ZonedDateTime departTime,
                  ZonedDateTime arriveTime) {
        super(guid);
        this.airlineId = airlineId;
        this.planeId = planeId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = new ArrayList<>();
    }

    public Flight(UUID airlineId, UUID planeId, UUID originId,
                  UUID destinationId, ZonedDateTime departTime,
                  ZonedDateTime arriveTime) {
        super(null);
        this.airlineId = airlineId;
        this.planeId = planeId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = new ArrayList<>();
    }

    public Flight(UUID guid, UUID airlineId, UUID planeId, UUID originId,
                  UUID destinationId, ZonedDateTime departTime,
                  ZonedDateTime arriveTime, List<UUID> stewards) {
        super(guid);
        this.airlineId = airlineId;
        this.planeId = planeId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = stewards;
    }

    public Flight(UUID airlineId, UUID planeId, UUID originId,
                  UUID destinationId, ZonedDateTime departTime,
                  ZonedDateTime arriveTime, List<UUID> stewards) {
        super(null);
        this.airlineId = airlineId;
        this.planeId = planeId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = stewards;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(UUID airlineId) {
        this.airlineId = airlineId;
    }

    public UUID getPlaneId() {
        return planeId;
    }

    public void setPlaneId(UUID planeId) {
        this.planeId = planeId;
    }

    public UUID getOriginId() {
        return originId;
    }

    public void setOriginId(UUID originId) {
        this.originId = originId;
    }

    public UUID getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(UUID destinationId) {
        this.destinationId = destinationId;
    }

    public ZonedDateTime getDepartTime() {
        return departTime;
    }

    public void setDepartTime(ZonedDateTime departTime) {
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
    }

    public ZonedDateTime getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(ZonedDateTime arriveTime) {
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
    }

    public List<UUID> getStewards() {
        return stewards;
    }

    public void setStewards(List<UUID> stewards) {
        this.stewards = stewards;
    }
}