package cz.muni.fi.pa165.airportmanager.flightmodule.facade;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightDetailDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.StewardChangeDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;

import java.util.List;
import java.util.UUID;


public interface FlightFacade
        extends DomainFacade<Flight, FlightInsertDto,
                             FlightUpdateDto, FlightFindDto> {
    FlightDetailDto getDetailDto(UUID id, String token);

    List<FlightFindDto> getSubsequentFlights(UUID id);

    void addSteward(StewardChangeDto steward, String token);

    void removeSteward(StewardChangeDto steward, String token);
}
