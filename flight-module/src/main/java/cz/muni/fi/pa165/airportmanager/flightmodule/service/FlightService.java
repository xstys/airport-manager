package cz.muni.fi.pa165.airportmanager.flightmodule.service;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.StewardChangeDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainService;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface FlightService
        extends DomainService<Flight, FlightInsertDto,
                              FlightUpdateDto, FlightFindDto> {
    Map<String, Object> selectFlights(int page, int size, String order);
    Map<String, Object> selectFlights(int page, int size, String order,
                                      ZonedDateTime currentTime);
    List<FlightFindDto> getSubsequentFlights(UUID id);
    void addSteward(StewardChangeDto steward);
    void removeSteward(StewardChangeDto steward);
    List<FlightFindDto> selectInterferingFlightsFromAirline(UUID airlineID,
                                                            ZonedDateTime startTime,
                                                            ZonedDateTime endTime);
    FlightFindDto findLastCompletedFlightOfPlaneBeforeDate(UUID planeID,
                                                           ZonedDateTime date);
}

