package cz.muni.fi.pa165.airportmanager.flightmodule.rest;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.rest.DomainController;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightDetailDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightInsertDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.FlightUpdateDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.dto.StewardChangeDto;
import cz.muni.fi.pa165.airportmanager.flightmodule.facade.FlightFacade;
import cz.muni.fi.pa165.airportmanager.flightmodule.facade.FlightFacadeImpl;
import cz.muni.fi.pa165.airportmanager.flightmodule.service.FlightService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@OpenAPIDefinition( // metadata for inclusion into OpenAPI document
        // see javadoc at https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
        // see docs at https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(title = "Flight Service",
                version = "0.1",
                description = """
                        Simple service for flight handling at an airport.
                        The API has operations for:
                        - getting all flights from now
                        - getting all flights with departure after given datetime
                        - adding a new flight
                        - getting a specific flight by its id
                        - removing a specific flight by its id
                        """,
                contact = @Contact(name = "Tereza Vrabcova",
                        email = "485431@mail.muni.cz",
                        url = "https://is.muni.cz/auth/osoba/" +
                                "485431"),
                license = @License(name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/" +
                                "LICENSE-2.0.html")
        ),
        servers = @Server(description = "localhost server",
                url = "{scheme}://{server}:{port}",
                variables = {
                        @ServerVariable(name = "scheme",
                                allowableValues = {"http", "https"},
                                defaultValue = "http"),
                        @ServerVariable(name = "server",
                                defaultValue = "localhost"),
                        @ServerVariable(name = "port",
                                defaultValue = "5003"),
                })
)
@Tag(name = "Flight", description = "microservice for flight handling") // metadata for inclusion into OpenAPI document
@RequestMapping(path = "api/v1/", produces = APPLICATION_JSON_VALUE)
// common prefix for all URLs handled by this controller
public class FlightController
        extends DomainController<Flight, FlightInsertDto,
        FlightUpdateDto, FlightFindDto> {
    private final FlightService flightService;

    @Autowired
    public FlightController(FlightFacade facade, FlightService flightService) {
        super(facade);
        this.flightService = flightService;
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Add a new flight",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully added"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Adds a provided flight and returns its ID
                    in the UUID/GUID format.
                    """)
    @PostMapping("flights") // URL mapping of this operation
    public UUID addEntity(@RequestHeader(name = "Authorization", required = false)
                          @Schema(hidden = true) String authorizationHeader, FlightInsertDto insertDto) {
        return ((FlightFacadeImpl) facade).addEntity(insertDto, authorizationHeader);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Changes an existing flight",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully modified"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Changes an existing flight with the values
                    from provided flight.
                    """)
    @PatchMapping("flights")
    public void updateEntity(@RequestHeader(name = "Authorization", required = false) @Schema(hidden = true)
                             String authorizationHeader, FlightUpdateDto updateDto) {
        ((FlightFacadeImpl) facade).updateEntity(updateDto, authorizationHeader);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Removes an existing flight",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully deleted"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Removes an existing flight with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
            schema = @Schema(type = "string"))
    @DeleteMapping("flights/{id}")
    public void delete(@Valid @Parameter(hidden = true)
                       @PathVariable UUID id) {
        super.delete(id);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing flight",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Retrieves an existing flight with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
            schema = @Schema(type = "string"))
    @GetMapping("flights/{id}")
    public FlightFindDto findById(@Valid @Parameter(hidden = true)
                                  @PathVariable UUID id) {
        return super.findById(id);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing flight with details",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Retrieves an existing flight with
                    the specified ID and its details.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
            schema = @Schema(type = "string"))
    @GetMapping("flights/detail/{id}")
    public FlightDetailDto findDetailedById(@RequestHeader(name = "Authorization", required = false)
                                            @Schema(hidden = true) String authorizationHeader,
                                            @Valid @Parameter(hidden = true) @PathVariable UUID id) {
        return ((FlightFacade) facade).getDetailDto(id, authorizationHeader);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all flights after departure Time",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flights were " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Returns an array of objects
                    representing flights with departure
                    time after or equal to given DateTime.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "time",
            schema = @Schema(type = "string"))
    @GetMapping("flights/time/{time}")
    public Map<String, Object> selectFlights(@Valid @Schema(defaultValue = "0") int page,
                                             @Valid @Schema(defaultValue = "5") int size,
                                             @Valid @Schema(allowableValues = {"oldest", "newest"}) String order,
                                             @Valid @PathVariable ZonedDateTime time) {
        return flightService.selectFlights(page, size, order, time);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all flights from now",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flights were " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Returns an array of objects
                    representing flights with departure
                    time after or equal to current time.
                    """)
    @GetMapping("flights")
    public Map<String, Object> selectFlights(@Valid @Schema(defaultValue = "0") int page,
                                             @Valid @Schema(defaultValue = "5") int size,
                                             @Valid @Schema(allowableValues = {"oldest", "newest"}) String order) {
        return flightService.selectFlights(page, size, order);
    }

    @Operation(
            summary = "Get subsequent flights for selected flight.",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flights were " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = "Subsequent flight is a flight " +
                    "that is departing from" +
                    "the place the last one arrived to " +
                    "in 5 hours the latest"
    )
    @GetMapping("flights/subsequent/{id}")
    public List<FlightFindDto> findSubsequentFlights(@Valid @PathVariable UUID id) {
        return ((FlightFacade) facade).getSubsequentFlights(id);
    }

    @Operation(
            summary = "Add a steward to a flight.",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - steward was successfully " +
                                    "added to a flight"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token does " +
                                    "not have scope test_1",
                            content = @Content()),
            },
            description = "Adds a steward to an existing flight."
    )
    @PostMapping("flights/stewards/")
    public void addStewardToAFlight(@RequestHeader(name = "Authorization", required = false) @Schema(hidden = true)
                                    String authorizationHeader, @RequestBody StewardChangeDto steward) {
        ((FlightFacade) facade).addSteward(steward, authorizationHeader);
    }

    @Operation(
            summary = "Remove a steward from a flight.",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - steward was successfully " +
                                    "removed from a flight"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = "Removes a steward from an existing flight."
    )
    @DeleteMapping("flights/stewards/")
    public void removeStewardFromAFlight(@RequestHeader(name = "Authorization", required = false) @Schema(hidden = true)
                                         String authorizationHeader, @RequestBody StewardChangeDto steward) {
        ((FlightFacade) facade).removeSteward(steward, authorizationHeader);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get flights from a specified airline that" +
                    "interfere with specified time period",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flights were " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token does " +
                                    "not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Returns an array of objects representing flights
                    from a specified airline that interfere
                    with the given time period.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "airlineID",
            schema = @Schema(type = "string"))
    @Parameter(in = ParameterIn.QUERY, name = "timeStart",
            schema = @Schema(type = "string"))
    @Parameter(in = ParameterIn.QUERY, name = "timeEnd",
            schema = @Schema(type = "string"))
    @GetMapping("flights/airlines/{airlineId}/between")
    public List<FlightFindDto> selectInterferingFlightsFromAirline(@Valid @PathVariable UUID airlineId,
                                                                   @Valid @RequestParam ZonedDateTime timeStart,
                                                                   @Valid @RequestParam ZonedDateTime timeEnd) {
        return flightService.selectInterferingFlightsFromAirline(airlineId, timeStart, timeEnd);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get last flight of an airplane that finishes before specified date",
            security = @SecurityRequirement(name = "Bearer",
                    scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "OK - flight was " +
                                    "successfully returned"),
                    @ApiResponse(responseCode = "401",
                            description = "Unauthorized - access token " +
                                    "not provided or valid",
                            content = @Content()),
                    @ApiResponse(responseCode = "403",
                            description = "Forbidden - access token " +
                                    "does not have scope test_1",
                            content = @Content()),
            },
            description = """
                    Returns last flight of specified plane
                    that finishes before specified date.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "planeId",
            schema = @Schema(type = "string"))
    @Parameter(in = ParameterIn.QUERY, name = "date",
            schema = @Schema(type = "string"))
    @GetMapping("flights/last/{planeId}/before")
    public FlightFindDto getLastFlightOfPlaneBeforeDate(@Valid @PathVariable UUID planeId,
                                                        @Valid @RequestParam ZonedDateTime date) {
        return flightService.findLastCompletedFlightOfPlaneBeforeDate(planeId, date);
    }
}
