package cz.muni.fi.pa165.airportmanager.flightmodule.seed;

import cz.muni.fi.pa165.airportmanager.flightmodule.data.model.Flight;
import cz.muni.fi.pa165.airportmanager.flightmodule.data.repository.FlightRepository;

import org.javatuples.Pair;
import org.javatuples.Septet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@Profile("dev")
public class FlightSeed {
    private final FlightRepository flightRepository;

    @Autowired
    public FlightSeed(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
        seed();
    }

    private void seed() {
        List<Septet<String, String, String, String,
                    Pair<Integer, Integer>, Integer,
                    List<String>>> flights = List.of(
                Septet.with("Mom and Pop Airlines", "Fokker 50",
                        "Vienna, Austria", "Prague, Czech Republic",
                        Pair.with(0, 50), 3,
                        List.of("Antonín, Opustil",
                                "Markéta, Opustilová")),
                Septet.with("Mom and Pop Airlines", "Fokker 50",
                        "Prague, Czech Republic", "Vienna, Austria",
                        Pair.with(0, 50), 5,
                        List.of("Antonín, Opustil",
                                "Markéta, Opustilová")),
                Septet.with("Czexpats Airlines", "Airbus A320",
                        "Prague, Czech Republic", "Athens, Greece",
                        Pair.with(2, 30), 1,
                        List.of("Albert, Vykukal",
                                "Sára, Daňková")),
                Septet.with("Czexpats Airlines", "Airbus A321",
                        "Athens, Greece", "Prague, Czech Republic",
                        Pair.with(2, 30), 1,
                        List.of("Julie, Hrdličková",
                                "Dušan, Opustil")),
                Septet.with("Czexpats Airlines", "Airbus A320",
                        "Prague, Czech Republic", "Vienna, Austria",
                        Pair.with(0, 50), 8,
                        List.of("Julie, Hrdličková",
                                "Dušan, Opustil",
                                "Viktor, Král")),
                Septet.with("Czexpats Airlines", "Airbus A320",
                        "Vienna, Austria", "Athens, Greece",
                        Pair.with(2, 10), 10,
                        List.of("Julie, Hrdličková",
                                "Viktor, Král"))
        );

        for (Septet<String, String, String, String,
                    Pair<Integer, Integer>, Integer,
                    List<String>> f: flights) {
            UUID airlineId = UUID.nameUUIDFromBytes(f.getValue0().getBytes());
            UUID planeId = UUID.nameUUIDFromBytes(
                    (f.getValue1() + ", " + f.getValue0()).getBytes());
            UUID originId = UUID.nameUUIDFromBytes(f.getValue2().getBytes());
            UUID destId = UUID.nameUUIDFromBytes(f.getValue3().getBytes());

            ZonedDateTime depart = ZonedDateTime.now()
                    .withZoneSameInstant(ZoneId.of("UTC"))
                    .plusHours(f.getValue5());
            ZonedDateTime arrive = ZonedDateTime.now()
                    .withZoneSameInstant(ZoneId.of("UTC"))
                    .plusHours(f.getValue5() + f.getValue4().getValue0())
                    .plusMinutes(f.getValue4().getValue1());

            List<UUID> stewardIds = new ArrayList<>();
            for (String s: f.getValue6()) {
                UUID stewardId = UUID.nameUUIDFromBytes(
                        (s + ", " + f.getValue0()).getBytes());
                stewardIds.add(stewardId);
            }

            UUID id = UUID.nameUUIDFromBytes(
                    (f.getValue0() + ", " + f.getValue1() + ", " + depart)
                    .getBytes());

            flightRepository.save(
                    new Flight(id, airlineId, planeId, originId, destId,
                               depart, arrive, stewardIds)
            );
        }
    }
}
