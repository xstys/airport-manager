package cz.muni.fi.pa165.airportmanager.flightmodule.dto;

import java.util.UUID;

public class StewardChangeDto {
    private final UUID steward;
    private final UUID flight;

    public StewardChangeDto(UUID steward, UUID flight) {
        this.steward = steward;
        this.flight = flight;
    }

    public UUID getSteward() {
        return steward;
    }

    public UUID getFlight() {
        return flight;
    }
}
