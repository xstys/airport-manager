package cz.muni.fi.pa165.airportmanager.flightmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainDetailDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

public class FlightDetailDto extends DomainDetailDto {
    private final PlaneFindDto plane;
    private final DestinationFindDto origin;
    private final DestinationFindDto destination;
    private final ZonedDateTime departTime;
    private final ZonedDateTime arriveTime;
    private final List<StewardFindDto> stewards;
    private final AirlineFindDto airline;

    public FlightDetailDto(UUID guid, AirlineFindDto airline,
                           PlaneFindDto plane, DestinationFindDto origin,
                           DestinationFindDto destination,
                           ZonedDateTime departTime, ZonedDateTime arriveTime,
                           List<StewardFindDto> stewards) {
        super(guid);
        this.plane = plane;
        this.origin = origin;
        this.destination = destination;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = stewards;
        this.airline = airline;
    }

    public AirlineFindDto getAirline() {
        return airline;
    }

    public PlaneFindDto getPlane() {
        return plane;
    }

    public DestinationFindDto getOrigin() {
        return origin;
    }

    public DestinationFindDto getDestination() {
        return destination;
    }

    public ZonedDateTime getDepartTime() {
        return departTime;
    }

    public ZonedDateTime getArriveTime() {
        return arriveTime;
    }

    public List<StewardFindDto> getStewards() {
        return stewards;
    }
}
