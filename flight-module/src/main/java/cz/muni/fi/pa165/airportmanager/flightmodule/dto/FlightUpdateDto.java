package cz.muni.fi.pa165.airportmanager.flightmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

public class FlightUpdateDto extends DomainUpdateDto {

    private UUID guid;
    private UUID airlineId;
    private UUID planeId;
    private UUID originId;
    private UUID destinationId;
    private ZonedDateTime departTime;
    private ZonedDateTime arriveTime;
    private List<UUID> stewards;

    public FlightUpdateDto(UUID guid, UUID airlineId, UUID planeId,
                           UUID originId, UUID destinationId,
                           ZonedDateTime departTime, ZonedDateTime arriveTime,
                           List<UUID> stewards) {
        super(guid);
        this.guid = guid;
        this.planeId = planeId;
        this.airlineId = airlineId;
        this.originId = originId;
        this.destinationId = destinationId;
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
        this.stewards = stewards;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(UUID airlineId) {
        this.airlineId = airlineId;
    }

    public UUID getPlaneId() {
        return planeId;
    }

    public void setPlaneId(UUID planeId) {
        this.planeId = planeId;
    }

    public UUID getOriginId() {
        return originId;
    }

    public void setOriginId(UUID originId) {
        this.originId = originId;
    }

    public UUID getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(UUID destinationId) {
        this.destinationId = destinationId;
    }

    public ZonedDateTime getDepartTime() {
        return departTime;
    }

    public void setDepartTime(ZonedDateTime departTime) {
        this.departTime = departTime.withZoneSameInstant(ZoneId.of("UTC"));
    }

    public ZonedDateTime getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(ZonedDateTime arriveTime) {
        this.arriveTime = arriveTime.withZoneSameInstant(ZoneId.of("UTC"));
    }

    public List<UUID> getStewards() {
        return stewards;
    }

    public void setStewards(List<UUID> stewards) {
        this.stewards = stewards;
    }
}