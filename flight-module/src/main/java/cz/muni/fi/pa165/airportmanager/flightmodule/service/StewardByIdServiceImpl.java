package cz.muni.fi.pa165.airportmanager.flightmodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ServerDownException;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveMultipleEntitiesService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveMultipleEntitiesByIdServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;

import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;
import java.util.UUID;

@Service
public class StewardByIdServiceImpl
        extends RetrieveMultipleEntitiesByIdServiceImpl<StewardFindDto>
        implements RetrieveMultipleEntitiesService<StewardFindDto> {
    public StewardByIdServiceImpl(@Value("${steward.url}") String stewardUrl,
                                  @Value("${steward.endpoint}") String stewardEndpoint,
                                  @Value("${steward.multiple.endpoint}") String stewardMultipleEndpoint) {
        super(stewardUrl, stewardEndpoint, stewardMultipleEndpoint);
    }

    @Override
    public List<StewardFindDto> retrieve(List<UUID> ids, String token) {
        try {
            return retrieveResponseSpec(ids, token)
                    .bodyToFlux(StewardFindDto.class)
                    .collectList()
                    .block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == HttpStatusCode.valueOf(404)) {
                throw new ObjectNotFoundException("Steward ID not found.");
            }

            if (e.getStatusCode() == HttpStatusCode.valueOf(400)) {
                throw new InvalidUserInputException(
                        "Invalid steward ID was provided.");
            }

            throw new ServerDownException(
                    "Unexpected error while communicating with the " +
                            "steward server.");
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Steward server is down.");
        }
    }

    @Override
    public StewardFindDto retrieve(UUID id, String authenticationHeader) {
        try {
            var webClient = WebClient.create();

            return webClient.get()
                    .uri(destinationUrl + destinationEndpoint + id)
                    .header(HttpHeaders.AUTHORIZATION, authenticationHeader)
                    .retrieve()
                    .bodyToMono(StewardFindDto.class)
                    .block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == HttpStatusCode.valueOf(404)) {
                throw new ObjectNotFoundException("Steward ID not found.");
            }

            if (e.getStatusCode() == HttpStatusCode.valueOf(400)) {
                throw new InvalidUserInputException(
                        "Invalid steward ID was provided.");
            }

            throw new ServerDownException(
                    "Unexpected error while communicating with the " +
                            "steward server.");
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Steward server is down.");
        }
    }
}
