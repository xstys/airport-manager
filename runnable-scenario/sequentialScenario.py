#!/usr/bin/env python3.11

"""
file: sequentialScenario.py
author: Tomas Tomala (541685)
description: This file contains sequential scenario for PA165 project.
project: https://gitlab.fi.muni.cz/xstys/airport-manager
testingtool: Locust https://locust.io/
! Before starting the test, make sure that all services are running, otherwise the test will fail!
! Next prerequisite is to login via localhost:8080 -> show your personal token on http://localhost:8080/token
! Copy the token and paste it to AUTHTOKEN variable below (Without "Token:")
! After that, you can start locust and run the test via website http://localhost:8089
usage: locust -f sequentialScenario.py --host=http://localhost:8089
"""

import json
import datetime
import random
import string
from locust import HttpUser, task, SequentialTaskSet
from locust.exception import StopUser
from urllib.parse import quote

AUTHTOKEN = ""

# Headers used for requests
heads = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + AUTHTOKEN
}
# Array of created destinations
destinations = []
# Created Airline
airlinesID = ""
# Created plane
planeID = ""
# Created Flight
flightID = ""
# Array of created stewards
stewards = []

# Dates used for flight
date1 = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) + \
    datetime.timedelta(days=random.randint(1, 100))
date2 = date1 + datetime.timedelta(days=random.randint(2, 5))

"""
Scenario:
- This scenario shows possible workflow of single airport manager. His job starts with creating a new airline company and registering destinations.
- After those are created, he registers planes and stewards, that will assigned to a flight.
- To create a flight, he needs to specify destination, plane, stewards, date and time. 
- To check what planes are available for given date at given destination, he searches via "unusedPlanes" function.
- After a flight is created, manager decides to change the departure time and update capacity of plane
- Lastly he cancels (deletes) the flight.
Other tasks that manager can do (more in runnableScenario.py):
    - List entities
    - Search for entity
    - Update entity
    - Delete entity
"""


class Sequence(SequentialTaskSet):

    # Task that creates an airline
    @task()
    def create_airline(self):
        with self.client.post("http://localhost:5001/api/v1/airlines", name="Create Airline", headers=heads, catch_response=True, data=json.dumps({
            "name": "".join(random.choice(string.ascii_letters) for _ in range(10)),
            "flights": []
        })) as rspns:
            if rspns.status_code == 200:
                global airlinesID
                airlinesID = rspns.json()
                print("Created airline with id: " + airlinesID)
                rspns.success()
            else:
                rspns.failure("Failed to create airline: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task that creates first destination

    @task()
    def create_first_destination(self):
        with self.client.post("http://localhost:5004/api/v1/destinations", name="Create Destination", headers=heads, catch_response=True, data=json.dumps({
            "country": "Czech Republic",
            "city": "Brno"
        })) as rspns:
            if rspns.status_code == 200:
                destinations.append(rspns.json())
                print("Created destination with id: " + destinations[0])
                rspns.success()
            else:
                rspns.failure(
                    "Failed to create first destination: " + rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task that creates second destination

    @task()
    def create_second_destination(self):
        with self.client.post("http://localhost:5004/api/v1/destinations", name="Create Destination", headers=heads, catch_response=True, data=json.dumps({
            "country": "Czech Republic",
            "city": "Prague"
        })) as rspns:
            if rspns.status_code == 200:
                destinations.append(rspns.json())
                print("Created destination with id: " + destinations[1])
                rspns.success()
            else:
                rspns.failure(
                    "Failed to create second destination: " + rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager registers a plane to airline
# convert int to str
    @task()
    def register_plane(self):
        with self.client.post("http://localhost:5002/api/v1/planes", name="Create Plane", headers=heads, catch_response=True, data=json.dumps({
            "name": "Boeing 737",
            "type": "T38TAT",
            "capacity": random.randint(50, 200),
            "airlineId": airlinesID,
            "currentLocationId": destinations[0],
        })) as rspns:
            if rspns.status_code == 200:
                global planeID
                planeID = rspns.json()
                print("Created plane with id: " + planeID)
                rspns.success()
            else:
                rspns.failure("Failed to create plane: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager registers first steward to airline

    @task()
    def create_first_steward(self):
        with self.client.post("http://localhost:5005/api/v1/stewards", name="Create First Steward", headers=heads, catch_response=True, data=json.dumps({
            "firstName": "John",
            "lastName": "Doe",
            "airlineId": airlinesID,
            "currentLocationId": destinations[0],
        })) as rspns:
            if rspns.status_code == 200:
                stewards.append(rspns.json())
                print("Created steward with id: " + stewards[0])
                rspns.success()
            else:
                rspns.failure("Failed to create steward: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager registers second steward to airline

    @task()
    def create_second_steward(self):
        with self.client.post("http://localhost:5005/api/v1/stewards", name="Create Second Steward", headers=heads, catch_response=True, data=json.dumps({
            "firstName": "Jane",
            "lastName": "Doe",
            "airlineId": airlinesID,
            "currentLocationId": destinations[0]
        })) as rspns:
            if rspns.status_code == 200:
                stewards.append(rspns.json())
                print("Created steward with id: " + stewards[1])
                rspns.success()
            else:
                rspns.failure("Failed to create steward: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Confirms there are available planes for given date and destination
    @task()
    def get_unused_planes(self):
        print(date1.isoformat())
        print(date2.isoformat())
        link = f"http://localhost:5002/api/v1/planes/unusedplanes?destinationID={destinations[0]}&airlineID={airlinesID}&timeframeStart={quote(date1.isoformat())}&timeframeEnd={quote(date2.isoformat())}"
        with self.client.get(link, name="Get Available Planes", headers=heads, catch_response=True) as response:
            if response.status_code == 200:
                response.success()
            else:
                response.failure("Failed to get available planes: " +
                                 response.text + " - " + str(response.status_code))
                raise StopUser

    # Task where airport manager creates a flight

    @task()
    def create_flight(self):
        link = f"http://localhost:5003/api/v1/flights?airlineId={airlinesID}&planeId={planeID}&destinationId={destinations[1]}&originId={destinations[0]}&departTime={quote(date1.isoformat())}&arriveTime={quote(date2.isoformat())}&stewards={stewards[0]}&stewards={stewards[1]}"

        print(link)

        with self.client.post(link, name="Create Flight", headers=heads, catch_response=True) as rspns:
            if rspns.status_code == 200:
                global flightID
                flightID = rspns.json()
                rspns.success()
            else:
                rspns.failure("Failed to create flight: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager postpones departure time 1 day

    @task()
    def update_flight(self):
        updatedDate = date1 + datetime.timedelta(days=1)
        link = f"http://localhost:5003/api/v1/flights?guid={flightID}&airlineId={airlinesID}&planeId={planeID}&destinationId={destinations[1]}&originId={destinations[0]}&departTime={quote(updatedDate.isoformat())}&arriveTime={quote(date2.isoformat())}&stewards={stewards[0]}&stewards={stewards[1]}"

        with self.client.patch(link, name="Update Flight", headers=heads, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update flight: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager updates capacity of plane

    @task()
    def update_plane(self):
        with self.client.patch(f"http://localhost:5002/api/v1/planes", name="Update Plane", headers=heads, catch_response=True, data=json.dumps({
            "guid": planeID,
            "name": "Boeing 737",
            "type": "T38TAT",
            "capacity": random.randint(50, 200),
            "airlineId": airlinesID,
            "currentLocationId": destinations[0]
        })) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update plane: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser

    # Task where airport manager deletes flight

    @task()
    def delete_flight(self):
        with self.client.delete(f"http://localhost:5003/api/v1/flights/{flightID}", name="Delete Flight", headers=heads, catch_response=True) as rspns:
            if rspns.status_code == 200:
                print("Sequential test completed successfully!")
                rspns.success()
                raise StopUser()
            else:
                rspns.failure("Failed to delete flight: " +
                              rspns.text + " - " + str(rspns.status_code))
                raise StopUser


class AirportManager(HttpUser):
    tasks = [Sequence]
