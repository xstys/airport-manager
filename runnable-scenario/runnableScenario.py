#!/usr/bin/env python3.11

"""
file: runnableScenario.py
author: Tomas Tomala (541685)
description: This file contains runnable scenario for PA165 project.
project: https://gitlab.fi.muni.cz/xstys/airport-manager
testingtool: Locust https://locust.io/
! Before starting the test, make sure that all services are running, otherwise the test will fail!
! Next prerequisite is to login via localhost:8080 -> show your personal token on http://localhost:8080/token
! Copy the token and paste it to AUTHTOKEN variable below (Without "Token:")
! After that, you can start locust and run the test via website http://localhost:8089
usage: locust -f runnableScenario.py --host=http://localhost:8089
"""

import json
import datetime
import random
import string
from urllib.parse import quote
from locust import HttpUser, TaskSet, task

AUTHTOKEN = ""

# Array of created destinations
destinations = []
# Array of created airlines
airlines = []
# Array of created planes
planes = []
# Array of created flights
flights = []
# Array of created stewards
stewards = []

# Headers used for requests
HEADS = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + AUTHTOKEN
}

"""
Scenario:

In this runnable scenario, multiple airport managers are created. 
Tasks that each airport manager will perform are selected randomly based on their assigned weight. Tasks with higher weight are more likely to be selected.
After creation, they can perform tasks like searching for entities, creating airlines/destinations, updating them and deleting them. 
After creating both airlines and destinations, they can create planes and stewards. Along with that, they can also update and delete them.
After necessary entities are created, they are able to create flights (and modify/update them). 
"""

# Generates random word


def random_word():
    return "".join(random.choice(string.ascii_letters) for _ in range(10))


def getFlightBody():
    if len(airlines) == 0 or len(destinations) < 2 or len(planes) == 0 or len(stewards) < 2:
        raise Exception("Not enough entities to create flight")

    airline = random.choice(airlines)

    usable_planes = list(filter(lambda x: x[1] == airline, planes))
    if len(usable_planes) == 0:
        raise Exception("Not enough planes to create flight")

    picked_plane = random.choice(usable_planes)

    usable_stewards = list(filter(lambda x: x[1] == airline, stewards))
    if len(usable_stewards) < 2:
        raise Exception("Not enough stewards to create flight")

    steward1 = random.choice(usable_stewards)
    steward2 = random.choice(usable_stewards)
    while steward1 == steward2:
        steward2 = random.choice(usable_stewards)

    return airline, picked_plane[0], steward1[0], steward2[0]


class UserBehavior(TaskSet):

    @task(5)
    def list_flights(self):
        with self.client.get("http://localhost:5003/api/v1/flights?page=0&size=5&order=oldest", name="List Flights", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to list flights: " +
                              rspns.text + " - " + str(rspns.status_code))

    @task(5)
    def list_airlines(self):
        with self.client.get("http://localhost:5001/api/v1/airlines", name="List Airlines", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to list airlines: " +
                              rspns.text + " - " + str(rspns.status_code))

    @task(5)
    def list_destinations(self):
        with self.client.get("http://localhost:5004/api/v1/destinations", name="List Destinations", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to list destinations: " +
                              rspns.text + " - " + str(rspns.status_code))

    @task(5)
    def find_flight(self):
        if len(flights) == 0:
            return
        with self.client.get(f"http://localhost:5003/api/v1/flights/{random.choice(flights)}", name="Find Flight", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200 or rspns.status_code == 404:
                rspns.success()
            else:
                rspns.failure("Failed to search flight: " +
                              rspns.text + " - " + str(rspns.status_code))

    @task(5)
    def find_airline(self):
        if len(airlines) == 0:
            return
        with self.client.get(f"http://localhost:5001/api/v1/flights/{random.choice(airlines)}", name="Find Airline", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200 or rspns.status_code == 404:
                rspns.success()
            else:
                rspns.failure("Failed to search airline: " +
                              rspns.text + " - " + str(rspns.status_code))

    @task(5)
    def find_destination(self):
        if len(destinations) == 0:
            return
        with self.client.get(f"http://localhost:5004/api/v1/flights/{random.choice(destinations)}", name="Find Destination", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200 or rspns.status_code == 404:
                rspns.success()
            else:
                rspns.failure("Failed to search destination: " +
                              rspns.text + " - " + str(rspns.status_code))

    # ==================== DESTINATIONS ====================

    # Add entity

    @task(6)
    def create_destination(self):
        with self.client.post("http://localhost:5004/api/v1/destinations", name="Create Destination", headers=HEADS, catch_response=True, data=json.dumps({
            "country": random_word(),
            "city": random_word()
        })) as rspns:
            if rspns.status_code == 200:
                destinations.append(rspns.json())
                # print("Created destination with id: " + rspns.json())
                rspns.success()
            else:
                rspns.failure(
                    "Failed to create first destination: " + rspns.text + " - " + str(rspns.status_code))

    # Update entity

    @task(2)
    def update_destination(self):
        if len(destinations) == 0:
            return
        with self.client.patch("http://localhost:5004/api/v1/destinations", name="Update Destination", headers=HEADS, catch_response=True, data=json.dumps({
            "guid": random.choice(destinations),
            "country": random_word(),
            "city": random_word()
        })) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update destination")

    # Delete entity

    @task(2)
    def delete_destination(self):
        if len(destinations) == 0:
            return
        destinationToDelete = random.choice(destinations)
        with self.client.delete(f"http://localhost:5004/api/v1/destinations/{destinationToDelete}", name="Delete Destination", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                # Find string in array and delete it
                if destinationToDelete in destinations:
                    destinations.remove(destinationToDelete)
            else:
                rspns.failure("Failed to delete destination")

    # ==================== AIRLINES ====================

    # Add entity

    @task(3)
    def create_airline(self):
        with self.client.post("http://localhost:5001/api/v1/airlines", name="Create Airline", headers=HEADS, catch_response=True, data=json.dumps({
            "name": random_word(),
            "flights": []
        })) as rspns:
            if rspns.status_code == 200:
                airlines.append(rspns.json())
                rspns.success()
            else:
                rspns.failure("Failed to create airline: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Update entity

    @task(2)
    def update_airline(self):
        if len(airlines) == 0:
            return
        with self.client.patch("http://localhost:5001/api/v1/airlines", name="Update Airline", headers=HEADS, catch_response=True, data=json.dumps({
            "guid": random.choice(airlines),
            "name": random_word(),
            "flights": []
        })) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update airline")

    # Delete entity

    @task(1)
    def delete_airline(self):
        if len(airlines) == 0:
            return
        airlineToDelete = random.choice(airlines)
        with self.client.delete(f"http://localhost:5001/api/v1/airlines/{airlineToDelete}", name="Delete Airline", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                if airlineToDelete in airlines:
                    airlines.remove(airlineToDelete)
                rspns.success()
            else:
                rspns.failure("Failed to delete airline")

    # ==================== PLANES ====================

    # List all planes

    @task(5)
    def list_planes(self):
        with self.client.get("http://localhost:5002/api/v1/planes", name="List Planes", headers=HEADS, catch_response=True,) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to list planes: " + rspns.text)

    # Add entity if airline and destination exist

    @task(7)
    def register_plane(self):
        if len(airlines) == 0 or len(destinations) == 0:
            return
        selectedAirline = random.choice(airlines)
        with self.client.post("http://localhost:5002/api/v1/planes", name="Create Plane", headers=HEADS, catch_response=True, data=json.dumps({
            "name": random_word(),
            "type": "T"+random_word(),
            "capacity": random.randint(50, 200),
            "airlineId": selectedAirline,
            "currentLocationId": random.choice(destinations)
        })) as rspns:
            if rspns.status_code == 200:
                planeID = rspns.json()
                planes.append((planeID, selectedAirline))
                rspns.success()
            else:
                rspns.failure("Failed to create plane: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Update entity

    @task(3)
    def update_plane(self):
        if len(planes) == 0 or len(airlines) == 0 or len(destinations) == 0:
            return
        planeToUpdate = random.choice(planes)
        if planeToUpdate[1] not in airlines:
            return
        with self.client.patch("http://localhost:5002/api/v1/planes", name="Update Plane", headers=HEADS, catch_response=True, data=json.dumps({
            "guid": planeToUpdate[0],
            "name": random_word(),
            "type": "T"+random_word(),
            "capacity": random.randint(50, 200),
            "airlineId": planeToUpdate[1],
            "currentLocationId": random.choice(destinations)
        })) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update plane: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Delete entity

    @task(2)
    def delete_plane(self):
        if len(planes) == 0:
            return
        planeToDelete = random.choice(planes)
        with self.client.delete(f"http://localhost:5002/api/v1/planes/{planeToDelete[0]}", name="Delete Plane", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                if planeToDelete in planes:
                    planes.remove(planeToDelete)
                rspns.success()
            else:
                rspns.failure("Failed to delete plane: " + planeToDelete)

    # Get unused planes at destination in date range

    @task(6)
    def get_unused_planes(self):
        if len(airlines) == 0 or len(destinations) == 0:
            return
        date1 = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) + \
            datetime.timedelta(days=random.randint(1, 100))
        date2 = date1 + datetime.timedelta(days=random.randint(2, 5))
        link = f"http://localhost:5002/api/v1/planes/unusedplanes?destinationID={random.choice(destinations)}&airlineID={random.choice(airlines)}&timeframeStart={quote(date1.isoformat())}&timeframeEnd={quote(date2.isoformat())}"

        with self.client.get(link, name="Get Available Planes", headers=HEADS, catch_response=True) as response:
            if response.status_code == 200:
                response.success()
            else:
                response.failure("Failed to get available planes: " +
                                 response.text + " - " + str(response.status_code))

     # ============== STEWARDS ==============

    # List all stewards

    @task(5)
    def list_stewards(self):
        with self.client.get("http://localhost:5005/api/v1/stewards", name="List Stewards", headers=HEADS, catch_response=True,) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to list stewards: " + rspns.text)

    # Add entity if airline and destination exist

    @task(8)
    def register_steward(self):
        if len(airlines) == 0 or len(destinations) == 0:
            return
        pickedAirline = random.choice(airlines)
        with self.client.post("http://localhost:5005/api/v1/stewards", name="Create Steward", headers=HEADS, catch_response=True, data=json.dumps({
            "firstName": random_word(),
            "lastName": random_word(),
            "airlineId": pickedAirline,
            "currentLocationId": random.choice(destinations)
        })) as rspns:
            if rspns.status_code == 200:
                stewards.append((rspns.json(), pickedAirline))
                rspns.success()
            else:
                rspns.failure("Failed to create steward: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Update entity

    @task(2)
    def update_steward(self):
        if len(stewards) == 0 or len(airlines) == 0 or len(destinations) == 0:
            return
        pickedSteward = random.choice(stewards)
        if pickedSteward[1] not in airlines:
            return
        with self.client.patch("http://localhost:5005/api/v1/stewards", name="Update Steward", headers=HEADS, catch_response=True, data=json.dumps({
            "guid": pickedSteward[0],
            "firstName": random_word(),
            "lastName": random_word(),
            "airlineId": pickedSteward[1],
            "currentLocationId": random.choice(destinations)
        })) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update steward")

    # Delete entity

    @task(2)
    def delete_steward(self):
        if len(stewards) == 0:
            return
        stewardToDelete = random.choice(stewards)
        with self.client.delete(f"http://localhost:5005/api/v1/stewards/{stewardToDelete[0]}", name="Delete Steward", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                if stewardToDelete in stewards:
                    stewards.remove(stewardToDelete)
                rspns.success()
            else:
                rspns.failure("Failed to delete steward")

    # ==================== FLIGHTS ====================

    # Add entity if airline, destination, plane and stewards exist

    @task(6)
    def create_flight(self):

        try:
            airline, pickedPlane, steward1, steward2 = getFlightBody()
        except Exception:
            return

        destination1 = random.choice(destinations)
        destination2 = random.choice(destinations)
        while destination1 == destination2:
            destination2 = random.choice(destinations)

        date1 = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) + \
            datetime.timedelta(days=random.randint(1, 100))
        date2 = date1 + datetime.timedelta(days=random.randint(2, 5))

        link = f"http://localhost:5003/api/v1/flights?airlineId={airline}&planeId={pickedPlane}&destinationId={destination1}&originId={destination2}&departTime={quote(date1.isoformat())}&arriveTime={quote(date2.isoformat())}&stewards={steward1}&stewards={steward2}"

        with self.client.post(link, name="Create Flight", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                flights.append(rspns.json())
                rspns.success()
            else:
                rspns.failure("Failed to create flight: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Update entity

    @task(1)
    def update_flight(self):
        if len(flights) == 0:
            return

        updatedDate = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) + \
            datetime.timedelta(days=random.randint(1, 100))
        date2 = updatedDate + datetime.timedelta(days=random.randint(2, 5))

        try:
            airline, pickedPlane, steward1, steward2 = getFlightBody()
        except Exception:
            return

        destination1 = random.choice(destinations)
        destination2 = random.choice(destinations)
        while destination1 == destination2:
            destination2 = random.choice(destinations)

        link = f"http://localhost:5003/api/v1/flights?guid={random.choice(flights)}&airlineId={airline}&planeId={pickedPlane}&destinationId={destination1}&originId={destination2}&departTime={quote(updatedDate.isoformat())}&arriveTime={quote(date2.isoformat())}&stewards={steward1}&stewards={steward2}"

        with self.client.patch(link, name="Update Flight", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to update flight: " +
                              rspns.text + " - " + str(rspns.status_code))

    # Delete entity

    @task(2)
    def deleteFlight(self):
        if len(flights) == 0:
            return

        flightToDelete = random.choice(flights)
        with self.client.delete(f"http://localhost:5003/api/v1/flights/{flightToDelete}", name="Delete Flight", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                if flightToDelete in flights:
                    flights.remove(flightToDelete)
                rspns.success()
            else:
                rspns.failure("Failed to delete flight")

    # Get subsequent flights

    @task(3)
    def getSubsequentFlights(self):
        if len(flights) == 0:
            return

        with self.client.get(f"http://localhost:5003/api/v1/flights/subsequent/{random.choice(flights)}", name="Get subsequent flights", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to get subsequent flights")

    # Get all flights after departure Time

    @task(5)
    def getFlightsAfterDepartureTime(self):
        if len(flights) == 0:
            return

        date1 = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc) + \
            datetime.timedelta(days=random.randint(1, 100))
        with self.client.get(f"http://localhost:5003/api/v1/flights/time/{quote(date1.isoformat())}?page=0&size=5&order=oldest", name="Get flights after departure time", headers=HEADS, catch_response=True) as rspns:
            if rspns.status_code == 200:
                rspns.success()
            else:
                rspns.failure("Failed to get flights after departure time: " +
                              rspns.text + " - " + str(rspns.status_code))


class AirlineManager(HttpUser):
    tasks = [UserBehavior]
