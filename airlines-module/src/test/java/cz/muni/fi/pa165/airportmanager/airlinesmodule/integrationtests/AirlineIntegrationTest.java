package cz.muni.fi.pa165.airportmanager.airlinesmodule.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.repository.AirlineRepository;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class AirlineIntegrationTest {
    @MockBean
    private AirlineRepository airlineRepository;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void findAllAirlinesTest() throws Exception {
        List<Airline> airlines =
                List.of(new Airline(UUID.randomUUID(), "Airline1",
                                    new ArrayList<>()),
                        new Airline(UUID.randomUUID(), "Airline2",
                                    new ArrayList<>()));
        when(airlineRepository.findByDeletedIsNull()).thenReturn(airlines);

        ResultActions result = mockMvc.perform(get("/api/v1/airlines"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        List<AirlineFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {
        });

        assertAll("entities",
                () -> assertEquals(2, entities.size()),
                () -> assertEquals(airlines.get(0).getName(),
                                   entities.get(0).getName()),
                () -> assertEquals(airlines.get(0).getFlights(),
                                   entities.get(0).getFlights()),
                () -> assertEquals(airlines.get(0).getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(airlines.get(1).getName(),
                                   entities.get(1).getName()),
                () -> assertEquals(airlines.get(1).getFlights(),
                                   entities.get(1).getFlights()),
                () -> assertEquals(airlines.get(1).getId(),
                                   entities.get(1).getGuid())
        );
    }

    @Test
    public void findAirlineTest() throws Exception {
        var airline = new Airline("Airline1");
        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(airline));

        ResultActions result = mockMvc.perform(
                get("/api/v1/airlines/" + airline.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        AirlineFindDto entity = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {
        });

        assertAll("entities",
                () -> assertEquals(airline.getName(), entity.getName()),
                () -> assertEquals(airline.getFlights(), entity.getFlights()),
                () -> assertEquals(airline.getId(), entity.getGuid())
        );
    }

    @Test
    public void findAirlineForNonExistentIdTest() throws Exception {
        var airline = new Airline("Airline1");
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        UUID nonExistentUUID = UUID.randomUUID();
        while (nonExistentUUID == airline.getId()) {
            nonExistentUUID = UUID.randomUUID();
        }

        ResultActions result = mockMvc.perform(
                get("/api/v1/airlines/" + nonExistentUUID));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void findAirlineForDeletedTest() throws Exception {
        var airline = new Airline("Airline1");
        airline.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1));

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(airline));

        ResultActions result = mockMvc.perform(
                get("/api/v1/airlines/" + airline.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteTest() throws Exception {
        var airline = new Airline("Airline1");

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(airline));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/airlines/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        verify(airlineRepository).findById(any());
        verify(airlineRepository, times(0)).delete(any());
        verify(airlineRepository).save(any());

        assertNotEquals(null, airline.getDeleted());
        assertNull(airline.getUpdated());
    }

    @Test
    public void deleteTestNonExistent() throws Exception {
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        ResultActions result = mockMvc.perform(
                delete("/api/v1/airlines/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());

        verify(airlineRepository).findById(any());
        verify(airlineRepository, times(0)).delete(any());
    }

    @Test
    public void deleteTestDeleted() throws Exception {
        var airline = new Airline("Airline1");
        airline.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));

        when(airlineRepository.findById(any())).thenReturn(Optional.of(airline));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/airlines/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        verify(airlineRepository).findById(any());
        verify(airlineRepository, times(0)).delete(any());
    }

    @Test
    public void addTest() throws Exception {
        var airline = new AirlineInsertDto("Airlines ONE", new ArrayList<>());
        when(airlineRepository.save(any()))
                .thenReturn(new Airline("Airlines ONE"));

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(airline);

        ResultActions result = mockMvc.perform(post("/api/v1/airlines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        verify(airlineRepository).save(any());
    }

    @Test
    public void addIncorrectInputTest() throws Exception {
        var airline = new AirlineInsertDto("", null);

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(airline);

        ResultActions result = mockMvc.perform(post("/api/v1/airlines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateTest() throws Exception {
        var id = UUID.randomUUID();
        var airline = new AirlineUpdateDto(id, "Airlines TWO",
                                           new ArrayList<>());
        var oldAirline = new Airline(id, "Airlines ONE", new ArrayList<>());

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(oldAirline));

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(airline);

        ResultActions result = mockMvc.perform(patch("/api/v1/airlines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        verify(airlineRepository).save(any());
        assertNull(oldAirline.getDeleted());
    }

    @Test
    public void updateIncorrectInputTest() throws Exception {
        var id = UUID.randomUUID();
        var airline = new AirlineUpdateDto(id, "", null);
        var oldAirline = new Airline(id, "Airlines ONE", new ArrayList<>());

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(oldAirline));

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(airline);

        ResultActions result = mockMvc.perform(patch("/api/v1/airlines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateTestNonExistent() throws Exception {
        var id = UUID.randomUUID();
        var airline = new AirlineUpdateDto(id, "Airlines TWO",
                                           new ArrayList<>());

        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(airline);

        ResultActions result = mockMvc.perform(patch("/api/v1/airlines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());

        verify(airlineRepository, times(0)).save(any());
    }
}
