package cz.muni.fi.pa165.airportmanager.airlinesmodule.service;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.repository.AirlineRepository;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.mapper.AirlineMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class AirlineServiceTest {
    private List<UUID> testFlights = Arrays.asList(UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID());
    private AirlineService airlineService;
    private AirlineMapper airlineMapper;
    private AirlineRepository airlineRepository;

    @BeforeEach
    void setUp() {
        airlineMapper = mock(AirlineMapper.class);
        airlineRepository = mock(AirlineRepository.class);
        airlineService = new AirlineServiceImpl(airlineRepository,
                                                airlineMapper);
    }

    @Test
    public void addTest() {
        var airline = new AirlineInsertDto("Test Airlines", testFlights);
        when(airlineRepository.save(any())).thenReturn(new Airline());
        airlineService.addEntity(airline);

        verify(airlineRepository).save(any());
    }

    @Test
    public void addBlankName() {
        var airline = new AirlineInsertDto("", testFlights);

        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.addEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void addNullFlights() {
        var airline = new AirlineInsertDto("Test Airlines", null);

        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.addEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void addNullTest() {;
        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.addEntity(null));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateNullTest() {;
        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.updateEntity(null));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateTest() {
        var airline = new AirlineUpdateDto(UUID.randomUUID(), "TestUpdate",
                                           testFlights);
        var airlineEntity = new Airline(UUID.randomUUID(), "TestUpdate",
                                        testFlights);

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(new Airline(UUID.randomUUID(),
                                        "New Test Update",
                                        new ArrayList<>())));
        when(airlineMapper.map(airline)).thenReturn(airlineEntity);

        airlineService.updateEntity(airline);

        verify(airlineRepository).save(any());
    }

    @Test
    public void updateNonexistentTest() {
        var airline = new AirlineUpdateDto(UUID.randomUUID(), "TEST",
                                           testFlights);
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> airlineService.updateEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateNullName() {
        var airline = new AirlineUpdateDto(UUID.randomUUID(), null,
                                           testFlights);

        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.updateEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateBlankName() {
        var airline = new AirlineUpdateDto(UUID.randomUUID(), "", testFlights);

        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.updateEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void updateNullFlights() {
        var airline = new AirlineUpdateDto(UUID.randomUUID(), "Test", null);

        assertThrows(InvalidUserInputException.class,
                     () -> airlineService.updateEntity(airline));

        verify(airlineRepository, times(0)).save(any());
    }

    @Test
    public void findTest() {
        var id = UUID.randomUUID();
        var airline = new Airline(id, "TESTF", testFlights);
        var airlineDto = new AirlineFindDto(id, airline.getName(),
                                            airline.getFlights());

        when(airlineRepository.findById(id)).thenReturn(Optional.of(airline));
        when(airlineMapper.map(airline)).thenReturn(airlineDto);

        var result = airlineService.findById(id);

        verify(airlineRepository, times(1)).findById(any());
        assertEquals(airlineDto, result);
    }

    @Test
    public void findMissingTest() {
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> airlineService.findById(UUID.randomUUID()));

        verify(airlineRepository, times(1)).findById(any());
        verify(airlineMapper, times(0)).map(any(Airline.class));
    }

    @Test
    public void findNullTest() {
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> airlineService.findById(null));

        verify(airlineRepository, times(1)).findById(any());
        verify(airlineMapper, times(0)).map(any(Airline.class));
    }

    @Test
    public void deleteNullTest() {
        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> airlineService.findById(null));

        verify(airlineRepository, times(0)).delete(any());
    }

    @Test
    public void deleteMissingTest() {

        when(airlineRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> airlineService.delete(UUID.randomUUID()));

        verify(airlineRepository, times(0)).delete(any());
    }

    @Test
    public void deleteExistingTest() {
        var id = UUID.randomUUID();
        var airline = new Airline(id, "Test Airlines", testFlights);

        when(airlineRepository.findById(any()))
                .thenReturn(Optional.of(airline));

        airlineService.delete(id);

        verify(airlineRepository, times(1)).save(airline);
    }

    @Test
    public void getEntitiesSingleTest() {
        var id = UUID.randomUUID();
        var airline = new Airline(id, "Test Airlines", testFlights);
        var airlineDto = new AirlineFindDto(id, airline.getName(),
                                            airline.getFlights());

        when(airlineRepository.findAll()).thenReturn(List.of(airline));
        when(airlineMapper.map(anyList())).thenReturn(List.of(airlineDto));

        var result = airlineService.getEntities();

        verify(airlineRepository, times(1)).findByDeletedIsNull();
        assertEquals(1, result.size());
        assertEquals(airlineDto, result.get(0));
    }

    @Test
    public void getEntitiesEmptyTest() {
        when(airlineRepository.findAll()).thenReturn(List.of());
        when(airlineMapper.map(anyList())).thenReturn(List.of());

        var result = airlineService.getEntities();

        verify(airlineRepository, times(1)).findByDeletedIsNull();
        assertEquals(0, result.size());
    }

    @Test
    public void getEntitiesMultipleTest() {
        var airlines = List.of(
                new Airline(UUID.randomUUID(), "Airline1", testFlights),
                new Airline(UUID.randomUUID(), "Airline2", testFlights)
        );

        var airlineDtos = List.of(
                new AirlineFindDto(airlines.get(0).getId(),
                                   airlines.get(0).getName(),
                                   airlines.get(0).getFlights()),
                new AirlineFindDto(airlines.get(1).getId(),
                                   airlines.get(1).getName(),
                                   airlines.get(1).getFlights())
        );

        when(airlineRepository.findAll()).thenReturn(airlines);
        when(airlineMapper.map(anyList())).thenReturn(airlineDtos);

        var result = airlineService.getEntities();

        verify(airlineRepository, times(1)).findByDeletedIsNull();
        assertEquals(2, result.size());
        assertEquals(airlineDtos.get(0), result.get(0));
        assertEquals(airlineDtos.get(1), result.get(1));
    }
}
