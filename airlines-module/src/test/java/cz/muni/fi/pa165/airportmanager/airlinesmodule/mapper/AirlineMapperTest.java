package cz.muni.fi.pa165.airportmanager.airlinesmodule.mapper;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class AirlineMapperTest {
    private List<UUID> testFlights = Arrays.asList(UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID());

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščé í^˘˛ˇ856"})
    public void airlineToFindDtoNameTest(String name) {
        var airline = new Airline(name);
        var findDto = new AirlineMapper().map(airline);

        assertEquals(name, findDto.getName());
        assertNotNull(findDto.getGuid());
        assertEquals(airline.getFlights(), findDto.getFlights());
    }

    @Test
    public void airlineToFindDtoFlightsTest() {
        List<UUID> flights = new ArrayList<>();
        UUID uuid = UUID.randomUUID();
        flights.add(uuid);

        var airline = new Airline(UUID.randomUUID(),"Name", flights);
        var findDto = new AirlineMapper().map(airline);

        assertEquals(airline.getFlights(), findDto.getFlights());
        assertNotNull(findDto.getGuid());
        assertEquals(airline.getName(), findDto.getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Nnnnn", "NRML", "nmnmnm", "Tůůůt", "T2st"})
    public void updateDtoToAirlineNameTest(String name) {
        var updateDto = new AirlineUpdateDto(UUID.randomUUID(), name, null);
        var airline = new AirlineMapper().map(updateDto);

        assertEquals(name, airline.getName());
        assertNotNull(airline.getId());
        assertNull(airline.getFlights());
        assertEquals(airline.getFlights(), updateDto.getFlights());
    }

    @Test
    public void updateDtoToAirlineFlightsTest() {
        List<UUID> flights = new ArrayList<>();
        UUID uuid = UUID.randomUUID();
        flights.add(uuid);

        var updateDto = new AirlineUpdateDto(UUID.randomUUID(), "Name",
                                             flights);
        var airline = new AirlineMapper().map(updateDto);

        assertEquals(airline.getFlights(), updateDto.getFlights());
        assertNotNull(airline.getId());
        assertTrue(airline.getFlights().size() == 1);
        assertEquals(airline.getFlights().get(0), uuid);
        assertEquals(airline.getName(), updateDto.getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Nnn#nn", "TESTNAME11", "nmnm?m", "52155", "T2st"})
    public void insertDtoToAirlineNameTest(String name) {
        var insertDto = new AirlineInsertDto(name, testFlights);
        var airline = new AirlineMapper().map(insertDto);

        assertEquals(name, airline.getName());
        assertNotNull(airline.getFlights());
        assertEquals(airline.getFlights(), insertDto.getFlights());
    }

    @Test
    public void insertDtoToAirlineFlightsTest() {
        List<UUID> flights = new ArrayList<>();
        UUID uuid = UUID.randomUUID();
        flights.add(uuid);

        var insertDto = new AirlineInsertDto("Name", flights);
        var airline = new AirlineMapper().map(insertDto);

        assertNotNull(airline.getId());
        assertEquals(airline.getFlights(), insertDto.getFlights());
        assertTrue(airline.getFlights().size() == 1);
        assertEquals(airline.getFlights().get(0), uuid);
        assertEquals(airline.getName(), insertDto.getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Rand#?om", "22Flights", "+FLight1",
                            "UPDTDTOTOAIR", "STRTOUID"})
    void airlineListTest(String name) {
        var list = new ArrayList<Airline>();
        var airline = new Airline(name);
        list.add(airline);
        var dtoList = new AirlineMapper().map(list);
        assertTrue(dtoList.size() == 1);
        assertEquals(dtoList.get(0).getName(), name);
    }

    @Test
    void emptyAirlineTest() {
        var list = new ArrayList<Airline>();
        var dtoList = new AirlineMapper().map(list);
        assertTrue(dtoList.isEmpty());
    }
}