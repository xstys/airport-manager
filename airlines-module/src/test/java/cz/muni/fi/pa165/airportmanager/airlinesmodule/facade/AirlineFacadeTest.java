package cz.muni.fi.pa165.airportmanager.airlinesmodule.facade;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.service.AirlineService;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.service.AirlineServiceImpl;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class AirlineFacadeTest {
    private AirlineFacade facade;
    private AirlineService service;
    private List<UUID> testFlights = Arrays.asList(UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID(),
                                                   UUID.randomUUID());

    @BeforeEach
    void setUp() {
        service = mock(AirlineServiceImpl.class);
        facade = new AirlineFacadeImpl(service);
    }

    @Test
    public void addTest() {
        var add = new AirlineInsertDto("test", testFlights);
        facade.addEntity(add);

        verify(service).addEntity(add);
    }

    @Test
    public void deleteTest() {
        var id = UUID.randomUUID();
        facade.delete(id);

        verify(service).delete(id);
    }

    @Test
    public void updateTest() {
        var update = new AirlineUpdateDto(UUID.randomUUID(),
                                    "test", testFlights);
        facade.updateEntity(update);

        verify(service).updateEntity(update);
    }

    @Test
    public void findTest() {
        var uuid = UUID.randomUUID();
        var returnValue = new AirlineFindDto(uuid, "testName",
                                             testFlights);

        when(service.findById(any())).thenReturn(returnValue);
        var result = facade.findById(uuid);

        verify(service).findById(uuid);
        assertEquals(returnValue, result);
    }

    @Test
    public void getEntitiesTest() {
        var returnValue = List.of(new AirlineFindDto(UUID.randomUUID(),
                                               "testName", testFlights));
        when(service.getEntities()).thenReturn(returnValue);

        var result = facade.getEntities();

        verify(service).getEntities();
        assertEquals(returnValue, result);
    }
}
