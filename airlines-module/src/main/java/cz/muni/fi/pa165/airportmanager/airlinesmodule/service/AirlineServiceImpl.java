package cz.muni.fi.pa165.airportmanager.airlinesmodule.service;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.repository.AirlineRepository;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AirlineServiceImpl
        extends DomainServiceImpl<Airline, AirlineInsertDto,
                                  AirlineUpdateDto, AirlineFindDto>
        implements AirlineService {
    @Autowired
    public AirlineServiceImpl(AirlineRepository airlineRepository,
                              DomainMapper<Airline,
                                           AirlineInsertDto,
                                           AirlineUpdateDto,
                                           AirlineFindDto> mapper) {
        super(airlineRepository, mapper);
    }

    @Override
    public UUID addEntity(AirlineInsertDto entity) {
        entityCheck(entity);
        return super.addEntity(entity);
    }

    @Override
    public void updateEntity(AirlineUpdateDto entity) {
        entityCheck(entity);
        super.updateEntity(entity);
    }

    private void entityCheck(AirlineInsertDto airline) {
        if (airline == null) {
            throw new InvalidUserInputException(
                    "The provided airline class is null!");
        }
        dataCheck(airline.getName(), airline.getFlights());
    }

    private void entityCheck(AirlineUpdateDto airline) {
        if (airline == null) {
            throw new InvalidUserInputException(
                    "The provided airline class is null!");
        }
        dataCheck(airline.getName(), airline.getFlights());
    }

    private void dataCheck(String name, List<UUID> flights) {
        if (name == null) {
            throw new InvalidUserInputException(
                    "A null value is not a legal name.");
        }

        if (name.isBlank()) {
            throw new InvalidUserInputException(
                    "Airline name can't be blank.");
        }

        if (flights == null) {
            throw new InvalidUserInputException(
                    "Null is not a valid flights list.");
        }
    }
}
