package cz.muni.fi.pa165.airportmanager.airlinesmodule.facade;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;

public interface AirlineFacade
        extends DomainFacade<Airline, AirlineInsertDto,
                             AirlineUpdateDto, AirlineFindDto> {
}
