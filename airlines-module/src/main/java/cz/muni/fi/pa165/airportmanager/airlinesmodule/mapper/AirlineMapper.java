package cz.muni.fi.pa165.airportmanager.airlinesmodule.mapper;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirlineMapper
        implements DomainMapper<Airline, AirlineInsertDto,
            AirlineUpdateDto, AirlineFindDto> {
    @Override
    public Airline map(AirlineInsertDto airlineInsertDto) {
        if (airlineInsertDto == null) {
            return null;
        }
        var air = new Airline(airlineInsertDto.getName());
        air.addFlights(airlineInsertDto.getFlights());

        return air;
    }

    @Override
    public Airline map(AirlineUpdateDto airlineUpdateDto) {
        if (airlineUpdateDto == null) {
            return null;
        }

        return new Airline(airlineUpdateDto.getGuid(),
                           airlineUpdateDto.getName(),
                           airlineUpdateDto.getFlights());
    }

    @Override
    public AirlineFindDto map(Airline entity) {
        if (entity == null) {
            return null;
        }

        return new AirlineFindDto(entity.getId(), entity.getName(),
                                  entity.getFlights());
    }

    @Override
    public List<AirlineFindDto> map(List<Airline> entity) {
        return entity.stream()
                .map(e -> new AirlineFindDto(e.getId(), e.getName(),
                                             e.getFlights()))
                .toList();
    }
}
