package cz.muni.fi.pa165.airportmanager.airlinesmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.List;
import java.util.UUID;

public class AirlineUpdateDto extends DomainUpdateDto {
    private final String name;
    private final List<UUID> flights;

    public AirlineUpdateDto(UUID guid, String name, List<UUID> flights) {
        super(guid);

        this.name = name;
        this.flights = flights;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getFlights() {
        return flights;
    }
}
