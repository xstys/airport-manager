package cz.muni.fi.pa165.airportmanager.airlinesmodule.seed;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.repository.AirlineRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@Profile("dev")
public class AirlineSeed {
    private final AirlineRepository airlineRepository;

    @Autowired
    public AirlineSeed(AirlineRepository airlineRepository) {
        this.airlineRepository = airlineRepository;
        seed();
    }

    private void seed() {
        List<String> airlines = List.of(
                "Empty Airlines",
                "Czexpats Airlines",
                "Mom and Pop Airlines"
        );

        for (String a : airlines) {
            UUID id = UUID.nameUUIDFromBytes(a.getBytes());

            airlineRepository.save(new Airline(id, a, new ArrayList<>()));
        }
    }
}
