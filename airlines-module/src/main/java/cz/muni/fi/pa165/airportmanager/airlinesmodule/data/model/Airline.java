package cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class representing an Airline company
 */
@Entity
@Table(name = "AIRLINE")
public class Airline extends DomainEntity {
    @ElementCollection
    @Column(name = "FLIGHTS", nullable = false)
    private List<UUID> flights;
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    public Airline() {
        super(null);
    }

    public Airline(String name) {
        super(null);

        this.name = name;
        flights = new ArrayList<>();
    }

    public Airline(UUID guid, String name, List<UUID> flights) { //used for updates
        super(guid);

        this.name = name;
        this.flights = flights;
    }

    public void addFlight(UUID flightID) {
        flights.add(flightID);
    }

    public void addFlights(List<UUID> uuids) {
        for (var id : uuids) {
            addFlight(id);
        }
    }

    public boolean removeFlight(UUID flightID) {
        return flights.remove(flightID);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UUID> getFlights() {
        return flights;
    }

    public void setFlights(List<UUID> flights) {
        this.flights = flights;
    }
}
