package cz.muni.fi.pa165.airportmanager.airlinesmodule.rest;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.facade.AirlineFacade;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.rest.DomainController;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@OpenAPIDefinition( // metadata for inclusion into OpenAPI document
        // see javadoc at https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
        // see docs at https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(title = "Airline Service",
                version = "0.1",
                description = """
                        Simple service for airline handling at an airport.
                        The API has operations for:
                        - getting all airlines
                        - adding a new airline
                        - getting a specific airline by their id
                        - removing a specific airline by their id
                        """,
                contact = @Contact(name = "Matěj Gorgol",
                                   email = "540492@mail.muni.cz",
                                   url = "https://is.muni.cz/auth/osoba/" +
                                           "540492"),
                license = @License(name = "Apache 2.0",
                                   url = "https://www.apache.org/licenses/" +
                                           "LICENSE-2.0.html")
        ),
        servers = @Server(description = "localhost server",
                          url = "{scheme}://{server}:{port}",
                          variables = {
                            @ServerVariable(name = "scheme",
                                            allowableValues = {"http", "https"},
                                            defaultValue = "http"),
                            @ServerVariable(name = "server",
                                            defaultValue = "localhost"),
                            @ServerVariable(name = "port",
                                            defaultValue = "5001"),
        })
)

@Tag(name = "Airline", description = "Microservice for airline handling")
// metadata for inclusion into OpenAPI document
@RequestMapping(path = "api/v1/", produces = APPLICATION_JSON_VALUE)
// common prefix for all URLs handled by this controller
public class AirlineController
        extends DomainController<Airline, AirlineInsertDto,
                                 AirlineUpdateDto, AirlineFindDto> {
    @Autowired
    public AirlineController(AirlineFacade facade) {
        super(facade);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all airlines",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - airlines were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Returns an array of objects representing airlines,
                    ordered by their seniority.
                    """)
    @GetMapping("airlines") // URL mapping of this operation
    @CrossOrigin(origins = "*") // CORS headers needed for JavaScript clients
    public List<AirlineFindDto> getEntities() {
        return super.getEntities();
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Adds a new airline",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - airline was " +
                                         "successfully added"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Adds a provided airline.
                    """)
    @PostMapping("airlines") // URL mapping of this operation
    public UUID addEntity(@RequestBody AirlineInsertDto insertDto) {
        return super.addEntity(insertDto);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Changes an existing airline",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - airline was " +
                                         "successfully modified"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Changes an existing airline with the values
                    from provided airline.
                    """)
    @PatchMapping("airlines")
    public void updateEntity(@RequestBody AirlineUpdateDto updateDto) {
        super.updateEntity(updateDto);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Removes an existing airline",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - airline was " +
                                         "successfully deleted"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Removes an existing airline with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string"))
    @DeleteMapping("airlines/{id}")
    public void delete(@Valid @Parameter(hidden = true) @PathVariable UUID id) {
        super.delete(id);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing airline",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - airline was " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Retrieves an existing airline with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string"))
    @GetMapping("airlines/{id}")
    public AirlineFindDto findById(@Valid @Parameter(hidden = true)
                                       @PathVariable UUID id) {
        return super.findById(id);
    }
}
