package cz.muni.fi.pa165.airportmanager.airlinesmodule.service;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainService;

public interface AirlineService
        extends DomainService<Airline, AirlineInsertDto,
                              AirlineUpdateDto, AirlineFindDto> {
}
