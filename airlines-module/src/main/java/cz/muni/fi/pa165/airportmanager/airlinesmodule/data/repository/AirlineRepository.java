package cz.muni.fi.pa165.airportmanager.airlinesmodule.data.repository;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface AirlineRepository extends DomainRepository<Airline> {
}
