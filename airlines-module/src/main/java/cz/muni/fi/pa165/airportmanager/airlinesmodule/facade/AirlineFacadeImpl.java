package cz.muni.fi.pa165.airportmanager.airlinesmodule.facade;

import cz.muni.fi.pa165.airportmanager.airlinesmodule.data.model.Airline;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineInsertDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.dto.AirlineUpdateDto;
import cz.muni.fi.pa165.airportmanager.airlinesmodule.service.AirlineService;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacadeImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirlineFacadeImpl
        extends DomainFacadeImpl<Airline, AirlineInsertDto,
                                 AirlineUpdateDto, AirlineFindDto>
        implements AirlineFacade {
    @Autowired
    public AirlineFacadeImpl(AirlineService domainService) {
        super(domainService);
    }
}
