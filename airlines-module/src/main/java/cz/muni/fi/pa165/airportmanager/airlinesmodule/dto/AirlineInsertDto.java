package cz.muni.fi.pa165.airportmanager.airlinesmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;

import java.util.List;
import java.util.UUID;

public class AirlineInsertDto extends DomainInsertDto {
    private final String name;
    private final List<UUID> flights;

    public AirlineInsertDto(String name, List<UUID> flights) {
        this.name = name;
        this.flights = flights;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getFlights() {
        return flights;
    }
}
