package cz.muni.fi.pa165.airportmanager.stewardmodule.facade;

import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.service.StewardService;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class StewardFacadeTest {
    private final RetrieveEntityServiceImpl<AirlineFindDto> airlineService = mock(RetrieveEntityServiceImpl.class);
    private final UUID airlineId = UUID.randomUUID();
    private final RetrieveEntityServiceImpl<DestinationFindDto> destinationService = mock(RetrieveEntityServiceImpl.class);
    private final UUID currentLocationId = UUID.randomUUID();
    private final StewardService domainService = mock(StewardService.class);
    private final StewardFacade facade = new StewardFacadeImpl(domainService, airlineService, destinationService);

    @BeforeEach
    public void setUp() {
        when(airlineService.retrieve(eq(airlineId), anyString()))
                .thenReturn(
                        new AirlineFindDto(airlineId, "Empty Airlines", null));
        when(destinationService.retrieve(eq(currentLocationId), anyString()))
                .thenReturn(
                        new DestinationFindDto(currentLocationId,
                                           "Destination", "USA"));
    }

    @Test
    public void addTest() {
        var add = new StewardInsertDto("First", "Last",
                                       airlineId, currentLocationId);

        facade.addEntity(add);

        verify(domainService).addEntity(add);
    }

    @Test
    public void deleteTest() {
        var id = UUID.randomUUID();

        facade.delete(id);

        verify(domainService).delete(id);
    }

    @Test
    public void updateTest() {
        var update = new StewardUpdateDto(UUID.randomUUID(), "First",
                                         "Last", airlineId, currentLocationId);

        facade.updateEntity(update);

        verify(domainService).updateEntity(update);
    }

    @Test
    public void findTest() {
        var id = UUID.randomUUID();
        var returnValue = new StewardFindDto(UUID.randomUUID(),"First",
                                     "Last", UUID.randomUUID(),
                UUID.randomUUID());
        when(domainService.findById(any()))
                .thenReturn(returnValue);

        var result = facade.findById(id);

        verify(domainService).findById(id);
        assertEquals(returnValue, result);
    }

    @Test
    public void getEntitiesTest() {
        var returnValue = List.of(new StewardFindDto(UUID.randomUUID(),
                                            "First", "Last",
                                                    UUID.randomUUID(),
                                                    UUID.randomUUID()));
        when(domainService.getEntities())
                .thenReturn(returnValue);

        var result = facade.getEntities();

        verify(domainService).getEntities();
        assertEquals(returnValue, result);
    }
}

