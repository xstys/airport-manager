package cz.muni.fi.pa165.airportmanager.stewardmodule.mapper;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ActiveProfiles("test")
public class StewardMapperTest {
    private final String firstName = "Sam";
    private final String lastName = "Lastname";
    private final UUID airlineId = UUID.randomUUID();
    private final UUID currentLocationId = UUID.randomUUID();

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščé í^˘˛ˇ856"})
    public void stewardInsertDtoToStewardFirstNameTest(String firstName) {
        var steward = new StewardInsertDto(firstName, lastName, airlineId,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertNotNull(result.getId());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Smith", "Novák", "+ěščéí ^˘˛ˇ856"})
    public void stewardInsertDtoToStewardLastNameTest(String lastName) {
        var steward = new StewardInsertDto(firstName, lastName, airlineId,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertNotNull(result.getId());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardInsertDtoToStewardAirlineIdTest(UUID airline) {
        var steward = new StewardInsertDto(firstName, lastName, airline,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertNotNull(result.getId());
        assertEquals(airline, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardInsertDtoToStewardCurrentLocationIdTest(UUID currentLocation) {
        var steward = new StewardInsertDto(firstName, lastName, airlineId,
                                           currentLocation);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertNotNull(result.getId());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocation, result.getCurrentLocationId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščéí^ ˘˛ˇ856"})
    public void stewardUpdateDtoToStewardFirstNameTest(String firstName) {
        var steward = new StewardUpdateDto(UUID.randomUUID(), firstName,
                                           lastName, airlineId,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Smith", "Novák", "+ěščéí ^˘˛ˇ856"})
    public void stewardUpdateDtoToStewardLastNameTest(String lastName) {
        var steward = new StewardUpdateDto(UUID.randomUUID(), firstName,
                                           lastName, airlineId,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardUpdateDtoToStewardAirlineIdTest(UUID airline) {
        var steward = new StewardUpdateDto(UUID.randomUUID(), firstName,
                                           lastName, airline,
                                           currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airline, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardUpdateDtoToStewardCurrentLocationIdTest(UUID currentLocation) {
        var steward = new StewardUpdateDto(UUID.randomUUID(), firstName,
                                           lastName, airlineId,
                                           currentLocation);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocation, result.getCurrentLocationId());
        assertEquals(steward.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščé í^˘˛ˇ856"})
    public void stewardFindDtoToStewardFirstNameTest(String firstName) {
        var steward = new Steward(UUID.randomUUID(), firstName, lastName,
                                  airlineId, currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Smith", "Novák", "+ěščéí ^˘˛ˇ856"})
    public void stewardFindDtoToStewardLastNameTest(String lastName) {
        var steward = new Steward(firstName, lastName, airlineId,
                                  currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardFindDtoToStewardAirlineIdTest(UUID airline) {
        var steward = new Steward(firstName, lastName, airline,
                                  currentLocationId);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airline, result.getAirlineId());
        assertEquals(currentLocationId, result.getCurrentLocationId());
        assertEquals(steward.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardFindDtoToStewardCurrentLocationIdTest(UUID currentLocation) {
        var steward = new Steward(firstName, lastName, airlineId,
                                  currentLocation);
        var result = new StewardMapper().map(steward);

        assertEquals(firstName, result.getFirstName());
        assertEquals(lastName, result.getLastName());
        assertEquals(airlineId, result.getAirlineId());
        assertEquals(currentLocation, result.getCurrentLocationId());
        assertEquals(steward.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščé í^˘˛ˇ856"})
    public void stewardListDtoToStewardFindDtoFirstNameTest(String firstName) {
        var steward = List.of(new Steward(UUID.randomUUID(), firstName,
                                          lastName, airlineId,
                                          currentLocationId));
        var result = new StewardMapper().map(steward);

        assertEquals(1, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertEquals(lastName, result.get(0).getLastName());
        assertEquals(airlineId, result.get(0).getAirlineId());
        assertEquals(currentLocationId, result.get(0).getCurrentLocationId());
        assertEquals(steward.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Smith", "Novák", "+ěščéí ^˘˛ˇ856"})
    public void stewardListDtoToStewardFindDtoLastNameTest(String lastName) {
        var steward = List.of(new Steward(firstName, lastName, airlineId,
                                          currentLocationId));
        var result = new StewardMapper().map(steward);

        assertEquals(1, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertEquals(lastName, result.get(0).getLastName());
        assertEquals(airlineId, result.get(0).getAirlineId());
        assertEquals(currentLocationId, result.get(0).getCurrentLocationId());
        assertEquals(steward.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardListDtoToStewardFindDtoAirlineIdTest(UUID airline) {
        var steward = List.of(
                new Steward(firstName, lastName, airline, currentLocationId));
        var result = new StewardMapper().map(steward);

        assertEquals(1, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertEquals(lastName, result.get(0).getLastName());
        assertEquals(airline, result.get(0).getAirlineId());
        assertEquals(currentLocationId, result.get(0).getCurrentLocationId());
        assertEquals(steward.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"123e4567-e89b-12d3-a456-426614174000"})
    public void stewardListDtoToStewardFindDtoCurrentLocationIdTest(UUID currentLocation) {
        var steward = List.of(
                new Steward(firstName, lastName, airlineId, currentLocation));
        var result = new StewardMapper().map(steward);

        assertEquals(1, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertEquals(lastName, result.get(0).getLastName());
        assertEquals(airlineId, result.get(0).getAirlineId());
        assertEquals(currentLocation, result.get(0).getCurrentLocationId());
        assertEquals(steward.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Jiří", "+ěščé í^˘˛ˇ856"})
    public void stewardListMultipleDtoToStewardFindDtoTest(String firstName) {
        var steward = List.of(new Steward(UUID.randomUUID(), firstName,
                                          lastName, airlineId,
                                          currentLocationId),
                              new Steward(UUID.randomUUID(), lastName,
                                          firstName, airlineId,
                                          currentLocationId));
        var result = new StewardMapper().map(steward);

        assertEquals(2, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertEquals(lastName, result.get(0).getLastName());
        assertEquals(airlineId, result.get(0).getAirlineId());
        assertEquals(currentLocationId, result.get(0).getCurrentLocationId());
        assertEquals(steward.get(0).getId(), result.get(0).getGuid());
        assertEquals(lastName, result.get(1).getFirstName());
        assertEquals(firstName, result.get(1).getLastName());
        assertEquals(airlineId, result.get(1).getAirlineId());
        assertEquals(currentLocationId, result.get(1).getCurrentLocationId());
        assertEquals(steward.get(1).getId(), result.get(1).getGuid());
    }
}
