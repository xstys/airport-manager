package cz.muni.fi.pa165.airportmanager.stewardmodule.endtoendtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository.StewardRepository;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class StewardEndToEndTests {
    @Autowired
    private StewardRepository stewardRepository;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private RetrieveEntityServiceImpl<AirlineFindDto> airlineService;
    private final UUID airlineId = UUID.randomUUID();
    @MockBean
    private RetrieveEntityServiceImpl<DestinationFindDto> destinationService;
    private final UUID currentLocationId = UUID.randomUUID();

    @BeforeEach
    public void setUp() {
        when(airlineService.retrieve(any(), any()))
                .thenReturn(
                        new AirlineFindDto(airlineId, "Empty Airlines", null));
        when(destinationService.retrieve(any(), any()))
                .thenReturn(
                        new DestinationFindDto(currentLocationId,
                                          "Destination", "USA"));
    }

    @AfterEach
    public void cleanUp() {
        stewardRepository.deleteAll();
    }

    @Test
    public void findAllStewardsTest() throws Exception {
        var stewardOne = stewardRepository.save(
                new Steward("First", "Person",
                            airlineId, currentLocationId));
        var stewardTwo = stewardRepository.save(
                new Steward("Second", "Person",
                            airlineId, currentLocationId));

        ResultActions result = mockMvc.perform(get("/api/v1/stewards"));
        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<StewardFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(2, entities.size()),
                () -> assertEquals(stewardOne.getFirstName(),
                                   entities.get(0).getFirstName()),
                () -> assertEquals(stewardOne.getLastName(),
                                   entities.get(0).getLastName()),
                () -> assertEquals(stewardOne.getAirlineId(),
                                   entities.get(0).getAirlineId()),
                () -> assertEquals(stewardOne.getCurrentLocationId(),
                                   entities.get(0).getCurrentLocationId()),
                () -> assertEquals(stewardTwo.getFirstName(),
                                   entities.get(1).getFirstName()),
                () -> assertEquals(stewardTwo.getLastName(),
                                   entities.get(1).getLastName()),
                () -> assertEquals(stewardTwo.getAirlineId(),
                                   entities.get(1).getAirlineId()),
                () -> assertEquals(stewardTwo.getCurrentLocationId(),
                                   entities.get(1).getCurrentLocationId())
        );
    }

    @Test
    public void findNonDeletedStewardsTest() throws Exception {
        var stewardOne = stewardRepository.save(
                new Steward("First", "Person",
                            airlineId, currentLocationId));
        var nondeletedStewardTwo = new Steward("Second", "Person",
                                               airlineId, currentLocationId);
        nondeletedStewardTwo.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));
        var stewardTwo = stewardRepository.save(nondeletedStewardTwo);

        ResultActions result = mockMvc.perform(get("/api/v1/stewards"));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<StewardFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(1, entities.size()),
                () -> assertEquals(stewardOne.getFirstName(),
                                   entities.get(0).getFirstName()),
                () -> assertEquals(stewardOne.getLastName(),
                                   entities.get(0).getLastName()),
                () -> assertEquals(stewardOne.getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(stewardOne.getAirlineId(),
                                   entities.get(0).getAirlineId()),
                () -> assertEquals(stewardOne.getCurrentLocationId(),
                                   entities.get(0).getCurrentLocationId())
        );
    }

    @Test
    public void findStewardTest() throws Exception {
        var stewardOne = stewardRepository.save(
                new Steward("First", "Person", airlineId, currentLocationId));

        ResultActions result = mockMvc.perform(get("/api/v1/stewards"));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<StewardFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(1, entities.size()),
                () -> assertEquals(stewardOne.getFirstName(),
                                   entities.get(0).getFirstName()),
                () -> assertEquals(stewardOne.getLastName(),
                                   entities.get(0).getLastName()),
                () -> assertEquals(stewardOne.getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(stewardOne.getAirlineId(),
                                   entities.get(0).getAirlineId()),
                () -> assertEquals(stewardOne.getCurrentLocationId(),
                                   entities.get(0).getCurrentLocationId())
        );
    }

    @Test
    public void findStewardForNonExistentIdTest() throws Exception {
        var steward = stewardRepository.save(
                new Steward("First", "Person", airlineId, currentLocationId));

        UUID nonExistentUUID = UUID.randomUUID();

        while (nonExistentUUID == steward.getId()) {
            nonExistentUUID = UUID.randomUUID();
        }

        ResultActions result = mockMvc.perform(
                get("/api/v1/stewards/" + nonExistentUUID));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void findStewardForDeletedTest() throws Exception {
        var steward = stewardRepository.save(
                new Steward("First", "Person", airlineId, currentLocationId));

        steward.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1));
        stewardRepository.save(steward);

        ResultActions result = mockMvc.perform(
                get("/api/v1/stewards/" + steward.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteTest() throws Exception {
        var steward = stewardRepository.save(
                new Steward("First", "Person", airlineId, currentLocationId));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + steward.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertNotEquals(null,
                        stewardRepository.findById(steward.getId())
                                .get().getDeleted());
    }

    @Test
    public void deleteTestNonExistent() throws Exception {
        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void deleteTestDeleted() throws Exception {
        var steward = stewardRepository.save(
                new Steward("First", "Person", airlineId, currentLocationId));

        steward.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));
        steward = stewardRepository.save(steward);

        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + steward.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void addIncorrectInputTest() throws Exception {
        var steward = new StewardInsertDto("F|rst", "Person", airlineId,
                                            currentLocationId);

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(steward);

        ResultActions result = mockMvc.perform(post("/api/v1/stewards")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));
        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateIncorrectInputTest() throws Exception {
        var oldSteward = stewardRepository.save(
                new Steward("First", "Premier", airlineId, currentLocationId));

        var steward = new StewardUpdateDto(oldSteward.getId(),
                "F|rst", "Person", airlineId, currentLocationId);

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(steward);

        ResultActions result = mockMvc.perform(patch("/api/v1/stewards")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        var items = stewardRepository.findAll();

        assertEquals(1, items.size());
        assertEquals(oldSteward.getFirstName(), items.get(0).getFirstName());
        assertEquals(oldSteward.getLastName(), items.get(0).getLastName());
        assertEquals(oldSteward.getAirlineId(), items.get(0).getAirlineId());
        assertEquals(oldSteward.getCurrentLocationId(),
                     items.get(0).getCurrentLocationId());
        assertNotEquals(null, items.get(0).getCreated());
        assertNull(items.get(0).getDeleted());
        assertNull(items.get(0).getUpdated());
    }
}
