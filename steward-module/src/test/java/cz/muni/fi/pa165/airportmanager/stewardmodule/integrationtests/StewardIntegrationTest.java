package cz.muni.fi.pa165.airportmanager.stewardmodule.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository.StewardRepository;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class StewardIntegrationTest {
    @MockBean
    private StewardRepository stewardRepository;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private RetrieveEntityServiceImpl<AirlineFindDto> airlineService;
    private final UUID airlineId = UUID.randomUUID();
    @MockBean
    private RetrieveEntityServiceImpl<DestinationFindDto> destinationService;
    private final UUID currentLocationId = UUID.randomUUID();
    
    @BeforeEach
    public void setUp() {
        when(airlineService.retrieve(eq(airlineId), anyString())).
                thenReturn(
                        new AirlineFindDto(airlineId, "Empty Airlines", null));
        when(destinationService.retrieve(eq(currentLocationId), anyString()))
                .thenReturn(
                        new DestinationFindDto(currentLocationId,
                                          "Destination", "USA"));}

    @Test
    public void findStewardTest() throws Exception {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);

        when(stewardRepository.findById(any()))
                .thenReturn(Optional.of(steward));

        ResultActions result = mockMvc.perform(
                get("/api/v1/stewards/" + steward.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        StewardFindDto entity = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(steward.getFirstName(),
                                   entity.getFirstName()),
                () -> assertEquals(steward.getLastName(),
                                   entity.getLastName()),
                () -> assertEquals(steward.getAirlineId(),
                                   entity.getAirlineId()),
                () -> assertEquals(steward.getCurrentLocationId(),
                                   entity.getCurrentLocationId()),
                () -> assertEquals(steward.getId(), entity.getGuid())
        );
    }

    @Test
    public void findStewardForNonExistentIdTest() throws Exception {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);

        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        UUID nonExistentUUID = UUID.randomUUID();

        while (nonExistentUUID.equals(steward.getId())) {
            nonExistentUUID = UUID.randomUUID();
        }

        ResultActions result = mockMvc.perform(
                get("/api/v1/stewards/" + nonExistentUUID));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void findStewardForDeletedTest() throws Exception {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);
        steward.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1));

        when(stewardRepository.findById(any())).thenReturn(Optional.of(steward));

        ResultActions result = mockMvc.perform(
                get("/api/v1/stewards/" + steward.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteTest() throws Exception {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);

        when(stewardRepository.findById(any()))
                .thenReturn(Optional.of(steward));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        verify(stewardRepository).findById(any());
        verify(stewardRepository, times(0)).delete(any());
        verify(stewardRepository).save(any());
        assertNotEquals(null, steward.getDeleted());
        assertNull(steward.getUpdated());
    }

    @Test
    public void deleteTestNongExistent() throws Exception {
        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
        verify(stewardRepository).findById(any());
        verify(stewardRepository, times(0)).delete(any());
    }

    @Test
    public void deleteTestDeleted() throws Exception {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);
        steward.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));

        when(stewardRepository.findById(any())).thenReturn(Optional.of(steward));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/stewards/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        verify(stewardRepository).findById(any());
        verify(stewardRepository, times(0)).delete(any());
    }
}
