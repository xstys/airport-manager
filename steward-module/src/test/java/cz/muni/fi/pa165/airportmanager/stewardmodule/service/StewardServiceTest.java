package cz.muni.fi.pa165.airportmanager.stewardmodule.service;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository.StewardRepository;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.mapper.StewardMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class StewardServiceTest {
    private final UUID airlineId = UUID.randomUUID();
    private final UUID currentLocationId = UUID.randomUUID();
    private final StewardRepository stewardRepository = mock(StewardRepository.class);
    private final StewardMapper stewardMapper = mock(StewardMapper.class);
    private final StewardService stewardService = new StewardServiceImpl(stewardRepository, stewardMapper);

    @Test
    public void addTest() {
        var steward = new StewardInsertDto("First", "Last",
                                           airlineId, currentLocationId);
        when(stewardRepository.save(any())).thenReturn(new Steward());

        stewardService.addEntity(steward);

        verify(stewardRepository).save(any());
    }

    @Test
    public void addNullFirstNameTest() {
        var steward = new StewardInsertDto(null, "Last",
                                           airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addNullLastNameTest() {
        var steward = new StewardInsertDto("First", null,
                                           airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addNullAirlineIdTest() {
        var steward = new StewardInsertDto("First", "Last",
                                    null, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addNullCurrentLocationIdTest() {
        var steward = new StewardInsertDto("First", "Last",
                                           airlineId, null);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(null));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(null));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addSpecialCharacterInFirstNameTest() {
        var steward = new StewardInsertDto("First~Name", "Last",
                                           airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void addSpecialCharacterInLastNameTest() {
        var steward = new StewardInsertDto("First", "Last-Hyphen",
                                            airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.addEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(),"First",
                                  "Last", airlineId, currentLocationId);
        var stewardEntity = new Steward(steward.getGuid(),
                                        steward.getFirstName(),
                                        steward.getLastName(),
                                        airlineId,
                                        currentLocationId);

        when(stewardRepository.findById(any()))
                .thenReturn(Optional.of(new Steward(UUID.randomUUID(),
                                            "Another", "Name",
                                                    airlineId,
                                                    currentLocationId)));
        when(stewardMapper.map(steward)).thenReturn(stewardEntity);

        stewardService.updateEntity(steward);

        verify(stewardRepository).save(any());
    }

    @Test
    public void updateNonexistentTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(),"First",
                                            "Last", airlineId,
                                            currentLocationId);

        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateNullFirstNameTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(), null,
                                   "Last", airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateNullLastNameTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(),"First",
                                   null, airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateNullAirlineIdTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(), "First",
                                  "Last", null, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateNullCurrentLocationIdTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(),"First",
                                   "Last", airlineId, null);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }
    
    @Test
    public void updateSpecialCharacterInFirstNameTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(), "First~Name",
                                    "Last", airlineId, currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void updateSpecialCharacterInLastNameTest() {
        var steward = new StewardUpdateDto(UUID.randomUUID(), "First",
                                           "Last-Hyphen", airlineId,
                                           currentLocationId);

        assertThrows(InvalidUserInputException.class,
                     () -> stewardService.updateEntity(steward));

        verify(stewardRepository, times(0)).save(any());
    }

    @Test
    public void findTest() {
        var steward = new Steward("First", "Last",
                                 airlineId, currentLocationId);
        var stewardDto = new StewardFindDto(steward.getId(),
                                            steward.getFirstName(),
                                            steward.getLastName(),
                                            steward.getAirlineId(),
                                            steward.getCurrentLocationId());

        when(stewardRepository.findById(steward.getId()))
                .thenReturn(Optional.of(steward));
        when(stewardMapper.map(steward)).thenReturn(stewardDto);

        var result = stewardService.findById(steward.getId());

        verify(stewardRepository, times(1)).findById(any());
        assertEquals(stewardDto, result);
    }

    @Test
    public void findMissingTest() {
        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> stewardService.findById(UUID.randomUUID()));

        verify(stewardRepository, times(1)).findById(any());
        verify(stewardMapper, times(0)).map(any(Steward.class));
    }

    @Test
    public void findNullTest() {
        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> stewardService.findById(null));

        verify(stewardRepository, times(1)).findById(any());
        verify(stewardMapper, times(0)).map(any(Steward.class));
    }

    @Test
    public void deleteNullTest() {
        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> stewardService.delete(null));

        verify(stewardRepository, times(0)).delete(any());
    }

    @Test
    public void deleteMissingTest() {
        when(stewardRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> stewardService.delete(UUID.randomUUID()));

        verify(stewardRepository, times(0)).delete(any());
    }

    @Test
    public void deleteExistingTest() {
        var id = UUID.randomUUID();

        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);

        when(stewardRepository.findById(id)).thenReturn(Optional.of(steward));

        stewardService.delete(id);

        verify(stewardRepository, times(1)).save(steward);

    }

    @Test
    public void getEntitiesSingleTest() {
        var id = UUID.randomUUID();

        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);
        var stewardDto = new StewardFindDto(id, steward.getFirstName(),
                                            steward.getLastName(),
                                            steward.getAirlineId(),
                                            steward.getCurrentLocationId());

        when(stewardRepository.findAll()).thenReturn(List.of(steward));
        when(stewardMapper.map(anyList())).thenReturn(List.of(stewardDto));

        var result = stewardService.getEntities();

        verify(stewardRepository, times(1)).findByDeletedIsNull();;
        assertEquals(1, result.size());
        assertEquals(stewardDto, result.get(0));
    }

    @Test
    public void getEntitiesMultipleTest() {
        var stewards = List.of(
                new Steward("First", "Last", airlineId, currentLocationId),
                new Steward("Second", "Last", airlineId, currentLocationId));

        var stewardDtos = List.of(
                new StewardFindDto(stewards.get(0).getId(),
                                   stewards.get(0).getFirstName(),
                                   stewards.get(0).getLastName(),
                                   stewards.get(0).getAirlineId(),
                                   stewards.get(0).getCurrentLocationId()),
                new StewardFindDto(stewards.get(1).getId(),
                                   stewards.get(1).getFirstName(),
                                   stewards.get(1).getLastName(),
                                   stewards.get(1).getAirlineId(),
                                   stewards.get(1).getCurrentLocationId()));

        when(stewardRepository.findAll()).thenReturn(stewards);
        when(stewardMapper.map(anyList())).thenReturn(stewardDtos);

        var result = stewardService.getEntities();

        verify(stewardRepository, times(1)).findByDeletedIsNull();
        assertEquals(2, result.size());
        assertEquals(stewardDtos.get(0), result.get(0));
        assertEquals(stewardDtos.get(1), result.get(1));
    }

    @Test
    public void getEntitiesByIdsSingleTest() {
        var steward = new Steward("First", "Last", airlineId,
                                  currentLocationId);
        var stewardDto = new StewardFindDto(steward.getId(),
                                            steward.getFirstName(),
                                            steward.getLastName(),
                                            steward.getAirlineId(),
                                            steward.getCurrentLocationId());
        var idList = List.of(steward.getId());

        when(stewardRepository.findAllById(idList)).thenReturn(List.of(steward));
        when(stewardMapper.map(anyList())).thenReturn(List.of(stewardDto));

        var result = stewardService.getEntitiesByIds(idList);

        verify(stewardRepository, times(1))
                .findByIdInAndDeletedIsNull(idList);
        assertEquals(1, result.size());
        assertEquals(stewardDto, result.get(0));
    }

    @Test
    public void getEntitiesByIdsEmptyTest() {
        when(stewardRepository.findAllById(List.of())).thenReturn(List.of());
        when(stewardMapper.map(anyList())).thenReturn(List.of());

        var result = stewardService.getEntitiesByIds(List.of());

        verify(stewardRepository, times(1))
                .findByIdInAndDeletedIsNull(List.of());
        assertEquals(0, result.size());
    }

    @Test
    public void getEntitiesByIdsMultipleTest() {
        var stewards = List.of(
                new Steward("First", "Last", airlineId, currentLocationId),
                new Steward("Second", "Last", airlineId, currentLocationId));

        var stewardDtos = List.of(
                new StewardFindDto(stewards.get(0).getId(),
                                   stewards.get(0).getFirstName(),
                                   stewards.get(0).getLastName(),
                                   stewards.get(0).getAirlineId(),
                                   stewards.get(0).getCurrentLocationId()),
                new StewardFindDto(stewards.get(1).getId(),
                                   stewards.get(1).getFirstName(),
                                   stewards.get(1).getLastName(),
                                   stewards.get(1).getAirlineId(),
                                   stewards.get(1).getCurrentLocationId()));

        var idList = List.of(stewards.get(0).getId(), stewards.get(1).getId());

        when(stewardRepository.findAllById(idList)).thenReturn(stewards);
        when(stewardMapper.map(anyList())).thenReturn(stewardDtos);

        var result = stewardService.getEntitiesByIds(idList);

        verify(stewardRepository, times(1))
                .findByIdInAndDeletedIsNull(idList);
        assertEquals(2, result.size());
        assertEquals(stewardDtos.get(0), result.get(0));
        assertEquals(stewardDtos.get(1), result.get(1));
    }
}
