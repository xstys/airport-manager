package cz.muni.fi.pa165.airportmanager.stewardmodule.rest;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.facade.StewardFacade;
import cz.muni.fi.pa165.airportmanager.stewardmodule.facade.StewardFacadeImpl;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.rest.DomainController;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@OpenAPIDefinition( // metadata for inclusion into OpenAPI document
        // see javadoc at https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
        // see docs at https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(title = "Steward Service",
                version = "0.1",
                description = """
                        Simple service for steward handling at an airport.
                        The API has operations for:
                        - getting all stewards
                        - adding a new steward
                        - getting a specific steward by their id
                        - removing a specific steward by their id
                        """,
                contact = @Contact(name = "Krystof-Mikulas Stys",
                                   email = "493159@mail.muni.cz",
                                   url = "https://is.muni.cz/auth/osoba/" +
                                           "493159"),
                license = @License(name = "Apache 2.0",
                                   url = "https://www.apache.org/licenses/" +
                                           "LICENSE-2.0.html")
        ),
        servers = @Server(description = "localhost server",
                          url = "{scheme}://{server}:{port}",
                          variables = {
                            @ServerVariable(name = "scheme",
                                            allowableValues = {"http", "https"},
                                            defaultValue = "http"),
                            @ServerVariable(name = "server",
                                            defaultValue = "localhost"),
                            @ServerVariable(name = "port",
                                            defaultValue = "5005"),
        })
)
@Tag(name = "Steward", description = "microservice for steward handling")
// metadata for inclusion into OpenAPI document
@RequestMapping(path = "api/v1/", produces = APPLICATION_JSON_VALUE)
// common prefix for all URLs handled by this controller
public class StewardController
        extends DomainController<Steward, StewardInsertDto,
                                 StewardUpdateDto, StewardFindDto> {
    @Autowired
    public StewardController(StewardFacade facade) {
        super(facade);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all stewards",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - stewards were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Returns an array of objects representing stewards,
                    ordered by their seniority.
                    """)
    @GetMapping("stewards") // URL mapping of this operation
    @CrossOrigin(origins = "*") // CORS headers needed for JavaScript clients
    public List<StewardFindDto> getEntities() {
        return super.getEntities();
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get stewards with the specified ids " +
                      "in the UUID/GUID format.",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - stewards were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Returns an array of objects representing stewards who had
                    their id specified in the input list.
                    """)
    @GetMapping("stewards/id") // URL mapping of this operation
    @Parameter(in = ParameterIn.QUERY, name = "ids",
               schema = @Schema(type = "array", implementation = UUID.class))
    @CrossOrigin(origins = "*") // CORS headers needed for JavaScript clients
    public List<StewardFindDto> getEntitiesByIds(@RequestParam("ids") List<UUID> ids) {
        return ((StewardFacade) facade).getEntitiesByIds(ids);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Add a new steward",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - steward was " +
                                         "successfully added"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token does " +
                                         "not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Adds a provided steward and returns its ID
                    in the UUID/GUID format.
                    """)
    @PostMapping("stewards") // URL mapping of this operation
    public UUID addEntity(@RequestHeader(name = "Authorization", required = false) @Schema(hidden = true)
                              String authorizationHeader, @RequestBody StewardInsertDto insertDto) {
        return ((StewardFacadeImpl) facade).addEntity(insertDto, authorizationHeader);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Changes an existing steward",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - steward was " +
                                         "successfully updated"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token does " +
                                         "not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Changes an existing steward with the values
                    from provided steward.
                    """)
    @PatchMapping("stewards")
    public void updateEntity(@RequestHeader(name = "Authorization", required = false) @Schema(hidden = true)
                                 String authorizationHeader,
                             @RequestBody StewardUpdateDto updateDto) {
        ((StewardFacadeImpl) facade).updateEntity(updateDto, authorizationHeader);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Removes an existing steward",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - steward was " +
                                         "successfully deleted"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Removes an existing steward with the specified ID
                    in the UUID/GUID format.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string", implementation = UUID.class))
    @DeleteMapping("stewards/{id}")
    public void delete(@Valid @Parameter(hidden = true)
                           @PathVariable UUID id) {
        super.delete(id);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing steward",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - steward was " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Retrieves an existing steward with the specified ID
                    in the UUID/GUID format.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string", implementation = UUID.class))
    @GetMapping("stewards/{id}")
    public StewardFindDto findById(@Valid @Parameter(hidden = true)
                                       @PathVariable UUID id) {
        return super.findById(id);
    }
}
