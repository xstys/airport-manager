package cz.muni.fi.pa165.airportmanager.stewardmodule.facade;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.service.StewardService;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacadeImpl;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StewardFacadeImpl
        extends DomainFacadeImpl<Steward, StewardInsertDto,
                                 StewardUpdateDto, StewardFindDto>
        implements StewardFacade {
    private final RetrieveEntityServiceImpl<AirlineFindDto> airlineService;
    private final RetrieveEntityServiceImpl<DestinationFindDto> destinationService;

    @Autowired
    public StewardFacadeImpl(StewardService domainService,
                             RetrieveEntityServiceImpl<AirlineFindDto> airlineService,
                             RetrieveEntityServiceImpl<DestinationFindDto> destinationService) {
        super(domainService);
        this.airlineService = airlineService;
        this.destinationService = destinationService;
    }

    @Override
    public List<StewardFindDto> getEntitiesByIds(List<UUID> ids) {
        return ((StewardService) domainService).getEntitiesByIds(ids);
    }

    public UUID addEntity(StewardInsertDto insertDto, String authenticationHeader) {
        if (airlineService.retrieve(insertDto.getAirlineId(), authenticationHeader)
                == null) {
            throw new ObjectNotFoundException("Unknown airline was provided.");
        }

        if (destinationService
                .retrieve(insertDto.getCurrentLocationId(),
                          authenticationHeader)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown destination was provided.");
        }

        return super.addEntity(insertDto);
    }

    public void updateEntity(StewardUpdateDto updateDto, String token) {
        if (airlineService.retrieve(updateDto.getAirlineId(), token) == null) {
            throw new ObjectNotFoundException("Unknown airline was provided.");
        }

        if (destinationService.retrieve(updateDto.getCurrentLocationId(), token)
                == null) {
            throw new ObjectNotFoundException(
                    "Unknown destination was provided.");
        }

        super.updateEntity(updateDto);
    }
}
