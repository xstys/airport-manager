package cz.muni.fi.pa165.airportmanager.stewardmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.UUID;

public class StewardUpdateDto extends DomainUpdateDto {
    private final String firstName;
    private final String lastName;
    private final UUID airlineId;
    private final UUID currentLocationId;

    public StewardUpdateDto() {
        super(null);
        firstName = "";
        lastName = "";
        airlineId = null;
        currentLocationId = null;
    }

    public StewardUpdateDto(UUID guid, String firstName, String lastName,
                            UUID airlineId, UUID currentLocationId) {
        super(guid);
        this.firstName = firstName;
        this.lastName = lastName;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }
}
