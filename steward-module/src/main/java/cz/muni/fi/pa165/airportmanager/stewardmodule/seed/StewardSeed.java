package cz.muni.fi.pa165.airportmanager.stewardmodule.seed;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository.StewardRepository;

import org.javatuples.Quartet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Profile("dev")
public class StewardSeed {
    private final StewardRepository stewardRepository;

    @Autowired
    public StewardSeed(StewardRepository stewardRepository) {
        this.stewardRepository = stewardRepository;
        seed();
    }

    private void seed() {
        List<Quartet<String, String, String, String>> stewards = List.of(
                Quartet.with("Albert", "Vykukal", "Czexpats Airlines",
                        "Prague, Czech Republic"),
                Quartet.with("Sára", "Daňková", "Czexpats Airlines",
                        "Prague, Czech Republic"),
                Quartet.with("Viktor", "Král", "Czexpats Airlines",
                        "Prague, Czech Republic"),
                Quartet.with("Julie", "Hrdličková", "Czexpats Airlines",
                        "Athens, Greece"),
                Quartet.with("Dušan", "Opustil", "Czexpats Airlines",
                        "Athens, Greece"),
                Quartet.with("Antonín", "Opustil", "Mom and Pop Airlines",
                        "Vienna, Austria"),
                Quartet.with("Markéta", "Opustilová", "Mom and Pop Airlines",
                        "Vienna, Austria")
        );

        for (Quartet<String, String, String, String> s: stewards) {
            UUID id = UUID.nameUUIDFromBytes(
                    (s.getValue0() + ", " + s.getValue1() + ", " + s.getValue2())
                            .getBytes());
            UUID airlineId = UUID.nameUUIDFromBytes(s.getValue2().getBytes());
            UUID destinationId = UUID.nameUUIDFromBytes(
                    s.getValue3().getBytes());

            stewardRepository.save(
                    new Steward(id, s.getValue0(), s.getValue1(),
                                airlineId, destinationId)
            );
        }
    }
}