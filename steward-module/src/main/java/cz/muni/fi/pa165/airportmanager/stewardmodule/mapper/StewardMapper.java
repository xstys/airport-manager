package cz.muni.fi.pa165.airportmanager.stewardmodule.mapper;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StewardMapper
        implements DomainMapper<Steward, StewardInsertDto,
                                StewardUpdateDto, StewardFindDto> {
    @Override
    public Steward map(StewardInsertDto stewardInsertDto) {
        if (stewardInsertDto == null) {
            return null;
        }

        return new Steward(stewardInsertDto.getFirstName(),
                           stewardInsertDto.getLastName(),
                           stewardInsertDto.getAirlineId(),
                           stewardInsertDto.getCurrentLocationId());
    }

    @Override
    public Steward map(StewardUpdateDto stewardUpdateDto) {
        if (stewardUpdateDto == null) {
            return null;
        }

        return new Steward(stewardUpdateDto.getGuid(),
                           stewardUpdateDto.getFirstName(),
                           stewardUpdateDto.getLastName(),
                           stewardUpdateDto.getAirlineId(),
                           stewardUpdateDto.getCurrentLocationId());
    }

    @Override
    public StewardFindDto map(Steward entity) {
        if (entity == null) {
            return null;
        }

        return new StewardFindDto(entity.getId(), entity.getFirstName(),
                                  entity.getLastName(), entity.getAirlineId(),
                                  entity.getCurrentLocationId());
    }

    @Override
    public List<StewardFindDto> map(List<Steward> entity) {
        return entity.stream()
                .map(this::map)
                .toList();
    }
}
