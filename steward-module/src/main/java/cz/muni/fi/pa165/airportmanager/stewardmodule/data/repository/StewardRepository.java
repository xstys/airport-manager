package cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface StewardRepository extends DomainRepository<Steward> {
}
