package cz.muni.fi.pa165.airportmanager.stewardmodule.facade;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;

import java.util.List;
import java.util.UUID;

public interface StewardFacade
        extends DomainFacade<Steward, StewardInsertDto,
                             StewardUpdateDto, StewardFindDto> {
    List<StewardFindDto> getEntitiesByIds(List<UUID> ids);
}
