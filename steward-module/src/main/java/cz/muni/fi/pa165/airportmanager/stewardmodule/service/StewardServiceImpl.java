package cz.muni.fi.pa165.airportmanager.stewardmodule.service;

import cz.muni.fi.pa165.airportmanager.stewardmodule.data.model.Steward;
import cz.muni.fi.pa165.airportmanager.stewardmodule.data.repository.StewardRepository;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardInsertDto;
import cz.muni.fi.pa165.airportmanager.stewardmodule.dto.StewardUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.StewardFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StewardServiceImpl
        extends DomainServiceImpl<Steward, StewardInsertDto,
                                  StewardUpdateDto, StewardFindDto>
        implements StewardService {
    @Autowired
    public StewardServiceImpl(StewardRepository stewardRepository,
                              DomainMapper<Steward, StewardInsertDto,
                                           StewardUpdateDto,
                                           StewardFindDto> mapper) {
        super(stewardRepository, mapper);
    }

    @Override
    public UUID addEntity(StewardInsertDto insertDto) {
        if (entityCheck(insertDto)) {
            return super.addEntity(insertDto);
        }

        throw new InvalidUserInputException("Invalid steward was provided!");
    }

    @Override
    public void updateEntity(StewardUpdateDto updateDto) {
        if (entityCheck(updateDto)) {
            super.updateEntity(updateDto);
        }
    }

    @Override
    public List<StewardFindDto> getEntitiesByIds(List<UUID> ids) {
        return domainMapper.map(
                domainRepository.findByIdInAndDeletedIsNull(ids));
    }

    private boolean entityCheck(StewardUpdateDto steward) {
        if (steward == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (steward.getClass() != StewardUpdateDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct Steward class!");
        }

        return attributesCheck(steward.getFirstName(), steward.getLastName(),
                               steward.getAirlineId(),
                               steward.getCurrentLocationId());
    }

    private boolean entityCheck(StewardInsertDto steward) {
        if (steward == null) {
            throw new InvalidUserInputException(
                    "The provided class cannot be null!");
        }

        if (steward.getClass() != StewardInsertDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct Steward class!");
        }

        return attributesCheck(steward.getFirstName(), steward.getLastName(),
                               steward.getAirlineId(),
                               steward.getCurrentLocationId());
    }

    private boolean attributesCheck(String firstName, String lastName,
                                    UUID airlineId, UUID currentLocationId) {
        if (firstName == null || lastName == null) {
            throw new InvalidUserInputException(
                    "A null value is not a legal name.");
        }

        if (firstName.isBlank()
                || !firstName.chars().allMatch(Character::isLetter)) {
            throw new InvalidUserInputException(
                    "The first name was not made out of letters.");
        }

        if (lastName.isBlank()
                || !lastName.chars().allMatch(Character::isLetter)) {
            throw new InvalidUserInputException(
                    "The last name was not made out of letters.");
        }

        if (airlineId == null) {
            throw new InvalidUserInputException(
                    "Airline GUID needs to be provided.");
        }

        if (currentLocationId == null) {
            throw new InvalidUserInputException(
                    "Current location GUID needs to be provided.");
        }

        return true;
    }
}
