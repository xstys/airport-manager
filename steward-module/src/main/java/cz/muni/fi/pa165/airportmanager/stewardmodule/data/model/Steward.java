package cz.muni.fi.pa165.airportmanager.stewardmodule.data.model;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import jakarta.validation.constraints.Size;

import java.util.UUID;

@Entity
@Table(name="STEWARD")
public class Steward extends DomainEntity {
    @Size(min = 1)
    @Column(name="FIRSTNAME", length=1019, nullable=false, unique=false)
    private String firstName;
    @Size(min = 1)
    @Column(name="LASTNAME", length=747, nullable=false, unique=false)
    private String lastName;
    @Column(name="AIRLINE_ID", nullable=false)
    private UUID airlineId;
    @Column(name="CURRENT_LOCATION_ID", nullable=false)
    private UUID currentLocationId;

    public Steward() {
        super(null);
    }

    public Steward(String firstName, String lastName, UUID airlineId,
                   UUID currentLocationId) {
        super(null);

        this.firstName = firstName;
        this.lastName = lastName;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public Steward(UUID guid, String firstName, String lastName,
                   UUID airlineId, UUID currentLocationId) {
        super(guid);

        this.firstName = firstName;
        this.lastName = lastName;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAirlineId(UUID airlineId) {
        this.airlineId = airlineId;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }

    public void setCurrentLocationId(UUID currentLocationId) {
        this.currentLocationId = currentLocationId;
    }
}
