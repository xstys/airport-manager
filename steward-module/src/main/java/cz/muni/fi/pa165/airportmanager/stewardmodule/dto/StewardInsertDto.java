package cz.muni.fi.pa165.airportmanager.stewardmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;

import java.util.UUID;

public class StewardInsertDto extends DomainInsertDto {
    private final String firstName;
    private final String lastName;
    private final UUID airlineId;
    private final UUID currentLocationId;

    public StewardInsertDto() {
        firstName = "";
        lastName = "";
        airlineId = null;
        currentLocationId = null;
    }

    public StewardInsertDto(String firstName, String lastName, UUID airlineId,
                            UUID currentLocationId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }
}
