# PA165 Java Enterprise Applications - Semestral Project

## Project Description

**Project name**: Airport Manager

### Assignment

Create an information system managing flight records at an airport. The system should allow the users to enter records about stewards, airplanes and destinations. It should also be possible to update and delete these records.

A destination should contain at least the information about the location of an airport (country, city). Airplane details should contain the capacity of the plane and its name (and possibly its type as well). A steward is described by his first and last name. The system should also allow us to record a flight.

When recording a flight, it is necessary to set the departure and arrival times, the origin, the destination and the plane. The system should also check that the plane does not have another flight scheduled during the time of the this flight. It should also be possible to assign (remove) stewards to (from) a flight while checking for the steward's availability.

The ultimate goal is to have a system capable of listing flights ordered by date and displaying the flight details (origin, destination, departure time, arrival time, plane, list of stewards)

### Team Epsilon - Developers

- **Kryštof-Mikuláš Štys**
  - Team Leader
  - 493159@mail.muni.cz
- Matěj Gorgol
  - 540492@mail.muni.cz
- Jan Sýkora
  - 485598@mail.muni.cz
- Tomáš Tomala
  - 541685@mail.muni.cz
- Tereza Vrabcová
  - 485431@mail.muni.cz

### Executable Modules

- **Flight** Module
- **Airline** Module
- **Steward** Module
- **Plane** Module
- **Destination** Module

### User roles

- Airline Manager - SCOPE_test_1
- Airport Manager - SCOPE_test_2
- Steward

## Instructions

### Dependencies

- Java 17
- Maven 3.8.1^

### How to run

If you wish to run the application locally replace `host.docker.internal` with `localhost` in `application.properties`
file of the desired module.

```bash
# Move to the root folder of the project.
cd airport-manager
mvn clean install
# Move into desired executable module 
cd flight-module
mvn spring-boot:run
```

Docker:

```bash
# Move to the root folder of the project.
cd airport-manager
mvn clean install
docker-compose up
```

## Diagrams

### Use Case Diagram

![Use Case Diagram](documentation/useCaseDiagram.png)

### Class Diagram

![Class Diagram](documentation/classDiagram.png)

### DTO Diagrams

**Find DTO Diagram**:

![Find DTO Diagram](documentation/FindDTODiagram.png)

**Insert DTO Diagram**:

![Insert DTO Diagram](documentation/InsertDTODiagram.png)

**Update DTO Diagram**:

![Update DTO Diagram](documentation/UpdateDTODiagram.png)

## Generics

To ensure uniformity and consistency across individual modules, generic entities are implemented for each layer, along with corresponding generic interfaces.

### Example diagram for Service layer

![Generic Service Diagram](documentation/genericService.png)

## Grafana

Grafana allows service monitoring. To access run the application in via docker-compose. Grafana is accessible via 
[Grafana](http://localhost:3000) and the credentials are `admin:admin`.

## Scenario

For these scenarios, API testing framework for python called [Locust](https://locust.io). was used.

Before starting a scenario, Authentication token must be specified inside the scenario file. 
- This token can be obtained at [http://localhost:8080](http://localhost:8080) after OAUTH service is started. 
- After login, you can visit [http://localhost:8080/token](http://localhost:8080/token), where your personal token will be displayed to you. 
- Copy this token (without the "Token:" part) and insert it into *./runnable-scenario/sequentialScenario.py* and *./runnable-scenario/runnableScenario.py* into **AUTHTOKEN** global variable. 

```python
AUTHTOKEN = "<INSERT TOKEN HERE>"
```

- If Authentification token is setup incorrectly, all API calls will fail.

There are two available runnable scenarios

### Sequential Scenario

First scenario is contained in file ./runnable-scenario/sequentialScenario.py

This scenario has predefined number of tasks, where it is supposed to simulate workflow of and airport manager when he has to create flight.

- His job starts with creating a new airline company and registering destinations.
- After those are created, he registers planes and stewards, that will assigned to a flight.
- To create a flight, he needs to specify destination, plane, stewards, date and time. 
- To check what planes are available for given date at given destination, he searches via "unusedPlanes" function.
- After a flight is created, manager decides to change the departure time and update capacity of plane
- Lastly he cancels (deletes) the flight.

After each task, program prints out if task was succesful or not.

### Load Scenario

Second scenario is contained in file ./runnable-scenario/runnableScenario.py

In this scenario, multiple airport managers are created. Tasks, which can airport manager do are endpoints that are exposed by every single microservice.

- Tasks that each airport manager will perform are selected randomly based on their assigned weight. Tasks with higher weight are more likely to be selected.
- After creation, they can perform tasks like searching for entities, creating airlines/destinations, updating them and deleting them. 
- After creating both airlines and destinations, they can create planes and stewards. Along with that, they can also update and delete them.
- After necessary entities are created, they are able to create flights (and modify/update them).

This scenario serves as an load/stress test. This type of test can expose performance issues depending on called tasks.

### Run Instructions

Running locally:
1) Make sure that every microservice is running and AUTHTOKEN has been specified inside scenarion source codes (see previous instructions)
2) Requirements: python3.11, locust and urllib package installed
3) From root project folder, navigate to folder with runnable-scenario 
  ```bash
  cd runnable-scenario
  ```
5) Choose scenario and run it: 
  ```bash 
  locust -f <runnableScenario.py | sequentialScenario.py> --host=http://localhost:8089
  ```
1) Open http://localhost:8089 in browser. First window you will see wants you to specify number of users and spawn rate.
   1) For Sequential Scenario, choose 1 for number of users. For Load Scenario, choose number of users you would like to simulate
   2) Spawnrate is number of users, that are every second added until number of users has not been reached.
2) Click "Start Swarming" to start the test.
3) You will see table with called tasks, number of calls and information about them. You can view possible failures in the "Failures" Tab
