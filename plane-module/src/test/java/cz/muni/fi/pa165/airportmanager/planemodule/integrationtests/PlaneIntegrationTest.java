package cz.muni.fi.pa165.airportmanager.planemodule.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.data.repository.PlaneRepository;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class PlaneIntegrationTest {

    private static final UUID TEST_ID = UUID.randomUUID();
    private static final UUID TEST_ID2 = UUID.randomUUID();
    private static final Plane TEST_PLANE = new Plane(TEST_ID, "TEST_PLANE", "B747", 69, UUID.randomUUID(), UUID.randomUUID());
    private static final Plane TEST_PLANE2 = new Plane(TEST_ID2, "TEST_PLANE2", "A320", 420, UUID.randomUUID(), UUID.randomUUID());
    private final ObjectMapper objectMapper = new ObjectMapper();
    @MockBean
    private PlaneRepository planeRepository;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void findAllPlanesTest() throws Exception {
        List<Plane> planes = List.of(TEST_PLANE, TEST_PLANE2);
        when(planeRepository.findByDeletedIsNull()).thenReturn(planes);

        ResultActions result = mockMvc.perform(get("/api/v1/planes"));
        result.andExpect(MockMvcResultMatchers.status().isOk());
        List<PlaneFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {
        });

        assertAll("entities",
                () -> assertEquals(2, entities.size()),
                () -> assertEquals(TEST_PLANE.getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(TEST_PLANE.getName(),
                                   entities.get(0).getName()),
                () -> assertEquals(TEST_PLANE.getCapacity(),
                                   entities.get(0).getCapacity()),
                () -> assertEquals(TEST_PLANE.getAirlineId(),
                                   entities.get(0).getAirlineId()),
                () -> assertEquals(TEST_PLANE.getCurrentLocationId(),
                                   entities.get(0).getCurrentLocationId()),
                () -> assertEquals(TEST_PLANE2.getId(),
                                   entities.get(1).getGuid()),
                () -> assertEquals(TEST_PLANE2.getName(),
                                   entities.get(1).getName()),
                () -> assertEquals(TEST_PLANE2.getCapacity(),
                                   entities.get(1).getCapacity()),
                () -> assertEquals(TEST_PLANE2.getAirlineId(),
                                   entities.get(1).getAirlineId()),
                () -> assertEquals(TEST_PLANE2.getCurrentLocationId(),
                                   entities.get(1).getCurrentLocationId())
        );
    }

    @Test
    void findPlaneTest() throws Exception {
        when(planeRepository.findById(any())).thenReturn(Optional.of(TEST_PLANE));

        ResultActions result = mockMvc.perform(
                get("/api/v1/planes/" + TEST_PLANE.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        PlaneFindDto entity = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {
        });

        assertAll("entities",
                () -> assertEquals(TEST_PLANE.getId(), entity.getGuid()),
                () -> assertEquals(TEST_PLANE.getName(), entity.getName()),
                () -> assertEquals(TEST_PLANE.getCapacity(),
                                   entity.getCapacity()),
                () -> assertEquals(TEST_PLANE.getAirlineId(),
                                   entity.getAirlineId())
        );
    }

    @Test
    void findPlaneForNonExistentIdTest() throws Exception {
        when(planeRepository.findById(any())).thenReturn(Optional.empty());
        ResultActions result = mockMvc.perform(
                get("/api/v1/planes/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void findPlaneForDeletedTest() throws Exception {
        var plane = new Plane(UUID.randomUUID(), "TESTX",
                         "B747", 69, UUID.randomUUID(), UUID.randomUUID());
        plane.setDeleted(ZonedDateTime.now(ZoneId.of("GMT")).minusDays(1));

        when(planeRepository.findById(any())).thenReturn(Optional.of(plane));

        ResultActions result = mockMvc.perform(
                get("/api/v1/planes/" + plane.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void deleteTest() throws Exception {
        var plane = new Plane(UUID.randomUUID(), "TESTX",
                             "B747", 69, UUID.randomUUID(), UUID.randomUUID());

        when(planeRepository.findById(any())).thenReturn(Optional.of(plane));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/planes/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        verify(planeRepository).findById(any());
        verify(planeRepository, times(0)).delete(any());
        verify(planeRepository).save(any());
        assertNotEquals(null, plane.getDeleted());
        assertNull(plane.getUpdated());
    }

    @Test
    void deleteTestNonExistent() throws Exception {
        when(planeRepository.findById(any())).thenReturn(Optional.empty());

        ResultActions result = mockMvc.perform(
                delete("/api/v1/planes/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
        verify(planeRepository).findById(any());
        verify(planeRepository, times(0)).delete(any());
    }

    @Test
    void deleteTestDeleted() throws Exception {
        var plane = new Plane(UUID.randomUUID(), "TESTX", "B747",
                             69, UUID.randomUUID(), UUID.randomUUID());
        plane.setDeleted(ZonedDateTime.now(ZoneId.of("GMT")));

        when(planeRepository.findById(any())).thenReturn(Optional.of(plane));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/planes/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        verify(planeRepository).findById(any());
        verify(planeRepository, times(0)).delete(any());
    }
}
