package cz.muni.fi.pa165.airportmanager.planemodule.service;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.data.repository.PlaneRepository;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;
import cz.muni.fi.pa165.airportmanager.planemodule.mapper.PlaneMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class PlaneServiceMockTest {
    private static final Plane TEST_PLANE = new Plane("QS2056", "Boeing 737", 185, UUID.randomUUID(), UUID.randomUUID());
    private static final PlaneFindDto FIND_DTO = new PlaneFindDto(UUID.randomUUID(), "QS2056", "Boeing 737", 185, UUID.randomUUID(), UUID.randomUUID());
    private static final List<PlaneFindDto> FIND_DTO_LIST = new ArrayList<>();
    private PlaneRepository planeRepository;
    private PlaneServiceImpl planeService;

    @BeforeEach
    void setUp() {
        planeRepository = mock(PlaneRepository.class);
        PlaneMapper planeMapper = mock(PlaneMapper.class);
        planeService = new PlaneServiceImpl(planeRepository, planeMapper);

        when(planeRepository.findById(any(UUID.class)))
                .thenReturn(Optional.of(TEST_PLANE));

        when(planeRepository
                .findPlanesOfAirlineAtLocation(any(UUID.class),
                                               any(UUID.class))
        )
                .thenReturn(Collections.emptyList());

        when(planeRepository.save(any())).thenReturn(TEST_PLANE);
        when(planeMapper.map(anyList())).thenReturn(FIND_DTO_LIST);
        when(planeMapper.map(any(Plane.class))).thenReturn(FIND_DTO);
        when(planeMapper.map(any(PlaneInsertDto.class)))
                .thenReturn(TEST_PLANE);
        when(planeMapper.map(any(PlaneUpdateDto.class)))
                .thenReturn(TEST_PLANE);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Test", "Correct", "STRSTR", "NAME", "NAME"})
    void addCorrectEntityTest(String name) {
        var plane = new PlaneInsertDto(name, name, 175, UUID.randomUUID(),
                                       UUID.randomUUID());
        planeService.addEntity(plane);
        verify(planeRepository, times(1)).save(any());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "\t", " "})
    void addEmptyNameTest(String name) {
        var plane = new PlaneInsertDto(name, "Type", 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.addEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @ParameterizedTest
    @ValueSource(strings = {"#TS4251", "214232", "1", "@", "?", "=",
                            "2SAF2AS"})
    void addIncorrectNameTest(String name) {
        var plane = new PlaneInsertDto(name, "Type", 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.addEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "\t", " ", "69XD"})
    void addBadTypeTest(String name) {
        var plane = new PlaneInsertDto("Test", name, 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.addEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void addNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.addEntity(null));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void addNullAirline() {
        var plane = new PlaneInsertDto("Name", "Type", 175,
                                    null, UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.addEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Test", "Correct", "STRSTR", "NAME", "NAME"})
    void updateCorrectEntityTest(String name) {
        var plane = new PlaneUpdateDto(UUID.randomUUID(), name, name, 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        planeService.updateEntity(plane);
        verify(planeRepository, times(1)).save(any());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "\t", " "})
    void updateEmptyNameTest(String name) {
        var plane = new PlaneUpdateDto(UUID.randomUUID(), name, name, 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.updateEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @ParameterizedTest
    @ValueSource(strings = {"#TS4251", "214232", "1", "@", "?", "=",
                            "2SAF2AS"})
    void updateIncorrectNameTest(String name) {
        var plane = new PlaneUpdateDto(UUID.randomUUID(), name, "Type", 175,
                                       UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.updateEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void updateNullTest() {
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.updateEntity(null));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void updateBadInputCapacityNull() {
        var plane = new PlaneUpdateDto(UUID.randomUUID(), "name", "name",
                                null, UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.updateEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void updateBadInputCapacityNegative() {
        var plane = new PlaneUpdateDto(UUID.randomUUID(), "name", "name",
                                        -10, UUID.randomUUID(), UUID.randomUUID());
        assertThrows(InvalidUserInputException.class,
                     () -> planeService.updateEntity(plane));
        verify(planeRepository, times(0)).save(any());
    }

    @Test
    void getPlanesOfAirlineAtDestinationTest() {
        var x = planeService.getPlanesOfAirlineAtLocation(UUID.randomUUID(),
                                                          UUID.randomUUID());
        verify(planeRepository, times(1))
                .findPlanesOfAirlineAtLocation(any(), any());
        assert (x.isEmpty());
    }
}
