package cz.muni.fi.pa165.airportmanager.planemodule.rest;

import cz.muni.fi.pa165.airportmanager.planemodule.facade.PlaneFacade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.ZonedDateTime;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class PlaneControllerTest {
    PlaneController controller;
    PlaneFacade planeFacade;

    @BeforeEach
    void setUp() {
        planeFacade = mock(PlaneFacade.class);
        when(planeFacade.getUnusedPlanes(any(), any(), any(), any(), anyString()))
                .thenReturn(java.util.List.of());
        controller = new PlaneController(planeFacade);
    }

    @Test
    void getUnused() {
        var res = controller.getUnusedPlanes(UUID.randomUUID(),
                                             UUID.randomUUID(),
                                             ZonedDateTime.now(),
                                             ZonedDateTime.now(),
                             "");
        verify(planeFacade, times(1))
                .getUnusedPlanes(any(), any(), any(), any(), anyString());
        assert (res.isEmpty());
    }
}