package cz.muni.fi.pa165.airportmanager.planemodule.facade;

import cz.muni.fi.pa165.airportmanager.planemodule.service.AirlineServiceImpl;
import cz.muni.fi.pa165.airportmanager.planemodule.service.DestinationServiceImpl;
import cz.muni.fi.pa165.airportmanager.planemodule.service.FlightServiceImpl;
import cz.muni.fi.pa165.airportmanager.planemodule.service.PlaneServiceImpl;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
class PlaneFacadeImplTest {
    private static final UUID TEST_ID = UUID.randomUUID();
    private static final PlaneFindDto TEST_PLANE_FIND_DTO = new PlaneFindDto(TEST_ID, "NAME", "TYPE", 69, TEST_ID, TEST_ID);
    private static final FlightFindDto TEST_FLIGHT_FIND_DTO = new FlightFindDto(TEST_ID, TEST_ID, TEST_ID, TEST_ID, ZonedDateTime.now(), ZonedDateTime.now(), List.of(TEST_ID, TEST_ID), TEST_ID);
    private PlaneServiceImpl planeService;
    private FlightServiceImpl flightService;
    private PlaneFacadeImpl planeFacade;
    private AirlineServiceImpl airlineService;
    private DestinationServiceImpl destinationService;

    @BeforeEach
    void setUp() {
        planeService = mock(PlaneServiceImpl.class);
        airlineService = mock(AirlineServiceImpl.class);
        destinationService = mock(DestinationServiceImpl.class);
        flightService = mock(FlightServiceImpl.class);

        when(planeService.getPlanesOfAirlineAtLocation(any(UUID.class), any(UUID.class)))
                .thenReturn(List.of(TEST_PLANE_FIND_DTO));
        when(flightService
                .retrieveFlightsOfAirline(any(UUID.class),
                                          any(ZonedDateTime.class),
                                          any(ZonedDateTime.class),
                                          anyString())
        )
                .thenReturn(List.of(TEST_FLIGHT_FIND_DTO));

        planeFacade = new PlaneFacadeImpl(planeService, flightService,
                                          airlineService, destinationService);
    }

    @Test
    void AllPlanesBusy() {
        var res = planeFacade.getUnusedPlanes(TEST_ID, TEST_ID, ZonedDateTime.now(),
                ZonedDateTime.now(), "");

        verify(planeService, times(1)).getPlanesOfAirline(any());
        verify(flightService, times(1))
                .retrieveFlightsOfAirline(any(), any(), any(), anyString());

        assertThat(res.isEmpty()).isTrue();
    }

    @Test
    void NonBusyPlane() {
        var freePlane = new PlaneFindDto(UUID.randomUUID(), "FREE", "XD420",
                                  69, TEST_ID, TEST_ID);
        when(planeService.getPlanesOfAirline(any(UUID.class)))
                .thenReturn(List.of(TEST_PLANE_FIND_DTO, freePlane));


        var res = planeFacade.getUnusedPlanes(TEST_ID, TEST_ID, ZonedDateTime.now(),
                ZonedDateTime.now(), "");

        verify(planeService, times(1)).getPlanesOfAirline(any());
        verify(flightService, times(1))
                .retrieveFlightsOfAirline(any(), any(), any(), any());

        assertThat(res.size()).isEqualTo(1);
        assertThat(res.get(0)).isEqualTo(freePlane);
    }
}