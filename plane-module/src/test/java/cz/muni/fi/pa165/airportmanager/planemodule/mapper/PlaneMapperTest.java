package cz.muni.fi.pa165.airportmanager.planemodule.mapper;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@ActiveProfiles("test")
class PlaneMapperTest {
    @Test
    void correctPlaneReturn() {
        var plane = new Plane(UUID.randomUUID(), "U26265", "Airbus A320",
                       186, UUID.randomUUID(), UUID.randomUUID());
        var mapper = new PlaneMapper();

        PlaneFindDto findDto = mapper.map(plane);

        assertThat(findDto.getName()).isEqualTo(plane.getName());
        assertThat(findDto.getType()).isEqualTo(plane.getType());
        assertThat(findDto.getCapacity()).isEqualTo(plane.getCapacity());
        assertThat(findDto.getAirlineId()).isEqualTo(plane.getAirlineId());
    }

    @Test
    void nullPlane() {
        var mapper = new PlaneMapper();

        PlaneFindDto findDto = mapper.map((Plane) null);

        assertThat(findDto).isNull();
    }

    @Test
    void correctPlaneListReturn() {
        var plane = new Plane(UUID.randomUUID(), "U26265", "Airbus A320",
                       186, UUID.randomUUID(), UUID.randomUUID());
        var mapper = new PlaneMapper();

        List<PlaneFindDto> findDto = mapper.map(List.of(plane));

        assertThat(findDto.get(0).getName()).isEqualTo(plane.getName());
        assertThat(findDto.get(0).getType()).isEqualTo(plane.getType());
        assertThat(findDto.get(0).getCapacity())
                .isEqualTo(plane.getCapacity());
        assertThat(findDto.get(0).getAirlineId())
                .isEqualTo(plane.getAirlineId());
    }

    @Test
    void emptyPlaneList() {
        var mapper = new PlaneMapper();

        List<PlaneFindDto> findDto = mapper.map(new ArrayList<>());

        assertThat(findDto.isEmpty()).isTrue();
    }

    @Test
    void correctPlaneInsertDtoReturn() {
        var planeInsertDto = new PlaneInsertDto("U26265", "Airbus A320",
                                         186, UUID.randomUUID(),
                                                UUID.randomUUID());

        var mapper = new PlaneMapper();

        Plane plane = mapper.map(planeInsertDto);

        assertThat(plane.getName()).isEqualTo(planeInsertDto.getName());
        assertThat(plane.getType()).isEqualTo(planeInsertDto.getType());
        assertThat(plane.getCapacity())
                .isEqualTo(planeInsertDto.getCapacity());
        assertThat(plane.getAirlineId())
                .isEqualTo(planeInsertDto.getAirlineId());
    }

    @Test
    void correctUpdateDtoToPlane() {
        var id = UUID.randomUUID();
        var planeInsertDto = new PlaneUpdateDto(id, "U26265", "Airbus A320",
                                         186, UUID.randomUUID(),
                                                UUID.randomUUID());

        var mapper = new PlaneMapper();
        var plane = mapper.map(planeInsertDto);

        assertThat(plane.getId()).isEqualTo(planeInsertDto.getGuid());
        assertThat(plane.getName()).isEqualTo(planeInsertDto.getName());
        assertThat(plane.getType()).isEqualTo(planeInsertDto.getType());
        assertThat(plane.getCapacity())
                .isEqualTo(planeInsertDto.getCapacity());
        assertThat(plane.getAirlineId())
                .isEqualTo(planeInsertDto.getAirlineId());
    }

    @Test
    void nullInsertDto() {
        var mapper = new PlaneMapper();

        Plane plane = mapper.map((PlaneInsertDto) null);

        assertThat(plane).isNull();
    }

    @Test
    void nullUpdateDto() {
        var mapper = new PlaneMapper();

        Plane plane = mapper.map((PlaneUpdateDto) null);

        assertThat(plane).isNull();
    }
}
