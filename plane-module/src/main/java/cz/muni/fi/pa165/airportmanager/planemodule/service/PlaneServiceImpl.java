package cz.muni.fi.pa165.airportmanager.planemodule.service;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.data.repository.PlaneRepository;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Service
public class PlaneServiceImpl
        extends DomainServiceImpl<Plane, PlaneInsertDto,
                                  PlaneUpdateDto, PlaneFindDto>
        implements PlaneService {
    @Autowired
    public PlaneServiceImpl(PlaneRepository planeRepository,
                            DomainMapper<Plane, PlaneInsertDto,
                                         PlaneUpdateDto,
                                         PlaneFindDto> mapper) {
        super(planeRepository, mapper);
    }

    @Override
    public UUID addEntity(PlaneInsertDto dto) {
        if (entityCheck(dto)) {
            return super.addEntity(dto);
        }

        return null;
    }

    @Override
    public void updateEntity(PlaneUpdateDto dto) {
        if (entityCheck(dto)) {
            super.updateEntity(dto);
        }
    }

    private boolean entityCheck(PlaneUpdateDto plane) {
        if (plane == null) {
            throw new InvalidUserInputException(
                    "The provided plane can not be null!");
        }

        if (plane.getClass() != PlaneUpdateDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct " +
                            "Plane Update Dto class!");
        }

        return checkDtoAttributes(plane.getCapacity(), plane.getName(),
                                  plane.getType(), plane.getAirlineId(),
                                  plane.getCurrentLocationId());
    }

    private boolean entityCheck(PlaneInsertDto plane) {
        if (plane == null) {
            throw new InvalidUserInputException(
                    "The provided plane can not be null!");
        }

        if (plane.getClass() != PlaneInsertDto.class) {
            throw new InvalidUserInputException(
                    "The provided class is not a correct " +
                            "Plane Insert Dto class!");
        }

        return checkDtoAttributes(plane.getCapacity(), plane.getName(),
                                  plane.getType(), plane.getAirlineId(),
                                  plane.getCurrentLocationId());
    }

    private boolean checkDtoAttributes(Integer capacity, String name,
                                       String type, UUID airlineId,
                                       UUID locationId) {
        if (capacity == null) {
            throw new InvalidUserInputException(
                    "Plane capacity can not be null.");
        }

        if (capacity < 0) {
            throw new InvalidUserInputException(
                    "Plane capacity can not be negative.");
        }

        if (name == null) {
            throw new InvalidUserInputException(
                    "A null value is not a legal name.");
        }

        if (name.isBlank()) {
            throw new InvalidUserInputException(
                    "The plane name can not be left blank.");
        }

        if (name.matches("[^A-Za-z0-9]+")) { //Matches every non A-Z a-z 0-9 character
            throw new InvalidUserInputException(
                    "The plane name can not contain special characters.");
        }

        if (!name.matches("^[A-Za-z].*")) { //Matches every name not starting with letter
            throw new InvalidUserInputException(
                    "The plane name can not begin with non-letter");
        }

        if (type == null) {
            throw new InvalidUserInputException(
                    "A null value is not a legal plane type.");
        }

        if (type.isBlank()) {
            throw new InvalidUserInputException("The plane type is blank");
        }

        if (!Character.isLetter(type.charAt(0))) {
            throw new InvalidUserInputException(
                    "The plane type must start with a letter");
        }

        if (airlineId == null) {
            throw new InvalidUserInputException(
                    "Airline UUID needs to be provided.");
        }

        if (locationId == null) {
            throw new InvalidUserInputException(
                    "Location UUID needs to be provided.");
        }

        return true;
    }

    @Override
    public List<PlaneFindDto> getPlanesOfAirline(UUID airlineId) {
        Collection<Plane> collection = ((PlaneRepository) domainRepository)
                .findPlanesOfAirline(airlineId);

        return domainMapper.map(new ArrayList<>(collection));
    }

    @Override
    public List<PlaneFindDto> getPlanesOfAirlineAtLocation(UUID airlineId,
                                                           UUID destinationId) {
        //Obtain Planes of airline from repository
        Collection<Plane> collection = ((PlaneRepository) domainRepository)
                .findPlanesOfAirlineAtLocation(airlineId, destinationId);

        //Return the list
        return domainMapper.map(new ArrayList<>(collection));
    }
}
