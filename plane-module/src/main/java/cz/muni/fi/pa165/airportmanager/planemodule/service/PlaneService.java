package cz.muni.fi.pa165.airportmanager.planemodule.service;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainService;

import java.util.List;
import java.util.UUID;

public interface PlaneService
        extends DomainService<Plane, PlaneInsertDto,
                              PlaneUpdateDto, PlaneFindDto> {
    List<PlaneFindDto> getPlanesOfAirline(UUID airlineId);
    List<PlaneFindDto> getPlanesOfAirlineAtLocation(UUID airlineId,
                                                    UUID destinationId);
}
