package cz.muni.fi.pa165.airportmanager.planemodule.data.repository;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface PlaneRepository extends DomainRepository<Plane> {
    @Query("SELECT plane from Plane plane WHERE plane.airlineId =?1 AND " +
            "plane.deleted IS NULL")
    Collection<Plane> findPlanesOfAirline(UUID airlineId);

    @Query("SELECT plane from Plane plane WHERE plane.airlineId =?1 AND " +
            "plane.currentLocationId =?2 AND plane.deleted IS NULL")
    Collection<Plane> findPlanesOfAirlineAtLocation(UUID airlineId,
                                                    UUID locationId);
}
