package cz.muni.fi.pa165.airportmanager.planemodule;

import io.swagger.v3.oas.models.security.SecurityScheme;

import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@SpringBootApplication(scanBasePackages = {"cz.muni.fi.pa165.airportmanager"})
@EntityScan("cz.muni.fi.pa165.airportmanager.planemodule.data.model")
public class PlaneModuleApplication {
    private static final String PLANES_MANAGER_SCOPE = "SCOPE_test_1";
    private static final String PLANES_URL = "/api/v1/planes";

    public static void main(String[] args) {
        SpringApplication.run(PlaneModuleApplication.class, args);
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http)
            throws Exception {
        http.authorizeHttpRequests(x -> x
                .requestMatchers(HttpMethod.GET, PLANES_URL)
                .hasAuthority(PLANES_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.POST, PLANES_URL)
                .hasAuthority(PLANES_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.GET, PLANES_URL + "/**")
                .hasAuthority(PLANES_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.PATCH, PLANES_URL + "/**")
                .hasAuthority(PLANES_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.DELETE, PLANES_URL + "/**")
                .hasAuthority(PLANES_MANAGER_SCOPE)
                .anyRequest().permitAll()
        ).oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken);

        return http.build();
    }

    /**
     * Add security definitions to generated openapi.yaml.
     */
    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> openApi.getComponents()
                .addSecuritySchemes("Bearer",
                        new SecurityScheme()
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .description("provide a valid access token")
                );
    }
}
