package cz.muni.fi.pa165.airportmanager.planemodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ServerDownException;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

@Service
public class FlightServiceImpl extends RetrieveEntityServiceImpl<FlightFindDto>
        implements RetrieveEntityService<FlightFindDto> {
    public FlightServiceImpl(@Value("${flight.url}") String flightUrl,
                             @Value("${flight.endpoint}") String flightEndpoint) {
        super(flightUrl, flightEndpoint);
    }

    @Override
    public FlightFindDto retrieve(UUID id, String authenticationHeader) {
        try {
            return retrieveResponseSpec(id, authenticationHeader)
                    .bodyToMono(FlightFindDto.class)
                    .block();
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Flights server is down.");
        }
    }

    public List<FlightFindDto> retrieveFlightsOfAirline(UUID airlineId,
                                                        ZonedDateTime start,
                                                        ZonedDateTime end,
                                                        String authenticationHeader) {
        var webClient = WebClient.create();
        try {
            return webClient.get()
                    .uri(destinationUrl + destinationEndpoint
                            + "airlines/" + airlineId + "/between?timeStart=" +
                            start + "&timeEnd=" + end)
                    .header(HttpHeaders.AUTHORIZATION, authenticationHeader)
                    .retrieve()
                    .bodyToFlux(FlightFindDto.class)
                    .collectList()
                    .block();
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Flights server is down.");
        }
    }

    public FlightFindDto retrieveLastFlightOfAirplaneBeforeDate(UUID planeId,
                                                                ZonedDateTime date,
                                                                String authenticationHeader) {
        var webClient = WebClient.create();
        try {
            return webClient.get()
                    .uri(destinationUrl + destinationEndpoint + "last/"
                            + planeId + "/before?date=" + date)
                    .header(HttpHeaders.AUTHORIZATION, authenticationHeader)
                    .retrieve()
                    .bodyToMono(FlightFindDto.class)
                    .block();
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Flights server is down.");
        }
    }
}
