package cz.muni.fi.pa165.airportmanager.planemodule.facade;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;
import cz.muni.fi.pa165.airportmanager.planemodule.service.FlightServiceImpl;
import cz.muni.fi.pa165.airportmanager.planemodule.service.PlaneService;
import cz.muni.fi.pa165.airportmanager.planemodule.service.PlaneServiceImpl;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.AirlineFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.FlightFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacadeImpl;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PlaneFacadeImpl
        extends DomainFacadeImpl<Plane, PlaneInsertDto,
                                 PlaneUpdateDto, PlaneFindDto>
        implements PlaneFacade {
    private final RetrieveEntityService<FlightFindDto> flightService;
    private final RetrieveEntityServiceImpl<AirlineFindDto> airlineService;
    private final RetrieveEntityServiceImpl<DestinationFindDto> destinationService;

    @Autowired
    public PlaneFacadeImpl(PlaneServiceImpl planeService,
                           RetrieveEntityService<FlightFindDto> flightService,
                           RetrieveEntityServiceImpl<AirlineFindDto> airlineService,
                           RetrieveEntityServiceImpl<DestinationFindDto> destinationService) {
        super(planeService);
        this.flightService = flightService;
        this.airlineService = airlineService;
        this.destinationService = destinationService;
    }

    @Override
    public List<PlaneFindDto> getUnusedPlanes(UUID airlineId,
                                              UUID destinationId,
                                              ZonedDateTime timeframeStart,
                                              ZonedDateTime timeframeEnd,
                                              String authenticationHeader) {
        List<PlaneFindDto> unusedPlanes = new ArrayList<>();

        //Get planes for given airline
        List<PlaneFindDto> airlinePlanes = ((PlaneService) domainService)
                .getPlanesOfAirline(airlineId);

        //Retrieve flights for given airline that are scheduled in given timeframe
        List<FlightFindDto> flightsOfPlane = ((FlightServiceImpl) flightService)
                .retrieveFlightsOfAirline(airlineId, timeframeStart, timeframeEnd, authenticationHeader);

        //Check every single plane if it is contained in returned flights and is at the destination
        for (PlaneFindDto plane : airlinePlanes) {
            if (flightsOfPlane.stream()
                    .filter(flight -> flight.getPlaneId().equals(plane.getGuid()))
                    .findFirst().isEmpty()) {
                //Plane is not contained in list of flights conflicting with timeframe ->
                //From flightService retrieve last completed flight of given plane before timeframeStart
                FlightFindDto lastFlight = ((FlightServiceImpl) flightService)
                        .retrieveLastFlightOfAirplaneBeforeDate(plane.getGuid(), timeframeStart, authenticationHeader);
                if (
                        (lastFlight == null && plane.getCurrentLocationId().equals(destinationId)) ||
                                (lastFlight != null && lastFlight.getDestinationId().equals(destinationId))
                ) {
                    //Plane does not have any flights before timeframeStart and is at the destination
                    //OR Plane has last flight before timeframeStart, and it is at the destination
                    unusedPlanes.add(plane);
                }
            }
        }

        return unusedPlanes;
    }

    public void checkProvidedInfo(UUID airline, UUID destination, String authToken) {
        if (airlineService.retrieve(airline, authToken) == null) {
            throw new ObjectNotFoundException("Unknown airline was provided.");
        }

        if (destinationService.retrieve(destination, authToken) == null) {
            throw new ObjectNotFoundException("Unknown location was provided.");
        }
    }

    @Override
    public UUID addEntity(PlaneInsertDto insertDto) {
        checkProvidedInfo(insertDto.getAirlineId(),
                          insertDto.getCurrentLocationId(),
                 null);

        return super.addEntity(insertDto);
    }

    @Override
    public UUID addEntity(PlaneInsertDto planeInsertDto, String token) {
        checkProvidedInfo(planeInsertDto.getAirlineId(),
                          planeInsertDto.getCurrentLocationId(),
                          token);
        return super.addEntity(planeInsertDto);
    }

    @Override
    public void updateEntity(PlaneUpdateDto updateDto) {
        checkProvidedInfo(updateDto.getAirlineId(),
                          updateDto.getCurrentLocationId(),
                 null);
        super.updateEntity(updateDto);
    }

    public void updateEntity(PlaneUpdateDto updateDto, String token) {
        checkProvidedInfo(updateDto.getAirlineId(),
                          updateDto.getCurrentLocationId(),
                          token);
        super.updateEntity(updateDto);
    }
}
