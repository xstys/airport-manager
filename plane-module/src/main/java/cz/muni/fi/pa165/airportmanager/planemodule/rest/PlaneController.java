package cz.muni.fi.pa165.airportmanager.planemodule.rest;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;
import cz.muni.fi.pa165.airportmanager.planemodule.facade.PlaneFacade;
import cz.muni.fi.pa165.airportmanager.planemodule.facade.PlaneFacadeImpl;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.rest.DomainController;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@OpenAPIDefinition( // metadata for inclusion into OpenAPI document
        // see javadoc at https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
        // see docs at https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(title = "Plane Microservice",
                version = "0.1",
                description = """
                        Service for plane handling.
                        The API has operations for:
                        - get all planes
                        - Retrieve plane based on the id
                        - adding a new plane
                        - updating existing plane
                        - removing a specific plane based on the id
                        - list available planes
                        """,
                contact = @Contact(name = "Tomas Tomala",
                                   email = "541685@mail.muni.cz",
                                   url = "https://is.muni.cz/auth/osoba/" +
                                           "541685"),
                license = @License(name = "Apache 2.0",
                                   url = "https://www.apache.org/licenses/" +
                                           "LICENSE-2.0.html")
        ),
        servers = @Server(description = "localhost server",
                          url = "{scheme}://{server}:{port}",
                          variables = {
                            @ServerVariable(name = "scheme",
                                            allowableValues = {"http", "https"},
                                            defaultValue = "http"),
                            @ServerVariable(name = "server",
                                            defaultValue = "localhost"),
                            @ServerVariable(name = "port",
                                            defaultValue = "5002"),
        })
)
@RestController
@Tag(name = "Plane", description = "Microservice for plane handling") // metadata for inclusion into OpenAPI document
@RequestMapping(path = "api/v1/", produces = APPLICATION_JSON_VALUE)
// common prefix for all URLs handled by this controller
public class PlaneController
        extends DomainController<Plane, PlaneInsertDto,
                                 PlaneUpdateDto, PlaneFindDto> {
    @Autowired
    public PlaneController(PlaneFacade planeFacade) {
        super(planeFacade);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all planes",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - planes were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Returns an array of objects representing planes.
                    """)
    @GetMapping("planes") // URL mapping of this operation
    @CrossOrigin(origins = "*") // CORS headers needed for JavaScript clients
    public List<PlaneFindDto> getEntities() {
        return super.getEntities();
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing plane",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - plane was " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Retrieves an existing plane with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string"))
    @GetMapping("planes/{id}")
    public PlaneFindDto findById(@Valid @Parameter(hidden = true)
                                     @PathVariable UUID id) {
        return super.findById(id);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Add a new plane",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - plane was " +
                                         "successfully added"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Adds a provided plane.
                    """)
    @PostMapping("planes") // URL mapping of this operation
    public UUID addEntity(@RequestHeader(name = "Authorization", required = false)
                              @Schema(hidden = true) String authorizationHeader,
                          @RequestBody PlaneInsertDto insertDto) {
        return ((PlaneFacade) facade).addEntity(insertDto, authorizationHeader);
    }

    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Changes an existing plane",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - plane was " +
                                         "successfully modified"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Changes an existing plane with the values from
                    provided plane.
                    """)
    @PatchMapping("planes")
    public void updateEntity(@RequestHeader(name = "Authorization", required = false)
                                 @Schema(hidden = true) String authorizationHeader,
                             @RequestBody PlaneUpdateDto updateDto) {
        ((PlaneFacadeImpl) facade).updateEntity(updateDto, authorizationHeader);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Removes an existing plane",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - plane was " +
                                         "successfully deleted"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Removes an existing plane with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string", format = "uuid"))
    @DeleteMapping("planes/{id}")
    public void delete(@Valid @Parameter(hidden = true)
                           @PathVariable UUID id) {
        super.delete(id);
    }

    @Operation(
            summary = "Get planes of specified airline that " +
                    "are not used in given time frame",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - planes were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_1",
                                 content = @Content()),
            },
            description = """
                    Returns a list of plane IDs representing planes
                    that do not have flight scheduled in given time frame
                    """)
    @Parameter(in = ParameterIn.QUERY, name = "airlineID", 
               schema = @Schema(type = "string", format = "uuid"))
    @Parameter(in = ParameterIn.QUERY, name = "destinationID", 
               schema = @Schema(type = "string", format = "uuid"))
    @Parameter(in = ParameterIn.QUERY, name = "timeframeStart", 
               schema = @Schema(type = "string", 
                                example = "2023-08-08T20:00:30.113Z"))
    @Parameter(in = ParameterIn.QUERY, name = "timeframeEnd", 
               schema = @Schema(type = "string", 
                                example = "2023-08-09T22:30:45.113Z"))
    @GetMapping("planes/unusedplanes") // URL mapping of this operation
    @CrossOrigin(origins = "*") // CORS headers needed for JavaScript clients
    public List<PlaneFindDto> getUnusedPlanes(
            @Valid @RequestParam UUID airlineID,
            @Valid @RequestParam UUID destinationID,
            @Valid @RequestParam ZonedDateTime timeframeStart,
            @Valid @RequestParam ZonedDateTime timeframeEnd,
            @RequestHeader(name = "Authorization", required = false)
            @Schema(hidden = true) String authorizationHeader) {
        return ((PlaneFacade) facade).getUnusedPlanes(airlineID, destinationID,
                                                      timeframeStart,
                                                      timeframeEnd,
                                                      authorizationHeader);
    }
}
