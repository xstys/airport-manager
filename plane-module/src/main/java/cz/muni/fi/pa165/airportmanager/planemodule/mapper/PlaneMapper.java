package cz.muni.fi.pa165.airportmanager.planemodule.mapper;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaneMapper
        implements DomainMapper<Plane, PlaneInsertDto,
                                PlaneUpdateDto, PlaneFindDto> {
    @Override
    public Plane map(PlaneInsertDto planeInsertDto) {
        if (planeInsertDto == null) {
            return null;
        }

        return new Plane(planeInsertDto.getName(), planeInsertDto.getType(),
                         planeInsertDto.getCapacity(),
                         planeInsertDto.getAirlineId(),
                         planeInsertDto.getCurrentLocationId());
    }

    @Override
    public Plane map(PlaneUpdateDto planeUpdateDto) {
        if (planeUpdateDto == null) {
            return null;
        }

        return new Plane(planeUpdateDto.getGuid(), planeUpdateDto.getName(),
                         planeUpdateDto.getType(),
                         planeUpdateDto.getCapacity(),
                         planeUpdateDto.getAirlineId(),
                         planeUpdateDto.getCurrentLocationId());
    }

    @Override
    public PlaneFindDto map(Plane entity) {
        if (entity == null) {
            return null;
        }

        return new PlaneFindDto(entity.getId(), entity.getName(),
                                entity.getType(), entity.getCapacity(),
                                entity.getAirlineId(),
                                entity.getCurrentLocationId());
    }

    @Override
    public List<PlaneFindDto> map(List<Plane> entity) {
        return entity.stream()
                .map(e -> new PlaneFindDto(e.getId(), e.getName(),
                                           e.getType(), e.getCapacity(),
                                           e.getAirlineId(),
                                           e.getCurrentLocationId()))
                .toList();
    }
}
