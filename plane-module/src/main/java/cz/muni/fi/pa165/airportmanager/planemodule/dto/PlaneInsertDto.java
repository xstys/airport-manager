package cz.muni.fi.pa165.airportmanager.planemodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;

import java.util.UUID;

public class PlaneInsertDto extends DomainInsertDto {

    private final String name;
    private final String type;
    private final Integer capacity;
    private final UUID airlineId;
    private final UUID currentLocationId;

    public PlaneInsertDto(String name, String type, Integer capacity,
                          UUID airlineId, UUID currentLocationId) {
        this.name = name;
        this.type = type;
        this.capacity = capacity;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }
}
