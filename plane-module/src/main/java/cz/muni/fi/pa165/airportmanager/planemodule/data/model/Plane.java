package cz.muni.fi.pa165.airportmanager.planemodule.data.model;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;

import java.util.UUID;

@Entity
@Table(name = "PLANE")
public class Plane extends DomainEntity {
    @Size(min = 1)
    @NotEmpty(message = "Name cannot be null")
    @Column(nullable = false, name = "NAME", unique = false)
    private String name;
    @Size(min = 1)
    @NotEmpty(message = "Type cannot be null")
    @Column(nullable = false, name = "TYPE", unique = false)
    private String type;
    @PositiveOrZero(message = "Capacity cannot be negative")
    @Column(nullable = false, name = "CAPACITY", unique = false)
    private Integer capacity;
    @Column(name = "AIRLINE_ID", nullable = false)
    private UUID airlineId;
    @Column(name = "CURRENT_LOCATION_ID", nullable = false)
    private UUID currentLocationId;


    public Plane(UUID guid, String name, String type, Integer capacity,
                 UUID airlineId, UUID currentLocationId) {
        super(guid);
        this.name = name;
        this.type = type;
        this.capacity = capacity;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public Plane(String name, String type, Integer capacity, UUID airlineId,
                 UUID currentLocationId) {
        super(UUID.randomUUID());
        this.name = name;
        this.type = type;
        this.capacity = capacity;
        this.airlineId = airlineId;
        this.currentLocationId = currentLocationId;
    }

    public Plane() {
        super(null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public UUID getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(UUID airlineId) {
        this.airlineId = airlineId;
    }

    public UUID getCurrentLocationId() {
        return currentLocationId;
    }

    public void setCurrentLocationId(UUID currentLocationId) {
        this.currentLocationId = currentLocationId;
    }
}
