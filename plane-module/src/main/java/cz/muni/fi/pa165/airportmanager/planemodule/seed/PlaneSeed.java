package cz.muni.fi.pa165.airportmanager.planemodule.seed;

import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.data.repository.PlaneRepository;

import org.javatuples.Quintet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Profile("dev")
public class PlaneSeed {
    private final PlaneRepository planeRepository;

    @Autowired
    public PlaneSeed(PlaneRepository planeRepository) {
        this.planeRepository = planeRepository;
        seed();
    }

    private void seed() {
        List<Quintet<String, String, Integer, String, String>> planes = List.of(
                Quintet.with("Airbus A320", "Jet", 180,
                        "Czexpats Airlines", "Prague, Czech Republic"),
                Quintet.with("Airbus A321", "Jet", 220,
                        "Czexpats Airlines", "Athens, Greece"),
                Quintet.with("Fokker 50", "Propeller", 58,
                        "Mom and Pop Airlines", "Vienna, Austria")
        );

        for (Quintet<String, String, Integer, String, String> p: planes) {
            UUID id = UUID.nameUUIDFromBytes(
                    (p.getValue0() + ", " + p.getValue3()).getBytes());
            UUID airlineId = UUID.nameUUIDFromBytes(p.getValue3().getBytes());
            UUID destinationId = UUID.nameUUIDFromBytes(
                    p.getValue4().getBytes());

            planeRepository.save(
                    new Plane(id, p.getValue0(), p.getValue1(),
                              p.getValue2(), airlineId, destinationId));
        }
    }
}
