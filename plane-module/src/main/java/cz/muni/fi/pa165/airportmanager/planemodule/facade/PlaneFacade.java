package cz.muni.fi.pa165.airportmanager.planemodule.facade;


import cz.muni.fi.pa165.airportmanager.planemodule.data.model.Plane;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneInsertDto;
import cz.muni.fi.pa165.airportmanager.planemodule.dto.PlaneUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.PlaneFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;

import java.time.ZonedDateTime;

import java.util.List;
import java.util.UUID;

public interface PlaneFacade
        extends DomainFacade<Plane, PlaneInsertDto,
                             PlaneUpdateDto, PlaneFindDto> {
    List<PlaneFindDto> getUnusedPlanes(UUID airlineId, UUID destinationId,
                                       ZonedDateTime timeframeStart,
                                       ZonedDateTime timeframeEnd,
                                       String authenticationHeader);
    UUID addEntity(PlaneInsertDto planeInsertDto, String authenticationHeader);
}
