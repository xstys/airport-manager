package cz.muni.fi.pa165.airportmanager.planemodule.service;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ServerDownException;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityService;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.RetrieveEntityServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;

import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.UUID;

@Service
public class DestinationServiceImpl
        extends RetrieveEntityServiceImpl<DestinationFindDto>
        implements RetrieveEntityService<DestinationFindDto> {
    public DestinationServiceImpl(@Value("${destination.url}") String destinationUrl,
                                  @Value("${destination.endpoint}") String destinationEndpoint) {
        super(destinationUrl, destinationEndpoint);
    }

    @Override
    public DestinationFindDto retrieve(UUID id, String authenticationHeader) {
        try {
            return retrieveResponseSpec(id, authenticationHeader)
                    .bodyToMono(DestinationFindDto.class)
                    .block();
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == HttpStatusCode.valueOf(404)) {
                throw new ObjectNotFoundException("Location ID not found.");
            }

            if (e.getStatusCode() == HttpStatusCode.valueOf(400)) {
                throw new InvalidUserInputException(
                        "Invalid location ID was provided.");
            }

            throw new ServerDownException(
                    "Unexpected error while communicating with the " +
                            "destination server.");
        } catch (WebClientRequestException e) {
            throw new ServerDownException("Destination server is down.");
        }
    }
}
