package cz.muni.fi.pa165.airportmanager.destinationmodule.service;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainService;

public interface DestinationService
        extends DomainService<Destination, DestinationInsertDto,
                              DestinationUpdateDto, DestinationFindDto> {
}
