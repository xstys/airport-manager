package cz.muni.fi.pa165.airportmanager.destinationmodule.service;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository.DestinationRepository;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.mapper.DestinationMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.service.DomainServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.regex.Pattern;

@Service
public class DestinationServiceImpl
        extends DomainServiceImpl<Destination, DestinationInsertDto,
                                  DestinationUpdateDto, DestinationFindDto>
        implements DestinationService {
    @Autowired
    public DestinationServiceImpl(DestinationRepository domainRepository,
                                  DestinationMapper mapper) {
        super(domainRepository, mapper);
    }

    @Override
    public UUID addEntity(DestinationInsertDto entity) {
        if (entityCheck(entity)) {
            return super.addEntity(entity);
        }

        throw new InvalidUserInputException("Invalid destination provided.");
    }

    @Override
    public void updateEntity(DestinationUpdateDto entity) {
        if (entityCheck(entity)) {
            super.updateEntity(entity);
        }
    }

    private boolean entityCheck(DestinationUpdateDto destination) {
        if (destination == null) {
            throw new InvalidUserInputException("Destination is null.");
        }

        if (destination.getClass() != DestinationUpdateDto.class) {
            throw new InvalidUserInputException(
                    "Provided object is not a destination.");
        }

        if (destination.getCountry() == null
                || destination.getCountry().isBlank()) {
            throw new InvalidUserInputException(
                    "Country needs to be provided!");
        }

        if (destination.getCity() == null
                || destination.getCity().isBlank()) {
            throw new InvalidUserInputException("City needs to be provided!");
        }

        var lettersAndSpaces = Pattern.compile("^[ A-Za-z]+$");
        var lettersNumbersAndSpaces = Pattern.compile("^[ A-Za-z0-9]+$");
        var countryMatcher = lettersAndSpaces
                                .matcher(destination.getCountry());
        var cityMatcher = lettersNumbersAndSpaces
                                .matcher(destination.getCity());

        if (!countryMatcher.matches()) {
            throw new InvalidUserInputException(
                    "Country can only contain letters and spaces");
        }

        if (!cityMatcher.matches()) {
            throw new InvalidUserInputException(
                    "City can only contain letters, numbers and spaces");
        }

        return true;
    }

    private boolean entityCheck(DestinationInsertDto destination) {
        if (destination == null) {
            throw new InvalidUserInputException("Destination is null.");
        }

        if (destination.getClass() != DestinationInsertDto.class) {
            throw new InvalidUserInputException(
                    "Provided object is not a destination.");
        }

        if (destination.getCountry() == null
                || destination.getCountry().isBlank()) {
            throw new InvalidUserInputException(
                    "Country needs to be provided!");
        }

        if (destination.getCity() == null
                || destination.getCity().isBlank()) {
            throw new InvalidUserInputException("City needs to be provided!");
        }

        var lettersAndSpaces = Pattern.compile("^[ A-Za-z]+$");
        var lettersNumbersAndSpaces = Pattern.compile("^[ A-Za-z0-9]+$");
        var countryMatcher = lettersAndSpaces
                                .matcher(destination.getCountry());
        var cityMatcher = lettersNumbersAndSpaces
                                .matcher(destination.getCity());

        if (!countryMatcher.matches()) {
            throw new InvalidUserInputException(
                    "Country can only contain letters and spaces");
        }

        if (!cityMatcher.matches()) {
            throw new InvalidUserInputException(
                    "City can only contain letters, numbers and spaces");
        }

        return true;
    }
}
