package cz.muni.fi.pa165.airportmanager.destinationmodule.seed;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository.DestinationRepository;

import org.javatuples.Pair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Profile("dev")
public class DestinationSeed {
    private final DestinationRepository destinationRepository;

    @Autowired
    public DestinationSeed(DestinationRepository destinationRepository) {
        this.destinationRepository = destinationRepository;
        seed();
    }

    private void seed() {
        List<Pair<String, String>> destinations = List.of(
                Pair.with("Prague", "Czech Republic"),
                Pair.with("Athens", "Greece"),
                Pair.with("Edinburgh", "United Kingdom"),
                Pair.with("Vienna", "Austria")
        );

        for (Pair<String, String> d : destinations) {
            UUID id = UUID.nameUUIDFromBytes(
                    (d.getValue0() + ", " + d.getValue1()).getBytes()
            );

            destinationRepository.save(new Destination(id, d.getValue0(),
                                                       d.getValue1()));
        }
    }
}
