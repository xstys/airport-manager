package cz.muni.fi.pa165.airportmanager.destinationmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainInsertDto;

public class DestinationInsertDto  extends DomainInsertDto {
    private final String city;
    private final String country;

    public DestinationInsertDto(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}