package cz.muni.fi.pa165.airportmanager.destinationmodule.dto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.DomainUpdateDto;

import java.util.UUID;

public class DestinationUpdateDto extends DomainUpdateDto {
    private final String city;
    private final String country;

    public DestinationUpdateDto(UUID guid, String city, String country) {
        super(guid);
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}