package cz.muni.fi.pa165.airportmanager.destinationmodule;

import io.swagger.v3.oas.models.security.SecurityScheme;

import org.springdoc.core.customizers.OpenApiCustomizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@SpringBootApplication(scanBasePackages = {
        "cz.muni.fi.pa165.airportmanager.destinationmodule",
        "cz.muni.fi.pa165.airportmanager.domainmodule"
})

@EntityScan("cz.muni.fi.pa165.airportmanager.destinationmodule.data.model")
public class DestinationModuleApplication {
    private static final String DESTINATION_MANAGER_SCOPE = "SCOPE_test_2";
    private static final String DESTINATION_URL = "/api/v1/destinations";

    public static void main(String[] args) {
        SpringApplication.run(DestinationModuleApplication.class, args);
    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http)
            throws Exception {
        http.authorizeHttpRequests(x -> x
                .requestMatchers(HttpMethod.GET, DESTINATION_URL)
                .hasAnyAuthority(DESTINATION_MANAGER_SCOPE, "SCOPE_test_1")
                .requestMatchers(HttpMethod.POST, DESTINATION_URL)
                .hasAuthority(DESTINATION_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.GET, DESTINATION_URL + "/**")
                .hasAnyAuthority(DESTINATION_MANAGER_SCOPE, "SCOPE_test_1")
                .requestMatchers(HttpMethod.PATCH, DESTINATION_URL + "/**")
                .hasAuthority(DESTINATION_MANAGER_SCOPE)
                .requestMatchers(HttpMethod.DELETE, DESTINATION_URL + "/**")
                .hasAuthority(DESTINATION_MANAGER_SCOPE)
                .anyRequest().permitAll()
        ).oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken);

        return http.build();
    }

    /**
     * Add security definitions to generated openapi.yaml.
     */
    @Bean
    public OpenApiCustomizer openAPICustomizer() {
        return openApi -> openApi.getComponents()
                .addSecuritySchemes("Bearer",
                        new SecurityScheme()
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .description("provide a valid access token")
                );
    }
}
