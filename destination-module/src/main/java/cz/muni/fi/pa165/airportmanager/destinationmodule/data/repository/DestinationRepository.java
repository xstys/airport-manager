package cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.domainmodule.data.repository.DomainRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface DestinationRepository extends DomainRepository<Destination> {
}
