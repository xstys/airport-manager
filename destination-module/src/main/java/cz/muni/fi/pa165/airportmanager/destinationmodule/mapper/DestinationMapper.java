package cz.muni.fi.pa165.airportmanager.destinationmodule.mapper;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.mapper.DomainMapper;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinationMapper
        implements DomainMapper<Destination, DestinationInsertDto,
                                DestinationUpdateDto, DestinationFindDto> {
    @Override
    public Destination map(DestinationInsertDto destinationInsertDto) {
        return new Destination(destinationInsertDto.getCity(),
                               destinationInsertDto.getCountry());
    }

    @Override
    public Destination map(DestinationUpdateDto destinationUpdateDto) {
        return new Destination(destinationUpdateDto.getGuid(),
                               destinationUpdateDto.getCity(),
                               destinationUpdateDto.getCountry());
    }

    @Override
    public DestinationFindDto map(Destination entity) {
        return new DestinationFindDto(entity.getId(), entity.getCity(),
                                      entity.getCountry());
    }

    @Override
    public List<DestinationFindDto> map(List<Destination> entity) {
        return entity
                .stream()
                .map(e -> new DestinationFindDto(e.getId(), e.getCity(),
                                                 e.getCountry()))
                .toList();
    }
}