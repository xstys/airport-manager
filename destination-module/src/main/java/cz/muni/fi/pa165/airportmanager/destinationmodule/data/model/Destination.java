package cz.muni.fi.pa165.airportmanager.destinationmodule.data.model;

import cz.muni.fi.pa165.airportmanager.domainmodule.data.model.DomainEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "DESTINATION")
public class Destination extends DomainEntity {
    @Column(name = "CITY", nullable = false)
    private String city;
    @Column(name = "COUNTRY", nullable = false)
    private String country;

    public Destination(String city, String country) {
        super(null);
        this.city = city;
        this.country = country;
    }

    public Destination(UUID guid, String city, String country) {
        super(guid);
        this.city = city;
        this.country = country;
    }

    public Destination() {
        super(null);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
