package cz.muni.fi.pa165.airportmanager.destinationmodule.facade;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacade;

public interface DestinationFacade
        extends DomainFacade<Destination, DestinationInsertDto,
                             DestinationUpdateDto, DestinationFindDto> {
}
