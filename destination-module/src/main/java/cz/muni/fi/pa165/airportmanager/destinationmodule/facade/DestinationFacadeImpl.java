package cz.muni.fi.pa165.airportmanager.destinationmodule.facade;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.facade.DomainFacadeImpl;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.service.DestinationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DestinationFacadeImpl
        extends DomainFacadeImpl<Destination, DestinationInsertDto,
                                 DestinationUpdateDto, DestinationFindDto>
        implements DestinationFacade {
    @Autowired
    public DestinationFacadeImpl(DestinationService domainService) {
        super(domainService);
    }
}
