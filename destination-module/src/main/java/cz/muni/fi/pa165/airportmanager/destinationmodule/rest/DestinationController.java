package cz.muni.fi.pa165.airportmanager.destinationmodule.rest;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.facade.DestinationFacade;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.rest.DomainController;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@OpenAPIDefinition(// metadata for inclusion into OpenAPI document
        // see javadoc at https://javadoc.io/doc/io.swagger.core.v3/swagger-annotations/latest/index.html
        // see docs at https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations
        info = @Info(title = "Destination Service",
                version = "0.1",
                description = """
                        Simple service for destination handling at an airport.
                        The API has operations for:
                        - getting all destinations
                        - adding a new destination
                        - getting a specific destination by its id
                        - removing a specific destination by its id
                        """,
                contact = @Contact(name = "Jan Sykora",
                                   email = "485598@mail.muni.cz",
                                   url = "https://is.muni.cz/auth/osoba/" +
                                           "485598"),
                license = @License(name = "Apache 2.0",
                                   url = "https://www.apache.org/licenses/" +
                                           "LICENSE-2.0.html")
        ),
        servers = @Server(description = "localhost server",
                          url = "{scheme}://{server}:{port}",
                          variables = {
                            @ServerVariable(name = "scheme",
                                            allowableValues = {"http", "https"},
                                            defaultValue = "http"),
                            @ServerVariable(name = "server",
                                            defaultValue = "localhost"),
                            @ServerVariable(name = "port",
                                            defaultValue = "5004"),
        })
)
@Tag(name = "Destination",
     description = "Microservice for destination handling")
@RequestMapping(path = "api/v1/", produces = APPLICATION_JSON_VALUE)
public class DestinationController
        extends DomainController<Destination, DestinationInsertDto,
                                 DestinationUpdateDto, DestinationFindDto> {
    @Autowired
    public DestinationController(DestinationFacade facade) {
        super(facade);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Get all destinations",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_2"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - destinations were " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_2",
                                 content = @Content()),
            },
            description = """
                    Returns an array of objects representing destinations.
                    """)
    @GetMapping("destinations")
    @CrossOrigin(origins = "*")
    public List<DestinationFindDto> getEntities() {
        return super.getEntities();
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Add a new destination",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_2"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - destination was " +
                                         "successfully added"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_2",
                                 content = @Content()),
            },
            description = """
                    Adds a provided destination and returns its ID.
                    """)
    @PostMapping("destinations") // URL mapping of this operation
    public UUID addEntity(@RequestBody DestinationInsertDto insertDto) {
        return super.addEntity(insertDto);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Changes an existing destination",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_2"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - destination was successfully modified"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_2",
                                 content = @Content()),
            },
            description = """
                    Changes an existing destination with the values
                    from provided destination.
                    """)
    @PatchMapping("destinations")
    public void updateEntity(@RequestBody DestinationUpdateDto updateDto) {
        super.updateEntity(updateDto);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Removes an existing destination",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_2"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - destination was " +
                                         "successfully deleted"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_2",
                                 content = @Content()),
            },
            description = """
                    Removes an existing destination with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string"))
    @DeleteMapping("destinations/{id}")
    public void delete(@Valid @Parameter(hidden = true) @PathVariable UUID id) {
        super.delete(id);
    }

    @Override
    @Operation( // metadata for inclusion into OpenAPI document
            summary = "Retrieves an existing destination",
            security = @SecurityRequirement(name = "Bearer",
                                            scopes = {"test_2"}),
            responses = {
                    @ApiResponse(responseCode = "200",
                                 description = "OK - destination was " +
                                         "successfully returned"),
                    @ApiResponse(responseCode = "401",
                                 description = "Unauthorized - access token " +
                                         "not provided or valid",
                                 content = @Content()),
                    @ApiResponse(responseCode = "403",
                                 description = "Forbidden - access token " +
                                         "does not have scope test_2",
                                 content = @Content()),
            },
            description = """
                    Retrieves an existing destination with the specified ID.
                    """)
    @Parameter(in = ParameterIn.PATH, name = "id",
               schema = @Schema(type = "string"))
    @GetMapping("destinations/{id}")
    public DestinationFindDto findById(@Valid @Parameter(hidden = true)
                                           @PathVariable UUID id) {
        return super.findById(id);
    }
}
