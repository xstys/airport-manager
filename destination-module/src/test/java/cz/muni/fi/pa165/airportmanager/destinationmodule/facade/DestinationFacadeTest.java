package cz.muni.fi.pa165.airportmanager.destinationmodule.facade;

import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.service.DestinationService;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class DestinationFacadeTest {
    @Test
    public void addTest() {
        var service = mock(DestinationService.class);
        var facade = new DestinationFacadeImpl(service);
        var add = new DestinationInsertDto("test", "dto");

        facade.addEntity(add);

        verify(service).addEntity(add);
    }

    @Test
    public void deleteTest() {
        var service = mock(DestinationService.class);
        var facade = new DestinationFacadeImpl(service);
        var id = UUID.randomUUID();

        facade.delete(id);

        verify(service).delete(id);
    }

    @Test
    public void updateTest() {
        var service = mock(DestinationService.class);
        var facade = new DestinationFacadeImpl(service);
        var update = new DestinationUpdateDto(UUID.randomUUID(), "test",
                                       "dto");

        facade.updateEntity(update);

        verify(service).updateEntity(update);
    }

    @Test
    public void findTest() {
        var service = mock(DestinationService.class);
        var id = UUID.randomUUID();
        var returnValue = new DestinationFindDto(id, "test", "dto");
        when(service.findById(any()))
                .thenReturn(returnValue);

        var facade = new DestinationFacadeImpl(service);

        var result = facade.findById(id);

        verify(service).findById(id);
        assertEquals(returnValue, result);
    }

    @Test
    public void getEntitiesTest() {
        var service = mock(DestinationService.class);
        var returnValue = List.of(new DestinationFindDto(UUID.randomUUID(),
                                                     "test", "dto"));
        when(service.getEntities())
                .thenReturn(returnValue);

        var facade = new DestinationFacadeImpl(service);

        var result = facade.getEntities();

        verify(service).getEntities();
        assertEquals(returnValue, result);
    }
}
