package cz.muni.fi.pa165.airportmanager.destinationmodule.service;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository.DestinationRepository;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.mapper.DestinationMapper;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.InvalidUserInputException;
import cz.muni.fi.pa165.airportmanager.domainmodule.exception.ObjectNotFoundException;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class DestinationServiceTest {
    @Test
    public void addTest() {
        var destination = new DestinationInsertDto("Wien", "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.save(any())).thenReturn(new Destination());

        destinationService.addEntity(destination);

        verify(destinationRepository).save(any());
    }

    @Test
    public void addNullCountryTest() {
        var destination = new DestinationInsertDto("Wien", null);
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.addEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void addNullCityTest() {
        var destination = new DestinationInsertDto(null, "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.addEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void addNullTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.addEntity(null));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateNullTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.updateEntity(null));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void addSpecialCharacterInCityTest() {
        var destination = new DestinationInsertDto("Wie#", "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.addEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void addSpecialCharacterInCountryTest() {
        var destination = new DestinationInsertDto("Wien", "Au$tria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.addEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), "Wien",
                                            "Austria");
        var destinationEntity = new Destination(UUID.randomUUID(), "Wien",
                                                "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(new Destination(UUID.randomUUID(),
                                                        "Brün",
                                                 "Austrian Empire")));
        when(destinationMapper.map(destination)).thenReturn(destinationEntity);

        destinationService.updateEntity(destination);

        verify(destinationRepository).save(any());
    }

    @Test
    public void updateNonexistentTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), "Wien",
                                            "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> destinationService.updateEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateNullCountryTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), "Wien",
                                            null);
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.updateEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateNullCityTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), null,
                                                   "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.updateEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateSpecialCharacterInCityTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), "Wie#",
                                            "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.updateEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateSpecialCharacterInCountryTest() {
        var destination = new DestinationUpdateDto(UUID.randomUUID(), "Wien",
                                            "Au$tria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        assertThrows(InvalidUserInputException.class,
                     () -> destinationService.updateEntity(destination));

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void findTest() {
        var id = UUID.randomUUID();
        var destination = new Destination(id, "Wien", "Austria");
        var destinationDto = new DestinationFindDto(id, destination.getCity(),
                                                    destination.getCountry());
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findById(id))
                .thenReturn(Optional.of(destination));
        when(destinationMapper.map(destination)).thenReturn(destinationDto);

        var result = destinationService.findById(id);

        verify(destinationRepository, times(1)).findById(any());
        assertEquals(destinationDto, result);
    }

    @Test
    public void findMissingTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> destinationService.findById(UUID.randomUUID()));

        verify(destinationRepository, times(1)).findById(any());
        verify(destinationMapper, times(0)).map(any(Destination.class));
    }

    @Test
    public void findNullTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> destinationService.findById(null));

        verify(destinationRepository, times(1)).findById(any());
        verify(destinationMapper, times(0)).map(any(Destination.class));
    }

    @Test
    public void deleteNullTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> destinationService.findById(null));

        verify(destinationRepository, times(0)).delete(any());
    }

    @Test
    public void deleteMissingTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ObjectNotFoundException.class,
                     () -> destinationService.delete(UUID.randomUUID()));

        verify(destinationRepository, times(0)).delete(any());
    }

    @Test
    public void deleteExistingTest() {
        var id = UUID.randomUUID();
        var destination = new Destination(id, "Wien", "Austria");
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(destination));

        destinationService.delete(id);

        verify(destinationRepository, times(1)).save(destination);
    }

    @Test
    public void getEntitiesSingleTest() {
        var id = UUID.randomUUID();
        var destination = new Destination(id, "Wien", "Austria");
        var destinationDto = new DestinationFindDto(id, destination.getCity(),
                                                    destination.getCountry());
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findAll()).thenReturn(List.of(destination));
        when(destinationMapper.map(anyList()))
                .thenReturn(List.of(destinationDto));

        var result = destinationService.getEntities();

        verify(destinationRepository, times(1)).findByDeletedIsNull();
        assertEquals(1, result.size());
        assertEquals(destinationDto, result.get(0));
    }

    @Test
    public void getEntitiesEmptyTest() {
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findAll()).thenReturn(List.of());
        when(destinationMapper.map(anyList())).thenReturn(List.of());

        var result = destinationService.getEntities();

        verify(destinationRepository, times(1)).findByDeletedIsNull();
        assertEquals(0, result.size());
    }

    @Test
    public void getEntitiesMultipleTest() {
        var destinations = List.of(new Destination(UUID.randomUUID(), "Wien",
                                            "Austria"),
                new Destination(UUID.randomUUID(), "Brün", "Austrian empire"));
        var destinationDtos = List.of(
                new DestinationFindDto(destinations.get(0).getId(),
                                       destinations.get(0).getCity(),
                                       destinations.get(0).getCountry()),
                new DestinationFindDto(destinations.get(1).getId(),
                                       destinations.get(1).getCity(),
                                       destinations.get(1).getCountry()));
        var destinationRepository = mock(DestinationRepository.class);
        var destinationMapper = mock(DestinationMapper.class);
        var destinationService = new DestinationServiceImpl(destinationRepository,
                                                            destinationMapper);

        when(destinationRepository.findAll()).thenReturn(destinations);
        when(destinationMapper.map(anyList())).thenReturn(destinationDtos);

        var result = destinationService.getEntities();

        verify(destinationRepository, times(1)).findByDeletedIsNull();
        assertEquals(2, result.size());
        assertEquals(destinationDtos.get(0), result.get(0));
        assertEquals(destinationDtos.get(1), result.get(1));
    }
}
