package cz.muni.fi.pa165.airportmanager.destinationmodule.endtoendtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository.DestinationRepository;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class DestinationEndToEndTests {
    @Autowired
    private DestinationRepository destinationRepository;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();

    @AfterEach
    public void cleanup() {
        destinationRepository.deleteAll();
    }

    @Test
    public void findAllDestinationsTest() throws Exception {
        var firstDest = destinationRepository.save(
                new Destination("England is my city",
                         "Great British Kingdom"));
        var secondDest = destinationRepository.save(
                new Destination("Wien", "Austria"));

        ResultActions result = mockMvc.perform(get("/api/v1/destinations"));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<DestinationFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(2, entities.size()),
                () -> assertEquals(firstDest.getCity(),
                                   entities.get(0).getCity()),
                () -> assertEquals(firstDest.getCountry(),
                                   entities.get(0).getCountry()),
                () -> assertEquals(firstDest.getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(secondDest.getCity(),
                                   entities.get(1).getCity()),
                () -> assertEquals(secondDest.getCountry(),
                                   entities.get(1).getCountry()),
                () -> assertEquals(secondDest.getId(),
                                   entities.get(1).getGuid())
        );
    }

    @Test
    public void findNonDeletedDestinationsTest() throws Exception {
        var firstDest = destinationRepository.save(
                new Destination("England is my city",
                         "Great British Kingdom"));
        var nondeletedSecondDest = new Destination("Wien", "Austria");

        nondeletedSecondDest.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));
        var secondDest = destinationRepository.save(nondeletedSecondDest);

        ResultActions result = mockMvc.perform(get("/api/v1/destinations"));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<DestinationFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(1, entities.size()),
                () -> assertEquals(firstDest.getCity(),
                                   entities.get(0).getCity()),
                () -> assertEquals(firstDest.getCountry(),
                                   entities.get(0).getCountry()),
                () -> assertEquals(firstDest.getId(),
                                   entities.get(0).getGuid())
        );
    }

    @Test
    public void findDestinationTest() throws Exception {
        var destination = destinationRepository.save(
                new Destination("England is my city",
                         "Great British Kingdom"));

        ResultActions result = mockMvc.perform(
                get("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        DestinationFindDto entity = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(destination.getCity(), entity.getCity()),
                () -> assertEquals(destination.getCountry(),
                                   entity.getCountry()),
                () -> assertEquals(destination.getId(), entity.getGuid())
        );
    }

    @Test
    public void findDestinationForNonExistentIdTest() throws Exception {
        var destination = destinationRepository.save(
                new Destination("Wien", "Austria"));

        UUID nonExistentUUID = UUID.randomUUID();

        while (nonExistentUUID == destination.getId()) {
            nonExistentUUID = UUID.randomUUID();
        }

        ResultActions result = mockMvc.perform(
                get("/api/v1/destinations/" + nonExistentUUID));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void findDestinationForDeletedTest() throws Exception {
        var destination = destinationRepository.save(
                new Destination("Wien", "Austria"));
        destination.setDeleted(
                ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1));
        destinationRepository.save(destination);

        ResultActions result = mockMvc.perform(
                get("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteTest() throws Exception {
        var destination = destinationRepository.save(
                new Destination("Wien", "Austria"));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertNotEquals(null,
                        destinationRepository.findById(destination.getId())
                                .get().getDeleted());
    }

    @Test
    public void deleteTestNonExistent() throws Exception {
        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void deleteTestDeleted() throws Exception {
        var destination = new Destination("Wien", "Austria");
        destination.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));
        destination = destinationRepository.save(destination);

        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void addTest() throws Exception {
        var destination = new DestinationInsertDto("Wien", "Austria");

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(post("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        var items = destinationRepository.findAll();

        assertEquals(1, items.size());
        assertEquals(destination.getCity(), items.get(0).getCity());
        assertEquals(destination.getCountry(), items.get(0).getCountry());
        assertNotEquals(null, items.get(0).getCreated());
        assertNull(items.get(0).getDeleted());
        assertNull(items.get(0).getUpdated());
    }

    @Test
    public void addIncorrectInputTest() throws Exception {
        var destination = new DestinationInsertDto("Wien", "Austri@");

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(post("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateTest() throws Exception {
        var oldDestination = destinationRepository.save(
                new Destination("Brün", "Austrian Empire"));
        var destination = new DestinationUpdateDto(oldDestination.getId(),
                                               "Wien", "Austria");

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        var items = destinationRepository.findAll();

        assertEquals(1, items.size());
        assertEquals(destination.getCity(), items.get(0).getCity());
        assertEquals(destination.getCountry(), items.get(0).getCountry());
        assertNotEquals(null, items.get(0).getCreated());
        assertNotEquals(null, items.get(0).getUpdated());
        assertNull(items.get(0).getDeleted());
    }

    @Test
    public void updateIncorrectInputTest() throws Exception {
        var oldDestination = destinationRepository.save(
                new Destination("Brün", "Austrian Empire"));
        var destination = new DestinationUpdateDto(oldDestination.getId(),
                                               "Wien", "Austri@");

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        var items = destinationRepository.findAll();

        assertEquals(1, items.size());
        assertEquals(oldDestination.getCity(), items.get(0).getCity());
        assertEquals(oldDestination.getCountry(), items.get(0).getCountry());
        assertNotEquals(null, items.get(0).getCreated());
        assertNull(items.get(0).getUpdated());
        assertNull(items.get(0).getDeleted());
    }

    @Test
    public void updateTestNonExistent() throws Exception {
        var destination = new DestinationUpdateDto(UUID.randomUUID(),
                                                   "Wien", "Austria");
        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
