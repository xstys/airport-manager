package cz.muni.fi.pa165.airportmanager.destinationmodule.integrationtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.data.repository.DestinationRepository;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import cz.muni.fi.pa165.airportmanager.domainmodule.dto.finddto.DestinationFindDto;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters=false)
@ActiveProfiles("test")
public class DestinationIntegrationTests {
    @MockBean
    private DestinationRepository destinationRepository;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void findAllDestinationsTest() throws Exception {
        List<Destination> destinations =
                List.of(new Destination("England is my city",
                                        "Great British Kingdom"),
                        new Destination("Wien", "Austria"));
        when(destinationRepository.findByDeletedIsNull())
                .thenReturn(destinations);

        ResultActions result = mockMvc.perform(get("/api/v1/destinations"));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        List<DestinationFindDto> entities = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(2, entities.size()),
                () -> assertEquals(destinations.get(0).getCity(),
                                   entities.get(0).getCity()),
                () -> assertEquals(destinations.get(0).getCountry(),
                                   entities.get(0).getCountry()),
                () -> assertEquals(destinations.get(0).getId(),
                                   entities.get(0).getGuid()),
                () -> assertEquals(destinations.get(1).getCity(),
                                   entities.get(1).getCity()),
                () -> assertEquals(destinations.get(1).getCountry(),
                                   entities.get(1).getCountry()),
                () -> assertEquals(destinations.get(1).getId(),
                                   entities.get(1).getGuid())
        );
    }

    @Test
    public void findDestinationTest() throws Exception {
        var destination = new Destination("Wien", "Austria");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(destination));

        ResultActions result = mockMvc.perform(
                get("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        DestinationFindDto entity = objectMapper.readValue(
                result.andReturn().getResponse().getContentAsString(),
                new TypeReference<>() {});

        assertAll("entities",
                () -> assertEquals(destination.getCity(), entity.getCity()),
                () -> assertEquals(destination.getCountry(),
                                   entity.getCountry()),
                () -> assertEquals(destination.getId(), entity.getGuid())
        );
    }

    @Test
    public void findDestinationForNonExistentIdTest() throws Exception {
        var destination = new Destination("Wien", "Austria");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        UUID nonExistentUUID = UUID.randomUUID();

        while (nonExistentUUID == destination.getId()) {
            nonExistentUUID = UUID.randomUUID();
        }

        ResultActions result = mockMvc.perform(
                get("/api/destinations/" + nonExistentUUID));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void findDestinationForDeletedTest() throws Exception {
        var destination = new Destination("Wien", "Austria");
        destination.setDeleted(
                ZonedDateTime.now(ZoneId.of("UTC")).minusDays(1));

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(destination));

        ResultActions result = mockMvc.perform(
                get("/api/v1/destinations/" + destination.getId()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteTest() throws Exception {
        var destination = new Destination("Wien", "Austria");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(destination));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        verify(destinationRepository).findById(any());
        verify(destinationRepository, times(0)).delete(any());
        verify(destinationRepository).save(any());
        assertNotEquals(null, destination.getDeleted());
        assertNull(destination.getUpdated());
    }

    @Test
    public void deleteTestNonExistent() throws Exception {
        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());

        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());
        verify(destinationRepository).findById(any());
        verify(destinationRepository, times(0)).delete(any());
    }

    @Test
    public void deleteTestDeleted() throws Exception {
        var destination = new Destination("Wien", "Austria");
        destination.setDeleted(ZonedDateTime.now(ZoneId.of("UTC")));

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(destination));

        ResultActions result = mockMvc.perform(
                delete("/api/v1/destinations/" + UUID.randomUUID()));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());
        verify(destinationRepository).findById(any());
        verify(destinationRepository, times(0)).delete(any());
    }

    @Test
    public void addTest() throws Exception {
        var destination = new DestinationInsertDto("Wien", "Austria");
        when(destinationRepository.save(any()))
                .thenReturn(new Destination("Wien", "Austria"));

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(post("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        verify(destinationRepository).save(any());
    }

    @Test
    public void addIncorrectInputTest() throws Exception {
        var destination = new DestinationInsertDto("Wien", "Austri@");

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(post("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateTest() throws Exception {
        var id = UUID.randomUUID();
        var destination = new DestinationUpdateDto(id, "Wien", "Austria");
        var oldDestination = new Destination(id, "Brün", "Austrian Empire");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(oldDestination));

        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        verify(destinationRepository).save(any());
        assertNull(oldDestination.getDeleted());
    }

    @Test
    public void updateIncorrectInputTest() throws Exception {
        var id = UUID.randomUUID();
        var destination = new DestinationUpdateDto(id, "Wien", "Austri@");
        var oldDestination = new Destination(id, "Brün", "Austrian Empire");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.of(oldDestination));


        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isBadRequest());

        verify(destinationRepository, times(0)).save(any());
    }

    @Test
    public void updateTestNonExistent() throws Exception {
        var id = UUID.randomUUID();
        var destination = new DestinationUpdateDto(id, "Wien", "Austria");

        when(destinationRepository.findById(any()))
                .thenReturn(Optional.empty());


        var content = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(destination);

        ResultActions result = mockMvc.perform(patch("/api/v1/destinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content));

        result.andExpect(MockMvcResultMatchers.status().isNotFound());

        verify(destinationRepository, times(0)).save(any());
    }
}