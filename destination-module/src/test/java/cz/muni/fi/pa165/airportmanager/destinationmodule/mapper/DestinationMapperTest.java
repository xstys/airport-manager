package cz.muni.fi.pa165.airportmanager.destinationmodule.mapper;

import cz.muni.fi.pa165.airportmanager.destinationmodule.data.model.Destination;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationInsertDto;
import cz.muni.fi.pa165.airportmanager.destinationmodule.dto.DestinationUpdateDto;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
public class DestinationMapperTest {
    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationInsertDtoToDestinationCityTest(String city) {
        var destination = new DestinationInsertDto(city, "Country");
        var result = new DestinationMapper().map(destination);

        assertEquals(city, result.getCity());
        assertEquals(destination.getCountry(), result.getCountry());
        assertNotNull(result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationInsertDtoToDestinationCountryTest(String country) {
        var destination = new DestinationInsertDto("City", country);
        var result = new DestinationMapper().map(destination);

        assertEquals(country, result.getCountry());
        assertEquals(destination.getCity(), result.getCity());
        assertNotNull(result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationUpdateDtoToDestinationCityTest(String city) {
        var destination = new DestinationUpdateDto(UUID.randomUUID(),
                                                   city, "Country");
        var result = new DestinationMapper().map(destination);

        assertEquals(city, result.getCity());
        assertEquals(destination.getCountry(), result.getCountry());
        assertEquals(destination.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationUpdateDtoToDestinationCountryTest(String country) {
        var destination = new DestinationUpdateDto(UUID.randomUUID(),
                                               "City", country);
        var result = new DestinationMapper().map(destination);

        assertEquals(country, result.getCountry());
        assertEquals(destination.getCity(), result.getCity());
        assertEquals(destination.getGuid(), result.getId());
        assertNotNull(result.getCreated());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationToDestinationFindDtoCityTest(String city) {
        var destination = new Destination(UUID.randomUUID(), city, "Country");
        var result = new DestinationMapper().map(destination);

        assertEquals(city, result.getCity());
        assertEquals(destination.getCountry(), result.getCountry());
        assertEquals(destination.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationToDestinationFindDtoCountryTest(String country) {
        var destination = new Destination(UUID.randomUUID(),"City", country);
        var result = new DestinationMapper().map(destination);

        assertEquals(country, result.getCountry());
        assertEquals(destination.getCity(), result.getCity());
        assertEquals(destination.getId(), result.getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationListToDestinationFindDtoCityTest(String city) {
        var destination = List.of(new Destination(UUID.randomUUID(),
                                                  city, "Country"));
        var result = new DestinationMapper().map(destination);

        assertEquals(1, result.size());
        assertEquals(city, result.get(0).getCity());
        assertEquals(destination.get(0).getCountry(),
                     result.get(0).getCountry());
        assertEquals(destination.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationListToDestinationFindDtoCountryTest(String country) {
        var destination = List.of(new Destination(UUID.randomUUID(),
                                              "City", country));
        var result = new DestinationMapper().map(destination);


        assertEquals(1, result.size());
        assertEquals(country, result.get(0).getCountry());
        assertEquals(destination.get(0).getCity(), result.get(0).getCity());
        assertEquals(destination.get(0).getId(), result.get(0).getGuid());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Normal", "Another okay name",
                            "+ěščřžýáíé=´=´=´é990áýí"})
    public void destinationListMultipleToDestinationFindDtoTest(String name) {
        var destination = List.of(new Destination(UUID.randomUUID(),
                                             "City", name),
                new Destination(UUID.randomUUID(), name, "City"));
        var result = new DestinationMapper().map(destination);

        assertEquals(2, result.size());
        assertEquals(name, result.get(0).getCountry());
        assertEquals(destination.get(0).getCity(), result.get(0).getCity());
        assertEquals(destination.get(0).getId(), result.get(0).getGuid());
        assertEquals(destination.get(1).getCountry(),
                     result.get(1).getCountry());
        assertEquals(name, result.get(1).getCity());
        assertEquals(destination.get(1).getId(), result.get(1).getGuid());
    }
}